
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;


/**
 * `ObservableCollection<T>` に `AddRange` などのメソッドを追加する。`List<T>` と型を合わせる.
 * ちなみに, 要素一つずつ `Add()` すると, つどイベントが発火するのでパフォーマン
 * スが酷い。

 * Note. メソッドの直行性が全然ない。
 *         List<T>          Collection<T>
 * ----------------------------------------------
 *  AddRange()                Add()
 *  InsertRange()             Insert()
 *  RemoveRange()             Remove()
 *
 * ちゃんとした実装は、例えばこちら;
 *  - http://artfulplace.hatenablog.com/entry/2016/12/29/133950
 *  - https://github.com/jamesmontemagno/mvvm-helpers/blob/master/MvvmHelpers/ObservableRangeCollection.cs
 */
public class ExObservableCollection<T>:
                    System.Collections.ObjectModel.ObservableCollection<T>
{
    // Collection<T>#Add() -> InsertItem() の呼び出し.
    public void AddRange(IEnumerable<T> collection)
    {
        if (collection == null)
            throw new ArgumentNullException(nameof(collection));

        InsertRange(Items.Count, collection);
    }

    public void InsertRange(int index, IEnumerable<T> collection)
    {
        if (collection == null)
            throw new ArgumentNullException(nameof(collection));
        this.CheckReentrancy();

        ((List<T>) this.Items).InsertRange(index, collection);

        OnPropertyChanged(nameof(Count));
        OnPropertyChanged(IndexerName);

        // 非ジェネリックな IList. List<T> は IEnumerable<T>, IList を実装.
        // Collection<T>, ImmutableArray<T> も OK. 
        var changedItems = collection as IList;
            
        // IList <- ICollection <- IEnumerable
        // IEnumerable<out T>  <- IEnumerable
        OnCollectionChanged(NotifyCollectionChangedAction.Add,
                    changedItems != null ? changedItems : collection.ToList<T>(),
                    index);
    }

    public void RemoveRange(int index, int count)
    {
        this.CheckReentrancy();

        // IList<T> ではダメ.
        List<T> removedItems = ((List<T>) Items).GetRange(index, count);
        ((List<T>) this.Items).RemoveRange(index, count);

        OnPropertyChanged(nameof(Count));
        OnPropertyChanged(IndexerName);
        OnCollectionChanged(NotifyCollectionChangedAction.Remove,
                            removedItems, index);
    }


    private const string IndexerName = "Item[]"; // 単数形

    // 基底クラスも private になっている。ムムム...
    private void OnPropertyChanged(string prop)
    {
        base.OnPropertyChanged(new PropertyChangedEventArgs(prop));
    }

    // Note. 基底クラスも private になっている。ムムム...
    private void OnCollectionChanged(NotifyCollectionChangedAction action,
                                     IList changedItems, int index)
    {
        // Add なら _newItems に, Remove なら _oldItems に入る.
        // _newItems, _oldItems とも, 型は `IList`.
        // 回り回って, ListCollectionView#ValidateCollectionChangedEventArgs() 内で
        // Add, Remove の場合に要素が 1 かどうか確認していて、例外になる。
        //  -> 結局, Reset しか使えない!
        base.OnCollectionChanged(
            //new NotifyCollectionChangedEventArgs(action, changedItems, index));
            new NotifyCollectionChangedEventArgs(
                                NotifyCollectionChangedAction.Reset, null, -1));
    }
}


