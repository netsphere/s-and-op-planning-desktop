﻿
using System;
using System.Collections;
using System.Collections.Specialized;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

/*
最初、このページに沿ってやってみたが、どうもうまく動かない:
▲ https://tyrrrz.me/blog/wpf-listbox-selecteditems-twoway-binding
  WPF ListBox SelectedItems TwoWay Binding
    OnAttached() 内で, AssociatedObject のほうにしかイベントハンドラを付けていない

このページのが上手くいった:
 - https://www.codeproject.com/Tips/1207965/SelectedItems-Behavior-for-ListBox-and-MultiSelect
   SelectedItems Behavior for ListBox and MultiSelector
*/

namespace WpfPlanning.Common
{

// 添付ビヘイビアは static class にする
public static class SelectedItemsBehaviour
{
    // 添付ビヘイビアは RegisterAttached() メソッドで登録
    // 2引数: PropertyMetadata(object defaultValue, PropertyChangedCallback propertyChangedCallback)
    public static readonly DependencyProperty SelectedItemsProperty =
            DependencyProperty.RegisterAttached(
                    "SelectedItems", // name
                    // propertyType: ObservableCollection<T> に限定しない
                    typeof(INotifyCollectionChanged),
                    typeof(SelectedItemsBehaviour), // ownerType
                    // defaultMetadata
                    new PropertyMetadata(default(IList),  // 既定値
                                         OnTargetPropertyChanged));

    // リフレクションで GetXXX, SetXXX
    public static void SetSelectedItems(DependencyObject d, INotifyCollectionChanged value)
    {
        d.SetValue(SelectedItemsProperty, value);
    }

    // ListBox.SelectedItems Property は非ジェネリックな IList
    // MultiSelector.SelectedItems Property も同じ
    public static IList GetSelectedItems(DependencyObject d)
    {
        return (IList) d.GetValue(SelectedItemsProperty);
    }

    // @callback
    // 依存プロパティが別のオブジェクトに bind されるときに呼び出される。
    //   => XAML で書いているので, ウィンドウが開くとき1回限り。
    private static void OnTargetPropertyChanged(DependencyObject d,
                                        DependencyPropertyChangedEventArgs e)
    {
        IList selectedItems = null;

        // C# ローカル関数は, 外側の変数をキャプチャする。ラムダ関数と同じ.
        // ここで, view model -> WPF control
        void CollectionChangedEventHandler(object? sender,
                                           NotifyCollectionChangedEventArgs args)
        {
            if (args.OldItems != null) {
                foreach (var item in args.OldItems) {
                    if (selectedItems.Contains(item))
                        selectedItems.Remove(item);
                }
            }

            if (args.NewItems != null) {
                foreach (var item in args.NewItems) {
                    if (!selectedItems.Contains(item))
                        selectedItems.Add(item);
                }
            }
        }

        if (d is MultiSelector multiSelector) {
            selectedItems = multiSelector.SelectedItems;
            multiSelector.SelectionChanged += OnSelectionChanged; // ここで仕込んで
        }
        else if (d is ListBox listBox) {
            selectedItems = listBox.SelectedItems;
            //listBox.SelectionMode = SelectionMode.Multiple; 指定忘れはエラーにする
            listBox.SelectionChanged += OnSelectionChanged;
        }
        else
            throw new InvalidOperationException("Not ListBox nor MultiSelector");

        // Bind されるオブジェクトが ObservableCollection<T> ならハンドラを仕込む
        if (e.OldValue is INotifyCollectionChanged collection1)
            collection1.CollectionChanged -= CollectionChangedEventHandler;
        if (e.NewValue is INotifyCollectionChanged collection2)
            collection2.CollectionChanged += CollectionChangedEventHandler;
    }

    // ここが実際に変更のたびに呼び出されるメソッド
    // WPF control -> view model
    static void OnSelectionChanged(object sender, SelectionChangedEventArgs e)
    {
        var s = (DependencyObject) sender;
        if (GetIsBusy(s))
            return;

        SetIsBusy(s, true);
        var list = GetSelectedItems(s);
        // view model 側の setter は使わない. 型が分からない
        foreach (var item in e.RemovedItems)
            if (list.Contains(item)) list.Remove(item); // ここで都度イベント発火
        foreach (var item in e.AddedItems) {
            // e.AddedItems の実体は配列で, null が格納できる!
            if (item != null && !list.Contains(item)) list.Add(item);
        }
        SetIsBusy(s, false);
    }

    // 添付ビヘイビアは全部が static のため, 内部で使うプロパティも定義が必要.
    private static readonly DependencyProperty IsBusyProperty =
            DependencyProperty.RegisterAttached("IsBusy",
                    typeof(bool),
                    typeof(SelectedItemsBehaviour),
                    new PropertyMetadata(default(bool))); // 既定値

    // リフレクションで SetXXX, GetXXX
    private static void SetIsBusy(DependencyObject element, bool value)
    {
        element.SetValue(IsBusyProperty, value);
    }

    private static bool GetIsBusy(DependencyObject element)
    {
        return (bool) element.GetValue(IsBusyProperty);
    }
}

}
