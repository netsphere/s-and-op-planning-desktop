﻿
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;


namespace WpfPlanning.Common
{

/**
 * Radio button の値を得る. intとの相互変換.
 * 
 * Note. いちいちコンバータが必要。
 *       WPF XAML はジェネリック型をサポートしないので、リフレクションで頑張る
 * 参考 https://atmarkit.itmedia.co.jp/ait/articles/1602/03/news032.html
 */
[System.Windows.Data.ValueConversion(typeof(int), typeof(bool))]
public class EnumToRadioButtonConverter : IValueConverter
{
    // @implements
    // Enum (view model側) -> radio button. 該当のラジオボタンだけ true を返す
    public object Convert(object value, Type targetType, object parameter, 
                          CultureInfo culture)
    {
        string param_str = (string) parameter; // XAML 内の引数文字列

        // value が Enum でない -> `ArgumentException` 例外
        if (Enum.IsDefined(value.GetType(), value) == false)
            return DependencyProperty.UnsetValue;

        // 該当ラジオボタンのパラメータと同じ場合は true を返す
        object buttonParam = Enum.Parse(value.GetType(), param_str,
                                        true); // ignoreCase
        return ((int) buttonParam) == (int) value;
    }

    // @implements
    // bool (radio button) -> view model 側
    // true -> false は無視する
    public object ConvertBack(object value, Type targetType, object parameter, 
                  CultureInfo culture)
    {
        // こうすることで、選択されたラジオボタンだけをデータに反映させる
        if (!(bool) value)
            return DependencyProperty.UnsetValue;

        string param_str = (string) parameter; // XAML 内の引数文字列
        return Enum.Parse(targetType, param_str, 
                          true);  // ignoreCase
    }
}

}
