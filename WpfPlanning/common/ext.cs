﻿
using System;
using System.Collections.Generic;

namespace WpfPlanning.common
{
    
static class Ext
{
    // 型を個数が取れるものに限定
    // Single-dimensional arrays also implement IList<T> and IEnumerable<T>.
    public static IEnumerable<T> ReverseIterator<T>(IList<T> items)
    {
        for (int i = items.Count - 1; i >= 0; --i)
            yield return items[i];
    }

    // @return x - y
    public static TimeSpan DateSubtract(DateOnly x, DateOnly y) {
        return x.ToDateTime(TimeOnly.MinValue) - y.ToDateTime(TimeOnly.MinValue);
    }

    public static DateOnly the_monday(DateOnly day) {
        return day.AddDays(- ((((int) day.DayOfWeek) + 6) % 7));
    }
}

}
