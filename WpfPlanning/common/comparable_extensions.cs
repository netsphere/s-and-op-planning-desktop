﻿
using System;

namespace WpfPlanning.Common
{

public static class ComparableExtensions
{
    public static bool IsBetween<T>(this T value, T minInclusiveValue, T maxInclusiveValue)
                            where T : IComparable<T>
    { 
        if (minInclusiveValue.CompareTo(maxInclusiveValue) > 0) 
            throw new ArgumentException();
        return value.CompareTo(minInclusiveValue) >= 0 && 
               value.CompareTo(maxInclusiveValue) <= 0;
    }

}
}
