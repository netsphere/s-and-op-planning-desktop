﻿
using Microsoft.EntityFrameworkCore;
using QuikGraph.Algorithms.TopologicalSort;
using QuikGraph;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using WpfPlanning.Models;


namespace WpfPlanning.Views
{

// 需要側は, location は見ない
class ItemEdge : Edge<Product>
{
    public EBoMType EdgeType { get; set; }

    public int Weight { get; }

    public ItemEdge(EBoMType edgeType, 
                    Product source, Product target, int weight)
                : base(source, target) {
        EdgeType = edgeType;
        Weight = weight;
    }
}

// Kitting の上位側の品目は、「在庫」を持たない.
// 需要と在庫を表示するため、需要を子品目に配分する
class ExpandPhantomAssyDemand
{
    // output
    //HashSet<Product> Reached;

    // 再帰する
    void addVE(AdjacencyGraph<Product, ItemEdge> graph,
               Product product )
    { 
        // ●● 要検討: kitting だけでなく生産も中間需要を発生させるべきでは?
    }

    AdjacencyGraph<Product, ItemEdge> build_graph(IList<Product> products)
    {
        //this.Reached = new HashSet<Product>();

        var graph = new AdjacencyGraph<Product, ItemEdge>();

        // 最終需要があるものだけ
        foreach (var d in products) {
            //Reached.Add(d);
            graph.AddVertex(d);
        }
        foreach (var p in products) 
            addVE(graph, p);

        return graph;
    }


    // arguments
    readonly DateOnly _start_date;
    readonly DateOnly _end_date;
    readonly IList<DemandTran> _demands;
    readonly IList<Product> _products;

    public ExpandPhantomAssyDemand(DateOnly start_date, DateOnly end_date)
    {
        _start_date = start_date;
        _end_date = end_date;
        int planVersion = MyApp.CurPlanVersion.Id;

        // 実データを要約せずに得る.
        _demands = MyApp.DbContext.DemandTran
                    .Where(x => x.PlanVersionId == planVersion && 
                                x.SaleDate >= start_date && x.SaleDate <= end_date)
                    //.Include(x => x.SalesChannel.Location)
                    .Include(x => x.Product)
                    .OrderBy(x => x.SaleDate)
                    .AsNoTracking()
                    .ToList();
        _products = MyApp.DbContext.DemandTran
                    .Where(x => x.PlanVersionId == planVersion &&
                                x.SaleDate >= start_date && x.SaleDate <= end_date)
                    .Include(x => x.Product)
                    .Select(x => x.Product)
                    .Distinct()
                    .AsNoTracking()
                    .ToList();
    }


    public void run()
    {
        foreach ( var demand in _demands ) {
            if ( demand.IntrmQtySold == 0 && demand.IntrmSalesAmount == 0 &&
                 demand.IntrmSalesCostAmount == 0)
                continue;

            demand.IntrmQtySold = 0;
            demand.IntrmSalesAmount = 0;
            demand.IntrmSalesCostAmount = 0;
            MyApp.DbContext.DemandTran.Update( demand );
        }

        AdjacencyGraph<Product, ItemEdge> graph = build_graph(_products);
        var sorter = new TopologicalSortAlgorithm<Product, ItemEdge>(graph);
        sorter.Compute();

        MyApp.DbContext.SaveChanges();
    }
}


    /// demand_page.xaml の相互作用ロジック
    /// </summary>
public partial class DemandPage : Page
{
    public DemandPage()
    {
        InitializeComponent();
    }


    /// /////////////////////////////////////////////////////////////////////
    // Event Handlers

    void Page_Loaded(object sender, System.Windows.RoutedEventArgs e)
    {
        // 実績列を緑色にする
        // @note this.Resources[] は DynamicResource
        var cnv = (ActualGreenConverter) this.FindResource("ActualGreenConverter");

        if (MyApp.CurPlanVersion != null) {
            // TODO: もうちょっとまともな方法がない?
            cnv.HorizonStart = MyApp.CurPlanVersion.StartDate;
        }
    }

    // 商品カテゴリツリーの項目選択
    void TreeView_SelectedItemChanged(object sender, 
                    System.Windows.RoutedPropertyChangedEventArgs<object> e)
    {
        var vm = (DemandPageViewModel) DataContext;
        if ( e.NewValue is DemandCatTemplate dcit) { 
            vm.SelectedCatItem = dcit;   // 右ペインの表示更新
            //vm.refreshTimeTable(dcit);
        }
    }

    // 販売チャネルツリーの項目選択
    void channels_SelectedItemChanged(object sender, 
                    System.Windows.RoutedPropertyChangedEventArgs<object> e)
    { 
        var vm = (DemandPageViewModel) DataContext;
        if ( e.NewValue is SalesChannelTemplate sct) {
            vm.SelectedChannel = sct;
        }
     }


    // @param e イベント引数. EditAction = Commit, EditingElement = TextBox
    //                        Column = DataGridTextColumn, Row = DataGridRow
    void DataGrid_CellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
    {
        var vm = (DemandPageViewModel) DataContext;

        if (e.EditAction != DataGridEditAction.Commit) 
            return; // 編集キャンセル時

        var col = e.Column as DataGridTextColumn;
        if ( col == null) 
            return;
        // col.SortMemberPath = "2018-01-15" など
        int col_idx = col.DisplayIndex; // 編集された列番号
        DataRow row = ((DataRowView) e.Row.Item).Row;
        if (row[0] != "QtySold" && row[0] != "SalesAmount") { 
            MessageBox.Show("QtySold, SalesAmount だけが編集可");
            e.Cancel = true;
            return;
        }
                //obj[1] = ""
                //obj[2] = "0", ...   これは編集前

        TextBox textBox = (TextBox) col.GetCellContent(e.Row);
        double oldValue = double.Parse((string) row[col_idx]);
        double newValue = double.Parse(textBox.Text);

        if ( oldValue == 0.00 ) {
            var prods = vm.ItemsSelected;
            if (prods.Count != 1 || vm.SelectedChannel.HasChildren() ) {
                MessageBox.Show("カテゴリ合計を 0.0 から変更不可. 品目を1つだけ選択してください");
                e.Cancel = true;
                return;
            }
        }

        vm.update_qty_sold(col_idx, DateOnly.Parse(col.SortMemberPath),
                               (string) row[0],
                               oldValue, newValue);
    }

    // 品目一覧の選択
    // ListBox.SelectedItems (複数形) は get only になっていて、さらに bind できない
    void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
        // イベントハンドラの「後」で view model が更新される. CollectionChanged を使え
        // empty.
    }

    // <All items>
    void Button_Click(object sender, RoutedEventArgs e)
    {
        lbProducts.SelectedItems.Clear();
    }


    // [Recalc intermediate demand!]
    // 需要側 = ファントム組立品だけを展開する
    void Button_Click_1(object sender, RoutedEventArgs e)
    {
        // ●● TODO: 実績ならインポートした期間だけ更新すればいい。
        // 計画なら、計画期間だけ更新すればいい。
        DateOnly start_day = new DateOnly(); 
        DateOnly end_day = new DateOnly(); 

        var expander = new ExpandPhantomAssyDemand(start_day, end_day);
        expander.run(); // このなかで保存する
    }

}
}
