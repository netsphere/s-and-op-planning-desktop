﻿
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using WpfPlanning.Models;
using System.Windows;
using NReco.PivotData;
using WpfPlanning.Common;
using WpfPlanning.common;


namespace WpfPlanning.Views
{

// <TreeView.ItemTemplate> の view model.
class SalesChannelTemplate : TmplModel
{
    public ObservableCollection<SalesChannelTemplate>? SubChannels { get; set; }

    public override bool HasChildren()  {
        return SubChannels != null && SubChannels.Count > 0;
    }

    // 実体の model data. Root (全体) のときは null
    public readonly SalesChannel? ChannelNode;

    public readonly SalesChannelTemplate? Parent;


    // コンストラクタ
    public SalesChannelTemplate(SalesChannelTemplate? parent, SalesChannel? channel) 
    {
        if (channel != null) {
            if (parent == null)
                throw new ArgumentNullException(nameof(parent));
            this.Parent = parent;
            this.ChannelNode = channel;
            this.Name = channel.Description;
            if (channel.LocationId != null)  
                this.Name = $"{Name} ({channel.LocationId})";
        }
        else { 
            if (parent != null)
                throw new ArgumentException(nameof(parent));
            this.Name = "<All channels>"  ;
        }
    }

    // 再帰でマスタを追加.
    // @param  self_cat_id  container カテゴリの id. Root の場合は -1.
    public static void add_children(SalesChannelTemplate container, int self_cat_id)
    {
        // ToList() でクエリを閉じないと、次のエラーが起きる:
        //    System.InvalidOperationException: 'There is already an open DataReader associated with this Connection which must be closed first.'
        List<SalesChannel> children = 
                MyApp.DbContext.SalesChannels.Where(
                                        (x) => self_cat_id == -1 ?
                                                    x.ParentId == null : 
                                                    x.ParentId == self_cat_id)
                     .ToList();
        if (children.Count > 0) { 
            container.SubChannels = new ObservableCollection<SalesChannelTemplate>();
            foreach (SalesChannel item in children) { 
                var tmpl = new SalesChannelTemplate(container, item);
                //tmpl.ChannelFilter ??= container.ChannelFilter ;
                add_children(tmpl, item.Id);
                container.SubChannels.Add(tmpl);
            }
        }
    }
} // class SalesChannelTemplate


// 需要サイド。販売チャネルはこちらの画面だけ. 編集可能
internal class DemandPageViewModel : CommonPlanViewModel 
{
    // 左上 pane: 商品カテゴリ. 共通 ///////////////////////////////////////////

    // 左下ペイン: 販売チャネル ////////////////////////////////////////////////

    public ObservableCollection<SalesChannelTemplate> SalesChannels { 
        get; protected set; }

    private SalesChannelTemplate _cvi;
    public SalesChannelTemplate SelectedChannel {
        get { return _cvi; } 
        set {
            bool changed = SetPropertyAndRaise(ref _cvi, value);
            if (changed) {
                refreshPivotTable();  // 品目リストペインはそのまま
            }
        }
    }


    // 品目リストペイン filter ////////////////////////////////////////////////

    // 時系列ペイン (右側ペイン) ///////////////////////////////////////////////


    // コンストラクタ
    public DemandPageViewModel()
    {
        // 左下ペイン: 販売チャネル
        this.SalesChannels = new ObservableCollection<SalesChannelTemplate>();
        var tmpl2 = new SalesChannelTemplate(null, null);
        SalesChannelTemplate.add_children(tmpl2, -1);
        SalesChannels.Add(tmpl2);
        //_cvi = tmpl2;
    }


    // 計画の元の表. PivotData は更新ができない. 編集はこちらに対して行う
    List<DemandTran> _week_demands;

    // 計画期間の分をつくってしまう. 
    PivotData _salesPvtData;


    // ページが読み込まれた。
    // データベースから読み直して, 入れ替えがありうるペインを reset する。
    public void initDynamicPanes()
    {
        loadDemands(); // this._week_demands を作る
        buildPivot();

        // 品目リストペイン
        clearProductList() ; // itemsSelect = "<all>"

        // 右側: 時系列 = 単にクリア ///////////////////////////////////////
        resetTable();
    }


    // データベースから読み込む。この際, 週次, 品目, 販売チャネル単位に集約する
    // this._week_demands を新たに作る
    void loadDemands()
    {
        List<DemandTran> t;
        if (MyApp.CurPlanVersion.IsActual) {
            // 実績のみ。3年間 = 157週間
            DateOnly this_monday = Ext.the_monday(
                                        DateOnly.FromDateTime(DateTime.Now));
            t = read_demand_table(MyApp.CurPlanVersion, 
                            this_monday.AddDays(-(7 * 157)), this_monday.AddDays(-1));
        }
        else { 
            // 計画verの場合は、実績 2年と overlay する
            PlanVersion actualVer = MyApp.DbContext.PlanVersions
                                            .Where(x => x.IsActual).First();
            DateOnly horizonStart = (DateOnly) MyApp.CurPlanVersion.StartDate;
            t = read_demand_table(actualVer, horizonStart.AddDays(-(7 * 105)),
                                  horizonStart.AddDays(-1)); 
            t.AddRange(read_demand_table(MyApp.CurPlanVersion,
                                         (DateOnly) MyApp.CurPlanVersion.StartDate,
                                         (DateOnly) MyApp.CurPlanVersion.EndDate));
        }

        _week_demands = t;
    }


    // `_week_demands` から `_salesPvtData` にキャッシュを構築する
    void buildPivot() 
    { 
        _salesPvtData = new PivotData(
                _pvtDimensions,
                new CompositeAggregatorFactory(_measure_factories));

        _salesPvtData.ProcessData(_week_demands, getValue);
    }


    private bool _isDirty = false;
    public bool IsDirty { 
        get { return _isDirty; }
    }

    // 編集中は、テーブルに現に表示されたものを使う。-> `_week_demands` を更新する
    public void update_qty_sold(int col_idx, DateOnly date, 
                                string field, // "QtySold" or "SalesAmount" 
                                double oldValue, double newValue)
    { 
        if (field != "QtySold" && field != "SalesAmount" )
            throw new ArgumentException("internal error: field: " + field);

        DemandTran cond = new DemandTran();
        if (_tvi.ItemCategory != null) {
            if (_tvi.ItemCategory.ParentId == null) 
                cond.CatL1 = _tvi.ItemCategory.Id;
            else
                cond.CatL2 = _tvi.ItemCategory.Id;
        }
        if (_cvi.ChannelNode != null) { 
            if (_cvi.ChannelNode.ParentId == null)
                cond.ChannelL1 = _cvi.ChannelNode.Id;
            else
                cond.ChannelL2 = _cvi.ChannelNode.Id;
        }
        HashSet<int> selected = new HashSet<int>(ItemsSelected.Select(x => x.Id));

        HashSet<int> found = new HashSet<int>();
        // 元のレコードを得る。複数ありうる
        List<DemandTran> targets = _week_demands.FindAll((x) => {
                if (x.PlanVersionId != MyApp.CurPlanVersion.Id) return false;
                if (!x.SaleDate.IsBetween(date, date.AddDays(6))) return false;
                if (cond.CatL1 > 0 && cond.CatL1 != x.CatL1) return false;
                if (cond.CatL2 > 0 && cond.CatL2 != x.CatL2) return false;
                if (cond.ChannelL1 > 0 && cond.ChannelL1 != x.ChannelL1) return false;
                if (cond.ChannelL2 > 0 && cond.ChannelL2 != x.ChannelL2) return false;
                if (selected.Count > 0) {
                    if (!selected.Contains(x.ProductId)) return false;
                    found.Add(x.ProductId);
                }

                return true;
            });
        foreach (int missing_id in selected.Except(found)) {
            var demand = new DemandTran() { 
                            PlanVersionId = MyApp.CurPlanVersion.Id,
                            ProductId = missing_id,
                            SaleDate = date,
                            SalesChannelId = _cvi.ChannelNode.Id,
                            QtySold = 0.00,
                            SalesAmount = 0.00,
                            SalesCostAmount = 0.00,
                            UnfilledOrders = 0, // Supply 必要. いったん 0 にする
                         };
            _week_demands.Add(demand);
            targets.Add(demand);
        }

        // 更新!!
        foreach (var demand in targets) { 
            if (oldValue != 0.00) { 
                double factor = newValue / oldValue;
                demand.QtySold         *= factor;
                demand.SalesAmount     *= factor;
                demand.SalesCostAmount *= factor;
            }
            else { // 特定の品目
                demand.QtySold         = newValue;
                demand.SalesAmount = 0.00; // ●● TODO: マスタから引っ張る
                demand.SalesCostAmount = 0.00; // マスタ??
            }
        }

        // 再表示
        buildPivot();
        //refreshPivotTable();
    }


    ///// //////////////////////////////////////////////////////////////////
    // 右側: 時系列 table ペイン

    // 右側: 表
    // プロパティ `SelectedCatItem` の変更と、時間層ラジオボタンの変更で直接呼
    // び出される。
    public override void refreshPivotTable()
    {
        resetTable(); // データベースから読み直さない

        // 各列を追加
        List<DataRow> dataRows = new List<DataRow>();
        foreach (var prop in _row_labels) {  // まず項目名だけ
            DataRow row = _dataTable.NewRow();
            row[0] = prop;
            row[1] = ""; // TODO: ●plan version を表示
            dataRows.Add(row);
            _dataTable.Rows.Add(row);
        }

        // mainwindow に依存している TODO: ●application に移動すること!
        var vm = (MainWindowViewModel) Application.Current.MainWindow.DataContext;

        if (_salesPvtData != null ) {
            // slice を表示
            var selectQuery = new SliceQuery(_salesPvtData)
                    .Dimension(vm.TimeStrata == TimeStrataMode.WEEK ? "Date" : "dt_month")
                    /*.Dimension("category-l1").Dimension("channel-l1")*/;
            if (_tvi != null && _tvi.ItemCategory != null) { // root 以外
                selectQuery = selectQuery.Where(
                        _tvi.ItemCategory.Level == 0 ? "category-l1" : "category-l2",
                        _tvi.ItemCategory.Id );
            }
            if (_cvi != null && _cvi.ChannelNode != null) {
                selectQuery = selectQuery.Where(
                        _cvi.ChannelNode.Level == 0 ? "channel-l1" : "channel-l2",
                        _cvi.ChannelNode.Id );
            }
            if (ItemsSelected.Count > 0) {
                // 配列を渡せるが、上手く動かない. 型が object 固定になっている
                // predicate 版を使え
                HashSet<int> selected = new HashSet<int>(ItemsSelected.Select(x => x.Id));
                selectQuery = selectQuery.Where("product",
                                (x) => selected.Contains((int) x));
            }

            PivotData slicedPvtData = selectQuery.Execute();

            var list = slicedPvtData.OrderBy(x => x.Key[0]).ToList();
            int prev_col = 0;
            for (int i = 0; i < list.Count; ++i) {
                var kv = list[i];
                int col = _dates.FindIndex(x => x == (DateOnly) kv.Key[0]);
                if (col == -1) 
                    continue;

                int j;
                for (j = prev_col; j <= col; ++j) { 
                    _dataTable.Columns.Add(new DataColumn() {
                            ColumnName = _dates[j].ToString().Replace("/", "-")});
                }
                prev_col = j;

                for (int ro = 0; ro < 8; ++ro) { 
                    dataRows[ro][col + 2] = 
                            Math.Round((Decimal) kv.Value.AsComposite()
                                                   .Aggregators[ro].Value, 2);
                }
            }
        }

        RaisePropertyChanged(nameof(DataTableView));
    }

} // class DemandPageViewModel


}
