﻿
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using WpfPlanning.Models;


namespace WpfPlanning.Views
{

internal enum TimeStrataMode {
    WEEK = 1, MONTH = 2,
}


internal class MainWindowViewModel : MVVM.BindableBase
{
    // 名前だけ (モデルは MyApp で定義)
    private string _curPlanName;
    public string CurrentPlanVersion { 
        get { return _curPlanName; }
        set { SetPropertyAndRaise(ref _curPlanName, value); }
    }

    // 時間層, 比較バージョンはビューに置く
    private TimeStrataMode _timeStrata;
    public TimeStrataMode TimeStrata { 
        get { return _timeStrata; }
        set {
            bool changed = SetPropertyAndRaise(ref _timeStrata, value);
            // ここから MainWindow のメソッドは呼び出せない。
            // TODO: プロパティの持たせ方?
        }
    }

    public PlanVersion ComparativeVersion { get; set; }

    public MainWindowViewModel()
    {
        TimeStrata = TimeStrataMode.WEEK;
    }
}


    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
public partial class MainWindow : Window
{
    private Page? _demandView;
    private Window? _supply2Window;
    private Page? _supplyView;

    public MainWindow()
    {
        InitializeComponent();

        // [Cycle] menu
        // 新しい planversion の生成を兼ねる
        CommandBindings.Add(new CommandBinding(MyCommands.GenBaselineForecast,
                                               OnCycleBaselineForecast));

        CommandBindings.Add(new CommandBinding(MyCommands.DemandPlansView,
                                               OnViewDemandPlanningView, 
                                               canExecuteIsPlanSelected) );
        CommandBindings.Add(new CommandBinding(MyCommands.RunMRPCommand,
                                               OnCycleMrp,
                                               canExecutePlanIsForecast));
        CommandBindings.Add(new CommandBinding(MyCommands.ProductsView,
                                               OnViewSupplyPlanningView,
                                               canExecuteIsPlanSelected));
        CommandBindings.Add(new CommandBinding(MyCommands.Supply2Show,
                                               onViewSupply2,
                                               canExecuteIsPlanSelected));
    }


    // event `Loaded`
    void Window_Loaded(object sender, RoutedEventArgs e)
    {
        //_demandView = new DemandPage();
        //mainFrame.Navigate(new Uri("/Views/demand_page.xaml", UriKind.Relative)); これはファイル名に依存する!
        //mainFrame.Navigate(_demandView);
    }

    // ◯Week - ◯Month
    void RadioTS_Checked(object sender, RoutedEventArgs e)
    {
        var vm = (MainWindowViewModel) DataContext;
        var x = vm.TimeStrata; // 変更後の値が来てる。OK
        if (_demandView != null) {
            var demand_vm = (DemandPageViewModel) _demandView.DataContext;
            demand_vm.refreshPivotTable();
        }
        if (_supplyView != null) {
            var supply_vm = (SupplyPageViewModel) _supplyView.DataContext;
            supply_vm.refreshPivotTable();
        }
    }

    public void changePlan() {
        if (_demandView != null || _supplyView != null) 
            mainFrame.Navigate(null);

        _demandView = null;
        _supplyView = null;
    }

    /// //////////////////////////////////////////////////////////////////
    // Command Handlers

    void OnCycleBaselineForecast(object sender, ExecutedRoutedEventArgs e)
    {
        var wnd = new BaselineForecastDlg();
        if (wnd.ShowDialog() == true) {
            // TODO: 現在の plan version を新しいものにする.
        }
    }

    void OnCycleMrp(object sender, ExecutedRoutedEventArgs e)
    {
        var wnd = new MrpWindow();
        wnd.Show();
    }


    // View -> Demand Planning View
    void OnViewDemandPlanningView(object sender, ExecutedRoutedEventArgs args)
    {
        bool to_refresh = false;
        if (_demandView == null) {  
            _demandView = new DemandPage();
            to_refresh = true;
        }

        mainFrame.Navigate(_demandView);
        if (to_refresh) { 
            var vm = (DemandPageViewModel) _demandView.DataContext;
            vm.initDynamicPanes(); // TODO: どんくさい. モデル更新で通知したほうがよい.
        }
    }

    // demand planning & supply planning 共通
    void canExecuteIsPlanSelected(object sender, CanExecuteRoutedEventArgs e)
    {
        e.CanExecute = MyApp.CurPlanVersion != null;
    }

    void canExecutePlanIsForecast(object sender, CanExecuteRoutedEventArgs args)
    {
        args.CanExecute = MyApp.CurPlanVersion != null &&
                          !MyApp.CurPlanVersion.IsActual ; 
    }

    // View -> Products View
    void OnViewSupplyPlanningView(object sender, ExecutedRoutedEventArgs e)
    {
        bool to_refresh = false;
        if (_supplyView == null) { 
            _supplyView = new SupplyPage();
            to_refresh = true;
        }
        
        mainFrame.Navigate(_supplyView);
        if (to_refresh) { 
            var vm = (SupplyPageViewModel) _supplyView.DataContext;
            vm.refresh(); // TODO: どんくさい
        }
    }

    void onViewSupply2(object sender, ExecutedRoutedEventArgs e)
    {
        bool to_refresh = false;
        if (_supply2Window == null) {
            _supply2Window = new SupplyPage2(); // TODO: ●開いていたらリサイクル
            to_refresh = true;
        }

        if (to_refresh) { 
            var vm = (SupplyPage2ViewModel) _supply2Window.DataContext;
            vm.initDynamicPanes(); // TODO: どんくさい. モデル更新で通知したほうがよい.
        }

        _supply2Window.Show();
    }
}

}
