﻿
using System;
using System.Windows;
using WpfPlanning.Models;


namespace WpfPlanning.Views
{

// 型が合わなので、model を view model にはできない. ひどい
class PlanVersionViewModel : MVVM.BindableBase
{
    string _name;
    public string Name { get => _name; 
        set { SetPropertyAndRaise(ref _name, value); }
    }

    bool _isActual;
    public bool IsActual { get => _isActual; 
        set { SetPropertyAndRaise(ref _isActual, value); }
    }

    // オープンなのは<s>一つだけ</s>. -> 複数可にする。
    bool _isOpen;
    public bool IsOpen { get => _isOpen;
        set { SetPropertyAndRaise(ref _isOpen, value); }
    }

    // 計画を作成した日。実績バージョンでは NULL.
    // Note. <DatePicker> は DateTime 型
    DateOnly? _forecastDate;
    public DateOnly? ForecastDate { get => _forecastDate;
        set { SetPropertyAndRaise(ref _forecastDate, value); }
    }

    // 計画期間 (planning horizon) の開始日。実績バージョンは NULL
    DateOnly? _startDate;
    public DateOnly? StartDate { get => _startDate; 
        set { SetPropertyAndRaise(ref _startDate, value); }
    }

    DateOnly? _endDate;
    public DateOnly? EndDate { get => _endDate;
        set { SetPropertyAndRaise(ref _endDate, value); }
    }
}


    /// <summary>
    /// new_plan_version_window.xaml の相互作用ロジック
    /// </summary>
public partial class EditPlanVersionWindow : Window
{
    public PlanVersion PlanVersion { get; protected set; }

    // default 
    public EditPlanVersionWindow()
    {
        InitializeComponent();
        this.PlanVersion = new PlanVersion();
    }

    public EditPlanVersionWindow(PlanVersion planVersion) {
        InitializeComponent();
        okBtn.Content = "Update";

        this.PlanVersion = planVersion;
        var vm = (PlanVersionViewModel) DataContext;
        vm.Name = planVersion.Name;
        vm.IsActual = planVersion.IsActual;
        vm.IsOpen = planVersion.IsOpen;
        vm.ForecastDate = planVersion.ForecastDate ;
        vm.StartDate = planVersion.StartDate ;
        vm.EndDate = planVersion.EndDate ;
    }
    

    /// ///////////////////////////////////////////////////////////
    // Event Handlers

    void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
    {

    }

    void cancelButton_Click(object sender, RoutedEventArgs e)
    {
        DialogResult = false;
    }

    // [Create/Update] button
    void okButton_Click(object sender, RoutedEventArgs e)
    {
        var vm = (PlanVersionViewModel) DataContext;
        this.PlanVersion.Name = vm.Name;
        this.PlanVersion.IsOpen = vm.IsOpen;
        this.PlanVersion.ForecastDate = vm.ForecastDate ;
        this.PlanVersion.StartDate = vm.StartDate ;
        this.PlanVersion.EndDate = vm.EndDate ;
        
        // TODO: ここで validation error 確認
        if (PlanVersion.Id > 0)
            MyApp.DbContext.PlanVersions.Update(PlanVersion);
        else
            MyApp.DbContext.PlanVersions.Add(PlanVersion);
        MyApp.DbContext.SaveChanges();

        DialogResult = true;
    }
}

}
