﻿
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using WpfPlanning.Models;


namespace WpfPlanning.Views
{

class PlanVersionItemTemplate //: INotifyPropertyChanged
{
    public string Name { get; set; }

    public List<PlanVersionItemTemplate> Children { get; set; }

    public PlanVersion PlanVersion;
}


internal class SelectPlanVersionDlgViewModel : MVVM.BindableBase
{
    public ObservableCollection<PlanVersionItemTemplate> PlanVersions { 
        get; protected set; }

    public PlanVersionItemTemplate? SelectedPlan { get; set; }


    // コンストラクタ
    public SelectPlanVersionDlgViewModel()
    {
        this.PlanVersions = new ObservableCollection<PlanVersionItemTemplate>();

        // ここで初期読み込みしてOK
        itemsRefresh();
    }


    void itemsRefresh()
    {
        var query = (from planver in MyApp.DbContext.PlanVersions
                     orderby planver.StartDate
                     select planver).ToList<PlanVersion>();
        this.PlanVersions.Clear();
        foreach (var pv in query) { 
            this.PlanVersions.Add(new PlanVersionItemTemplate() {
                                    Name = pv.Name, PlanVersion = pv} );  
        }
        RaisePropertyChanged(nameof(PlanVersions));
    }


    /// /////////////////////////////////////////////////////////////
    // Command Handlers

    public void PlanVersionNew_executed(object sender, ExecutedRoutedEventArgs e)
    {
        var wnd = new EditPlanVersionWindow();
        if (wnd.ShowDialog() == true) 
            itemsRefresh();
    }

    public void canExecutePlanVersionEdit(object sender, 
                                          CanExecuteRoutedEventArgs e)
    {
        e.CanExecute = this.SelectedPlan != null && 
                       !SelectedPlan.PlanVersion.IsActual;
    }

    public void PlanVersionEdit_executed(object sender, ExecutedRoutedEventArgs e)
    {
        var wnd = new EditPlanVersionWindow(SelectedPlan.PlanVersion);
        if ( wnd.ShowDialog() == true) { 
            SelectedPlan.Name = wnd.PlanVersion.Name;
/*
        SelectedPlan.IsOpen = vm.IsOpen;
        SelectedPlan.ForecastDate = vm.ForecastDate != null ?
                                        DateOnly.FromDateTime((DateTime)vm.ForecastDate) : null;
        SelectedPlan.StartDate = vm.StartDate != null ?
                                        DateOnly.FromDateTime((DateTime)vm.StartDate) : null;
        SelectedPlan.EndDate = vm.EndDate != null ?
                                        DateOnly.FromDateTime((DateTime)vm.EndDate) : null;
*/        
            itemsRefresh();
        }
    }

    public void PlanVersionDestroy_executed(object sender, ExecutedRoutedEventArgs e)
    {
        if (MessageBox.Show("Sure?", "Destroy", MessageBoxButton.YesNo) == 
                    MessageBoxResult.Yes) {
            MyApp.DbContext.PlanVersions.Remove(this.SelectedPlan.PlanVersion);
            MyApp.DbContext.SaveChanges();
            itemsRefresh();
        }
    }
}


    /// <summary>
    /// SelectPlanVersionDlg.xaml の相互作用ロジック
    /// </summary>
public partial class SelectPlanVersionDlg : Window
{
    // 呼び出し元に渡す用
    public PlanVersion? SelectedPlan { 
        get { 
            var vm = (SelectPlanVersionDlgViewModel) DataContext;
            return vm.SelectedPlan ?.PlanVersion;
        }
    }

    // コンストラクタ
    public SelectPlanVersionDlg()
    {
        InitializeComponent();

        var vm = (SelectPlanVersionDlgViewModel) DataContext;
        CommandBindings.Add(new CommandBinding(MyCommands.NewPlanVersion,
                                               vm.PlanVersionNew_executed));
        CommandBindings.Add(new CommandBinding(MyCommands.PlanVersionEdit,
                                               vm.PlanVersionEdit_executed,
                                               vm.canExecutePlanVersionEdit));
        CommandBindings.Add(new CommandBinding(MyCommands.PlanVersionDestroy,
                                               vm.PlanVersionDestroy_executed,
                                               vm.canExecutePlanVersionEdit));
    }


    /// /////////////////////////////////////////////////////////////
    // Event handlers

    private void Window_Closing(object sender, CancelEventArgs e)
    {

    }

    private void cancelBtn_Click(object sender, RoutedEventArgs e)
    {
        DialogResult = false;
    }

    // [Select] button
    private void okBtn_Click(object sender, RoutedEventArgs e)
    {
        DialogResult = true;
    }

    // イベント
    void treeView_SelectedItemChanged(object sender, 
                                      RoutedPropertyChangedEventArgs<object> e)
    {
        var vm = (SelectPlanVersionDlgViewModel) DataContext;

        if (e.NewValue is PlanVersionItemTemplate item) { 
            vm.SelectedPlan = item;
            //this.SelectedPlan = item.planVersion;
            selectedPane.DataContext = new PlanVersionViewModel() {
                        Name = this.SelectedPlan.Name,
                        IsActual = this.SelectedPlan.IsActual,
                        IsOpen = this.SelectedPlan.IsOpen,
                        ForecastDate = this.SelectedPlan.ForecastDate ,
                        StartDate = this.SelectedPlan.StartDate ,
                        EndDate = this.SelectedPlan.EndDate  
                    };
        }
        else
            vm.SelectedPlan = null;
    }

}

} // namespace WpfPlanning.Views

