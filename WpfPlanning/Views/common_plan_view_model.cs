﻿
using Microsoft.EntityFrameworkCore;
using NReco.PivotData;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Windows;
using WpfPlanning.common;
using WpfPlanning.Models;


namespace WpfPlanning.Views
{

abstract class TmplModel
{
    // 表示名
    public string Name { get; protected set; }

    public bool IsExpanded { get; set; }

    public bool IsSelected { get; set; }

    public abstract bool HasChildren(); 
}


// 商品カテゴリ: <TreeView.ItemTemplate> の view model.
class DemandCatTemplate : TmplModel
{
    // @note `HierarchicalDataTemplate.ItemsSource` プロパティの型は `BindingBase`. どゆこと?
    public ObservableCollection<DemandCatTemplate>? SubCategories { 
        get; set; }

    public override bool HasChildren()  {
        return SubCategories != null && SubCategories.Count > 0;
    }

    //public readonly DemandCatTemplate? Parent;

    // 実体の model data。Root (全体) のときは null
    public readonly ItemCategory? ItemCategory;


    // コンストラクタ
    public DemandCatTemplate(DemandCatTemplate? parent, ItemCategory? cat)
    { 
        if (cat != null) { 
            if (parent == null)
                throw new ArgumentNullException(nameof(parent));
            //this.Parent = parent;
            this.ItemCategory = cat;
            this.Name = cat.Name;
        }
        else { 
            if (parent != null)
                throw new ArgumentException(nameof(parent));
            this.Name = "<All categories>"                ;
        }
    }

    // 再帰でマスタを追加.
    // @param  self_cat_id  container カテゴリの id. Root の場合は -1.
    public static void add_children(DemandCatTemplate container, int self_cat_id)
    {
        // ToList() でクエリを閉じないと、次のエラーが起きる:
        //    System.InvalidOperationException: 'There is already an open DataReader associated with this Connection which must be closed first.'
        List<ItemCategory> children = 
                MyApp.DbContext.ItemCategories.Where(
                                        (x) => self_cat_id == -1 ?
                                                    x.ParentId == null : 
                                                    x.ParentId == self_cat_id)
                     .ToList();
        if (children.Count > 0) { 
            container.SubCategories = new ObservableCollection<DemandCatTemplate>();
            foreach (ItemCategory item in children) { 
                var tmpl = new DemandCatTemplate(container, item);
                //tmpl.ChannelFilter ??= container.ChannelFilter ;
                add_children(tmpl, item.Id);
                container.SubCategories.Add(tmpl);
            }
        }
    }

} // class DemandCatTemplate


// 需要サイドのうち使い回す部分    
abstract class CommonPlanViewModel : MVVM.BindableBase
{
    // 左上ペイン: 商品カテゴリ tree  //////////////////////////////////////////

    // @note ここはコレクションにしないといけない.
    public ObservableCollection<DemandCatTemplate> ProductCategories { 
        get; protected set; }

    protected DemandCatTemplate _tvi;
    public DemandCatTemplate SelectedCatItem { 
        get { return _tvi; }
        set { 
            //ItemCategory? oldValue = _tvi != null ? _tvi.ItemCategory : null;
            bool changed = SetPropertyAndRaise(ref _tvi, value);
            if (changed) { 
                refreshItemList();  // 品目リストペイン更新 → 時系列テーブルの更新も発生
                //refreshPivotTable(); 
            }
        }
    }


    // 品目リストペイン filter ////////////////////////////////////////////////

    // 1. 選択肢
    public ObservableCollection<Product> Products { get; protected set; }
    // 2. 選択されているもの. いくつか, または "<all>"
    // two way binding ではない. 非ジェネリックな IList 派生にすること
    public ObservableCollection<Product> ItemsSelected { get; protected set; }

    protected void clearProductList()
    {
        this.Products.Clear();
        //Products.Add(new Product() {Description = "<all items>"}); 実装むずい
        this.ItemsSelected.Clear(); 
        //this.ItemsSelected.Add(Products.ElementAt(0));
    }

    // 選択中のカテゴリの直接の品目のみを表示
    void refreshItemList()
    {
        clearProductList(); // 選択状態は <all>

        // Cat = Root (全体) のときは null
        ItemCategory? cat = _tvi.ItemCategory;
        if (cat != null) { 
            // Products まで展開する. Demand 側はセット品も並列
            var prod_list = MyApp.DbContext.Products.Where(
                                (x) => x.ItemCategoryId == cat.Id && x.IsSaleable)
                             .ToList();
            foreach(var item in prod_list) 
                Products.Add(item);
        }
    }


    // 時系列ペイン (右側ペイン) ///////////////////////////////////////////////

    // Point! こちらは private にする.
    protected readonly DataTable _dataTable = new DataTable();

    // こちらを `<DataGrid>` に bind する.
    // 注意! このような使い捨ての書き方にしなければならない。
    public DataView DataTableView => new DataView(_dataTable); // { get; protected set; }

    public abstract void refreshPivotTable();


    // コンストラクタ
    protected CommonPlanViewModel() 
    {
        // 左上ペイン: 商品カテゴリツリー
        this.ProductCategories = new ObservableCollection<DemandCatTemplate>();

        // Root は特別。総合計を表示するようにする。
        var tmpl = new DemandCatTemplate(null, null);
        DemandCatTemplate.add_children(tmpl, -1);
        ProductCategories.Add(tmpl);
        //_tvi = tmpl; // 初期値 = <all cat>

        // 品目リストペイン
        this.Products = new ObservableCollection<Product>();
        this.ItemsSelected = new ObservableCollection<Product>();
        clearProductList(); // itemsSelect = "<all>"
        this.ItemsSelected.CollectionChanged += ItemsSelected_CollectionChanged;
    }

    // event handler
    void ItemsSelected_CollectionChanged(object? sender, 
                    System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
    {
        refreshPivotTable();
    }

/*
生 SQL の渡し方:
  - EF6 時代は `SqlQuery()`. メソッド名が変わった
  - EF Core 3.x では 生文字列 plain strings も許容。危険なので廃止.
      => `FromSqlRaw()` を使え.  
  - EF Core 7.0 以降では `FormattableString` に. API は一つの引数しか受け付けない。
    "... Category = {0} ..." みたいなのは不可

週の月曜日:
  - DATEPART(WEEKDAY) が環境変数によって変わる変態仕様。
    `SET DATEFIRST 1` (月曜日始まり) は, ほかに影響あるのでよくない
  => 環境変数の値で補正した, 月曜..日曜 = 0..6 を引いてやる
*/
    protected static List<DemandTran> read_demand_table(PlanVersion planVer, 
                                       DateOnly start, DateOnly end)
    {
/*
月・週で集計済みのデータ
NReco.PivotData 側で "受注残" を足し上げてしまわないように,
期間ごとにジャストで 1 レコードだけを出力するようにする

windowing_clause を省略すると RANGE BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW になる。明示が必要。
LAST_VALUE() には OVER句が必須. GROUP BY 後なので, その側の SELECT に SaleDate が使えない
*/
        // ORDER BY 句:
        // Microsoft.Data.SqlClient.SqlException: 'The ORDER BY clause is
        // invalid in views, inline functions, derived tables, subqueries, and
        // common table expressions, unless TOP, OFFSET or FOR XML is also specified.'
        List<DemandTran> t = MyApp.DbContext.DemandTran
                    .FromSql(
$@"
SELECT MAX(tmp.Id) AS Id, PlanVersionId, CatL1, CatL2, ProductId, SalesChannelId, 
    SUM(QtySold) AS QtySold,
    SUM(IntrmQtySold) AS IntrmQtySold,
    SUM(SalesAmount) AS SalesAmount,
    SUM(IntrmSalesAmount) AS IntrmSalesAmount,
    SUM(SalesCostAmount) AS SalesCostAmount, 
    SUM(IntrmSalesCostAmount) AS IntrmSalesCostAmount,
    AVG(UnfilledOrders2) AS UnfilledOrders, dt_week AS SaleDate -- , dt_month
FROM (  
    SELECT -- CAST(FORMAT(SaleDate, 'yyyy-MM-01') AS DATE) AS dt_month,
        DATEADD(DAY, -((DATEPART(weekday, SaleDate) + @@DATEFIRST - 2) % 7),
                SaleDate) AS dt_week,
        LAST_VALUE(unfilledorders) OVER(
                PARTITION BY PlanVersionId, ProductId, SalesChannelId, 
                             DATEADD(DAY, -((DATEPART(weekday, SaleDate) + @@DATEFIRST - 2) % 7), SaleDate)
                ORDER BY SaleDate
                ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS unfilledorders2,
           *
    FROM dbo.demand_tran) AS tmp
WHERE PlanVersionId = {planVer.Id} AND 
      (dt_week BETWEEN {start} AND {end})   -- ここが週前提
GROUP BY PlanVersionId, CatL1, CatL2, ProductId, SalesChannelId, dt_week --  dt_month,
-- ORDER BY SaleDate 
")
                    .Include(x => x.SalesChannel) // For PSI
                    //.OrderBy(x => x.SaleDate) すごい遅い!
                    .AsNoTracking() // これがないと、キャッシュしてしまう!
                    .ToList();

        // カテゴリはデータベースに非正規化で保存される。チャネルは展開
        foreach (var x in t) {
/*
            var prod = MyApp.DbContext.Find<Product>(x.ProductId);
            var cat = prod.ItemCategory;
            if (cat.Level != (cat.ParentId != null ? 1 : 0))
                throw new ArgumentException("cat level");
            x.CatL1 = cat.ParentId != null ? cat.Parent.Id : cat.Id;
            x.CatL2 = cat.ParentId != null ? cat.Id : null;
*/
            var channel = x.SalesChannel; //MyApp.DbContext.Find<SalesChannel>(x.SalesChannelId);
            if (channel.Level != (channel.ParentId != null ? 1 : 0))
                throw new ArgumentException("channel level");
            x.ChannelL1 = channel.ParentId != null ? (int) channel.ParentId : channel.Id;
            x.ChannelL2 = channel.ParentId != null ? channel.Id : null;
            //_week_demands.Add(x);
        }

        return t;
    }


    protected static readonly string[] _pvtDimensions = new[] { 
            "category-l1", "category-l2", "product", 
            "channel-l1", "channel-l2", 
            "dt_month", "Date" };  // 計算上の dimension も可

    // 人が見るラベル. 順序に注意
    protected static readonly string[] _row_labels = new string[] {
            "Qty Sold", "Qty Sold (Intermediate)", 
            "Sales Amount", "Avg. Unit Price", "Sales Amount (Intrm)",
            "Sales Cost Amount", "Sales Cost Amt (Intrm)",
            "Unfilled Orders @end" };


    protected static readonly IAggregatorFactory[] _measure_factories = {
                    new SumAggregatorFactory("QtySold"),
                    new SumAggregatorFactory("IntrmQtySold"),
                    new SumAggregatorFactory("SalesAmount"),
                    new FormulaAggregatorFactory("Avg.UnitPrice",
                        (aggs) => { 
                            var amt = (Decimal) aggs[0].Value;
                            var qty = (Decimal) aggs[1].Value;
                            return qty != 0 ? amt/ qty : (Decimal) 0.0; },
                        new[] { new SumAggregatorFactory("SalesAmount"),
                                new SumAggregatorFactory("QtySold") }),
                    new SumAggregatorFactory("IntrmSalesAmount"),
                    new SumAggregatorFactory("SalesCostAmount"),
                    new SumAggregatorFactory("IntrmSalesCostAmount"),
                    new SumAggregatorFactory("UnfilledOrders")
                };


    // @param arg1 [System.Data.Common.DataRecordInternal] 値
    protected static object getValue(object arg1, string dimName)
    {
        var row = (DemandTran) arg1;
        //var dt = (DateOnly) row["sale-date"];
        switch (dimName) 
        {
        case "category-l1": return row.CatL1;
        case "category-l2": return row.CatL2;
        case "product":     return row.ProductId;
        case "channel-l1":  return row.ChannelL1;
        case "channel-l2":     return row.ChannelL2;
        case "dt_month":    return row.dt_month;
        case "Date":    return row.SaleDate;
        //case "month":
        //    return new DateOnly(dt.Year, dt.Month, 1);
        //case "week":
        //    var d = (((int) dt.DayOfWeek) + 6) % 7;
        //    return dt.AddDays(-d);
        case "QtySold":     return row.QtySold;
        case "IntrmQtySold": return row.IntrmQtySold;
        case "SalesAmount": return row.SalesAmount;
        case "IntrmSalesAmount": return row.IntrmSalesAmount;
        case "SalesCostAmount": return row.SalesCostAmount;
        case "IntrmSalesCostAmount": return row.IntrmSalesCostAmount;
        case "UnfilledOrders":  return row.UnfilledOrders;
        default: 
            throw new ArgumentException(); //return row[dimName];
        }
    }


    ///// //////////////////////////////////////////////////////////////////
    // 右側: 時系列 table ペイン

    // 表示している列
    protected List<DateOnly> _dates;

    // 表をクリアするだけ. データベースから読み直さない
    protected void resetTable()
    {
        _dataTable.Columns.Clear();
        _dataTable.Rows.Clear();
        _dataTable.Columns.Add(new DataColumn() { ColumnName = "項目" });
        _dataTable.Columns.Add(new DataColumn() { ColumnName = "PlanVer"});

        DateOnly date, endDate;
        // 実績のときはナル値
        if (MyApp.CurPlanVersion.IsActual ) {
            date = DateOnly.FromDateTime(DateTime.Now).AddYears(-3);
            endDate = DateOnly.FromDateTime(DateTime.Now);
        }
        else { 
            // 計画verのとき: 実績2年間 + 計画期間
            date = ((DateOnly) MyApp.CurPlanVersion.StartDate).AddDays(-(7 * 105)) ;
            endDate = (DateOnly) MyApp.CurPlanVersion.EndDate ;
        }

        var vm = (MainWindowViewModel) Application.Current.MainWindow.DataContext;
        if (vm.TimeStrata == TimeStrataMode.MONTH)  
            date = new DateOnly(date.Year, date.Month, 1);
        else {
            // 月曜日～日曜日に合わせる
            date = Ext.the_monday(date);
            //endDate = endDate.AddDays( (7 - (int) endDate.DayOfWeek) % 7 );
        }

        _dates = new List<DateOnly>();
        for ( ; date <= endDate; 
              date = (vm.TimeStrata == TimeStrataMode.MONTH) ? date.AddMonths(1) :
                                                               date.AddDays(7) ) {  
            // ヘッダに "/" が含まれていると値が表示されない。
            // そんなん分かるか
       //     _dataTable.Columns.Add(new DataColumn() {
       //                     ColumnName = date.ToString().Replace("/", "-")});
            _dates.Add(date);
        }

        RaisePropertyChanged(nameof(DataTableView));
    }

}

}
