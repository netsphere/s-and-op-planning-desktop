﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfPlanning.Models;

namespace WpfPlanning.Views
{

class ProdStructureTemplate
{
    public string Name { get; set; }

    public List<ProdStructureTemplate> Children { get; set; }

    public Product prod;
}

internal class BoMWindowViewModel : MVVM.BindableBase
{
    public ObservableCollection<ProdStructureTemplate> Structures { 
        get; protected set; }

    // コンストラクタ
    public BoMWindowViewModel()
    {
        this.Structures = new ObservableCollection<ProdStructureTemplate>();
        itemsRefresh();
    }

    void addChildren(ProdStructureTemplate tmpl)
    {
        var children = tmpl.prod.bomChildren();
        if (children.Count > 0 ) {
            tmpl.Children = new List<ProdStructureTemplate>();
            foreach ( ProductStructure ps in children ) {
                var childTemplate = new ProdStructureTemplate() {
                                            Name = $"{ps.Child.Description} x {ps.Qty}",
                                            prod = ps.Child, };
                addChildren( childTemplate );
                tmpl.Children.Add( childTemplate );
            }
        }
    }

    void itemsRefresh()
    {
        this.Structures.Clear();

        foreach (Product ps in ProductStructure.roots()) {
            var tmpl = new ProdStructureTemplate() {
                                    Name = ps.Description, prod = ps };
            addChildren(tmpl);
            this.Structures.Add(tmpl);
        }
    }
}


    /// <summary>
    /// BoMWindow.xaml の相互作用ロジック
    /// </summary>
public partial class BoMWindow : Window
{
    public BoMWindow()
    {
        InitializeComponent();
    }

    private void TreeView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
    {

    }
}

}
