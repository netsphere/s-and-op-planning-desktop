﻿
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Windows;
using WpfPlanning.Models;


namespace WpfPlanning.Views
{

class ProductListViewModel : MVVM.BindableBase
{
    public ExObservableCollection<Product> Products { get; protected set; }

    public ItemsSupplier ProductPurchase { get; set; }

    // コンストラクタ
    public ProductListViewModel() 
    {
        Products = new ExObservableCollection<Product>();
        Products.AddRange( MyApp.DbContext.Products
                                          .Include(x => x.ItemCategory) );

        itemsRefresh();
    }

    private void itemsRefresh()
    {
        var query = (from prod in MyApp.DbContext.Products
                     orderby prod.Id
                     select prod).ToList<Product>();
        this.Products.Clear();
        this.Products.AddRange(query);
        RaisePropertyChanged(nameof(Products));
    }
}


    /// <summary>
    /// product_items_window.xaml の相互作用ロジック
    /// </summary>
public partial class ProductListWindow : Window
{
    public ProductListWindow()
    {
        InitializeComponent();
    }

    // [New Product...]
    private void Button_Click(object sender, RoutedEventArgs e)
    {
        var dlg = new EditProductDlg();
        if (dlg.ShowDialog() == true) {
            
        }
    }

}

}
