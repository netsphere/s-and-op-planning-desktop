﻿
using System;
using System.Windows;
using System.Windows.Controls;
using WpfPlanning.Models;


namespace WpfPlanning.Views
{

class ProductViewModel : MVVM.BindableBase
{
    public string ItemCode { get; set; }

    public string Description { get; set; }

    // PN はカテゴリを持たない。
    // TODO: コンボボックス
    public int ItemCategoryId { get; set; }

    //public bool IsPurchasable { get; set; }

    // Not Saleable
    public DateTime? Inactive { get; set; }
}


    /// <summary>
    /// edit_product_dlg.xaml の相互作用ロジック
    /// </summary>
public partial class EditProductDlg : Window
{
    public EditProductDlg()
    {
        InitializeComponent();
    }

    private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
    {

    }

    private void cancelButton_Click(object sender, RoutedEventArgs e)
    {
        DialogResult = false;
    }

    private void okButton_Click(object sender, RoutedEventArgs e)
    {
        var vm = (ProductViewModel) DataContext;
        var product = new Product() {
                ItemCode = vm.ItemCode,
                Description = vm.Description,
                ItemCategoryId = vm.ItemCategoryId,
                //IsPurchasable = vm.IsPurchasable,
                Inactive = vm.Inactive != null ? 
                                DateOnly.FromDateTime((DateTime) vm.Inactive) : null, };
        MyApp.DbContext.Products.Add(product);
        try { 
            MyApp.DbContext.SaveChanges();
        }
        catch (Microsoft.EntityFrameworkCore.DbUpdateException ex) {
            MessageBox.Show(ex.InnerException != null ? 
                                    ex.InnerException.Message : ex.Message);
            return;
        }

        DialogResult = true;
    }
}

}
