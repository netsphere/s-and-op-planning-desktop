﻿
using ClosedXML.Excel;
using Microsoft.Win32;
using System;
using System.IO;
using System.Windows;
using WpfPlanning.Models;


namespace WpfPlanning.Views
{


    /// <summary>
    /// tool_import_master_data_window.xaml の相互作用ロジック
    /// </summary>
public partial class ToolImportMasterDataWindow : Window
{
    const string ExcelFilter = "Excel 2007ファイル (*.xlsx)|*.xlsx";

    // フルパス
    string? _fileName;


    public ToolImportMasterDataWindow()
    {
        InitializeComponent();
    }

    // @return cancel 時false
    bool import_dialog(ImportExcelBase importer)
    {
        var dlg = new OpenFileDialog() { Filter = ExcelFilter };
        if (dlg.ShowDialog() != true) 
            return false;

        _fileName = dlg.FileName;
        //ImportMasterData importer;
        try { 
            FileStream fs = new FileStream(_fileName, FileMode.Open, 
                                           FileAccess.Read, FileShare.ReadWrite);
            importer.open(new XLWorkbook(fs));
        }
        catch (System.IO.IOException ex) { 
            MessageBox.Show(ex.Message); // ほかのプロセスが開いている、など
            _fileName = null;
            return false;
        }

        using (var tran = MyApp.DbContext.Database.BeginTransaction()) {
            try { 
                importer.importMasterDataFile();
                tran.Commit();
            }
            catch (Exception ex) {
                tran.Rollback();
                throw;
            }
        }
        return true;
    }

    // [Master Data...]
    void masterBtn_Click(object sender, RoutedEventArgs e)
    {
        import_dialog(new ImportMasterData());
    }

    // [Update Products/Items...]
    void Button_Click(object sender, RoutedEventArgs e)
    {
        import_dialog(new ImportProductMaster());
    }

    // [Update item categories...]
    void Button_Click_1(object sender, RoutedEventArgs e)
    {
        import_dialog(new ImportItemCategories());
    }
}
}
