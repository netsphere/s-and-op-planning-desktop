﻿
using graph_app;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using WpfPlanning.Models;


namespace WpfPlanning.Views
{

class MrpViewModel : MVVM.BindableBase
{

}


    /// mrp_window.xaml の相互作用ロジック
    /// </summary>
public partial class MrpWindow : Window
{
    public MrpWindow()
    {
        InitializeComponent();
    }

    // [Run!]
    void Button_Click(object sender, RoutedEventArgs e)
    {
        PlanVersion planVer = (PlanVersion) MyApp.CurPlanVersion;
        DateOnly start_day = (DateOnly) planVer.StartDate;
        DateOnly horizon_end = (DateOnly) planVer.EndDate;

        MyApp.MrpResults = new Mrp() ;

// 制限:
// - `WITH` 句と `Include()` は両立できない。サブクエリにする
//   See https://stackoverflow.com/questions/47122425/error-when-using-fromsql-and-include-the-include-operation-is-not-supported-wh
//   または, 後から1件ずつ load する。ヒドい
// - `ORDER BY` を含めることはできない。後から自分でソートする
//   Microsoft.Data.SqlClient.SqlException:
//   'The ORDER BY clause is invalid in views, inline functions, derived tables, subqueries, and common table expressions, unless TOP, OFFSET or FOR XML is also specified.'

// ● いったん, 週で決め打ち  TODO: fixme
// saleschannel -> location に括り直す。
        List<DemandTran> demands = MyApp.DbContext.DemandTran
                                        .FromSql(
$@"
SELECT MAX(T.Id) AS Id, PlanVersionId, ProductId, 
    MAX(T.SalesChannelId) AS SalesChannelId, 
    SUM(QtySold) AS QtySold,
    SUM(SalesAmount) AS SalesAmount,
    SUM(SalesCostAmount) AS SalesCostAmount, 
    SUM(UnfilledOrders) AS UnfilledOrders, dt_week AS SaleDate
FROM (
    SELECT DATEADD(DAY, -((DATEPART(weekday, SaleDate) + @@DATEFIRST - 2) % 7),SaleDate) AS dt_week,
           *
    FROM dbo.demand_tran) T
    LEFT JOIN SalesChannels U ON T.SalesChannelId = U.Id 
WHERE PlanVersionId = {planVer.Id} AND (dt_week BETWEEN {start_day} AND {horizon_end})
GROUP BY PlanVersionId, ProductId, LocationId, dt_week
-- ORDER BY SaleDate 
")
                     .Include(x => x.SalesChannel.Location)
                     .Include(x => x.Product)
                     .OrderBy(x => x.SaleDate)
                     .AsNoTracking() // これがないと、キャッシュしてしまう!
                     .ToList();
        //demands.Sort((a, b) =>  a.SaleDate.CompareTo(b.SaleDate)); // 破壊的メソッド

        // horizon_start の1週間前の日付で在庫データ作る
        List<ShipmentTran> shipments = new List<ShipmentTran>();

        // 出発点となる, 実需の発生する品目と locations.
        var iloc_list = MyApp.DbContext.DemandTran
                .Where(x => x.PlanVersionId == planVer.Id && 
                            x.SaleDate >= start_day && x.SaleDate <= horizon_end)
                .Include(x => x.Product)
                .Include(x => x.SalesChannel.Location) 
                .Select(x => new ItemAndLocation {
                                            Item     = x.Product, 
                                            Location = x.SalesChannel.Location})
                .Distinct()
                .AsNoTracking()
                .ToList<ItemAndLocation>();

        MyApp.MrpResults.run2(start_day, horizon_end, demands, shipments,
                              iloc_list);

        MessageBox.Show("done");
    }
}

} // namespace WpfPlanning.Views
