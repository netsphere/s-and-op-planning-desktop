﻿
using CsvHelper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using WpfPlanning.Models;

namespace WpfPlanning.Views
{

[System.Windows.Data.ValueConversion(typeof(DateOnly), typeof(DateTime))]
class DateOnlyToDateTimeConverter : IValueConverter
{
    // @override
    // vm -> wpf control
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        if (value == null)
            return DependencyProperty.UnsetValue; // 値を設定しない

        DateOnly date = (DateOnly) value ;
        return date.ToDateTime(TimeOnly.MinValue);
    }

    // @override
    // wpf control -> vm
    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        if (value == null)
            return DependencyProperty.UnsetValue; // 値を設定しない

        DateTime date = (DateTime) value; 
        return DateOnly.FromDateTime(date);
    }
}


class BaselineForecastViewModel : MVVM.BindableBase
{
    public string Name { get; set; }

    public DateOnly HorizonStart { get; set; }

    public DateOnly HorizonEnd { get; set; }

    string _trainFile;
    public string TrainFile { 
        get => _trainFile;
        set { SetPropertyAndRaise(ref _trainFile, value); }
    }

    string _predictionFile;
    public string PredictionFile { 
        get => _predictionFile;
        set { SetPropertyAndRaise(ref _predictionFile, value); }
    }

    public void saveTrainFile()
    {
        Models.PlanVersion actualVer = MyApp.DbContext.PlanVersions
                                            .Where(x => x.IsActual).First();
        DateOnly start = HorizonStart.AddDays(-(7 * 157));

        // 週だけで作る
        // @note SQL Server は, GROUP BY 句に AS 別名を使えない。一時表を作ってやる
        // Id, PlanVersionId は必須フィールドになっている。
        // CatL1, CatL2 は [NotMapped] になっており反映されない。 ●● 正規化ゆるめるべきか?
        var t = MyApp.DbContext.DemandTran
                     .FromSql(
$@"WITH
  tmp AS (
    SELECT DATEADD(DAY, -((DATEPART(weekday, SaleDate) + @@DATEFIRST - 2) % 7),SaleDate) AS dt_week,
           *
    FROM dbo.demand_tran)
SELECT MAX(tmp.Id) AS Id, PlanVersionId, ProductId, -- ItemCategoryId AS CatL2,
    SalesChannelId, 
    SUM(QtySold) AS QtySold,
    SUM(SalesAmount) AS SalesAmount,
    SUM(SalesCostAmount) AS SalesCostAmount, 
    SUM(UnfilledOrders) AS UnfilledOrders, dt_week AS SaleDate
FROM tmp  
    -- LEFT JOIN products ON tmp.productId = products.id
WHERE PlanVersionId = {actualVer.Id} AND 
      SaleDate BETWEEN {start} AND {HorizonStart.AddDays(-1)}
GROUP BY PlanVersionId, ProductId, -- ItemCategoryId, 
         SalesChannelId, dt_week
ORDER BY SaleDate 
")
                     //.AsEnumerable()
                     //.Include(x => x.Product)  "構成可能な SQL クエリ" (= `SELECT` で始める) でなければならない
                     .AsNoTracking() // これがないと、キャッシュしてしまう!
                     /*.AsEnumerable()*/; //.ToList();

        Dictionary<int, Product> prods = MyApp.DbContext.Products
                    .ToDictionary(x => x.Id);

        // Cube の表の形に作り直す
        // 日付は末日にする
        using (StreamWriter sw = new StreamWriter(TrainFile, false)) {
            // 存在する組み合わせのみリストアップ
            Dictionary<Tuple<int, int>, bool> done = new Dictionary<Tuple<int, int>, bool>();

            sw.WriteLine("ds,product,category,channel,y");

            var dt = start; // 隙間を埋める
            foreach (var r in t) {
                while (dt < r.SaleDate) {
                    sw.WriteLine(
$"{dt.AddDays(6)},{r.ProductId},{prods[r.ProductId].ItemCategoryId},{r.SalesChannelId},0");
                    dt = dt.AddDays(+7);
                }
                sw.WriteLine(
$"{r.SaleDate.AddDays(6)},{r.ProductId},{prods[r.ProductId].ItemCategoryId},{r.SalesChannelId},{r.QtySold}");
                done[new Tuple<int,int>(r.ProductId, r.SalesChannelId)] = true;
                if (r.SaleDate == HorizonStart.AddDays(-7))
                    done[new Tuple<int,int>(r.ProductId, r.SalesChannelId)] = false;

                if (dt == r.SaleDate)
                    dt = dt.AddDays(+7);
            }
            // 間を埋める
            for ( ; dt < HorizonStart.AddDays(-7); dt = dt.AddDays(7) ) {
                var r = t.First();
                sw.WriteLine(
$"{dt.AddDays(6)},{r.ProductId},{prods[r.ProductId].ItemCategoryId},{r.SalesChannelId},0");
            }
            // Python 側の horizon 指定が int. 全部の組み合わせが必要。
            dt = HorizonStart.AddDays(-1);
            foreach ( var kv in done) {
                if (kv.Value) { 
                    //Product p = MyApp.DbContext.Products.Find(kv.Key.Item1);
                    sw.WriteLine(
$"{dt},{kv.Key.Item1},{prods[kv.Key.Item1].ItemCategoryId},{kv.Key.Item2},0");
                }
            }
        }
    }


    internal void load_prediction_and_create_planvar()
    {
        DateOnly today = DateOnly.FromDateTime(DateTime.Now);
        PlanVersion newPlan = new PlanVersion() { 
                Name = $"Plan {HorizonStart} - {HorizonEnd} @{today}",
                ForecastDate = today,
                StartDate = HorizonStart,
                EndDate = HorizonEnd, };

        using (var tran = MyApp.DbContext.Database.BeginTransaction()) 
        try { 
            MyApp.DbContext.PlanVersions.Add(newPlan);
            MyApp.DbContext.SaveChanges();

            using (var reader = new StreamReader(_predictionFile)) 
            using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture)) {
                csv.Read();
                csv.ReadHeader();
                while (csv.Read()) {
                    string[] uid = csv.GetField("unique_id").Split('/');
                    if (uid.Length  < 4)
                        continue;
                    DateOnly date = DateOnly.Parse(csv.GetField("ds"));
                    date = date.AddDays(-6); // 月曜日に戻す
                    double qty = csv.GetField<double>("HoltWinters/ERM_method-closed_lambda_reg-0.01");
                    if (qty < 0)
                        qty = 0;

                    var r = new DemandTran() {
                                    PlanVersionId = newPlan.Id,
                                    SaleDate = date,
                                    ProductId = int.Parse(uid[3]),
                                    SalesChannelId = int.Parse(uid[1]),
                                    QtySold = qty,
                                    SalesAmount = qty * 10 , // TODO: impl.
                                    SalesCostAmount = 0 ,
                                    UnfilledOrders = 0,
                            };
                    MyApp.DbContext.DemandTran.Add(r);
                }
            }
            MyApp.DbContext.SaveChanges();
            tran.Commit();
        }
        catch (Exception ex) {
            tran.Rollback();
            throw;
        }
    }
}


// 文字列があること [汎用ルーチン]
public class ValidatesPresence : ValidationRule
{
    public override ValidationResult Validate(object value,
                                System.Globalization.CultureInfo cultureInfo)
    {
        string? b = value as string;
        if ( String.IsNullOrWhiteSpace(b) )
            return new ValidationResult(false, "値が必須");

        return ValidationResult.ValidResult;
    }
}


    /// <summary>
    /// baseline_forecast_window.xaml の相互作用ロジック
    /// </summary>
public partial class BaselineForecastDlg : Window
{
    public BaselineForecastDlg()
    {
        InitializeComponent();
    }


    /// /////////////////////////////////////////////////////////////
    // Event handlers

    // train select
    void Button_Click(object sender, RoutedEventArgs e)
    {
        var ofd = new SaveFileDialog() { Filter = "CSV Files (.csv)|*.csv" };
        if (ofd.ShowDialog() == true) {  
            var vm = (BaselineForecastViewModel) DataContext;
            vm.TrainFile = ofd.FileName;
        }
    }

    // prediction select
    void Button_Click_1(object sender, RoutedEventArgs e)
    {
        OpenFileDialog ofd = new OpenFileDialog() { 
                                    Filter = "CSV Files (.csv)|*.csv" };
        if (ofd.ShowDialog() == true) {  
            var vm = (BaselineForecastViewModel) DataContext;
            vm.PredictionFile = ofd.FileName;
        }
    }

    void cxlBtn_Click(object sender, RoutedEventArgs e)
    {
        DialogResult = false;
    }

    void okBtn_Click(object sender, RoutedEventArgs e)
    {
        var vm = (BaselineForecastViewModel) DataContext;

        if (steps.SelectedIndex == 0) {
            vm.saveTrainFile();
            okBtn.Content = "Create Plan";
            steps.SelectedIndex = 1;
        }
        else {
            vm.load_prediction_and_create_planvar();
            DialogResult = true;
        }
    }
}

}
