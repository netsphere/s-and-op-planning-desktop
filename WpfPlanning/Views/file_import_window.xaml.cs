﻿
using ClosedXML.Excel;
using Microsoft.Win32;
using System;
using System.Collections.ObjectModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using WpfPlanning.Models;


namespace WpfPlanning.Views
{

internal class FileImportViewModel : MVVM.BindableBase
{
    // コンストラクタ
    public FileImportViewModel() 
    { 
        _sheets = new ObservableCollection<IXLWorksheet>();
        _dataTable1 = new DataTable();
    }

    // コンボボックスに bind
    private ObservableCollection<IXLWorksheet> _sheets;
    public ObservableCollection<IXLWorksheet> Sheets {
        get => _sheets;
        set {
            SetPropertyAndRaise(ref _sheets, value);
        }
    }

    private readonly DataTable _dataTable1;
    public DataView DataTableView => new DataView(_dataTable1);

}


    /// <summary>
    /// file_import_window.xaml の相互作用ロジック
    /// </summary>
public partial class FileImportWindow : Window
{
    // フルパス
    string? _fileName;


    // コンストラクタ
    public FileImportWindow()
    {
        InitializeComponent();
    }


    // [Transactional Data...]
    private void transactionalBtn_Click(object sender, RoutedEventArgs e)
    {
        var dlg = new OpenFileDialog() { 
                                Filter = "Excel 2007ファイル (*.xlsx)|*.xlsx" };
        if (dlg.ShowDialog() != true) 
            return;
            
        _fileName = dlg.FileName;
        ImportTransactionData importer;
        try { 
            FileStream fs = new FileStream(_fileName, FileMode.Open, 
                                               FileAccess.Read, FileShare.ReadWrite);
            PlanVersion pln = MyApp.DbContext.PlanVersions
                                    .Where(x => x.IsActual == true).First();
            importer = new ImportTransactionData(pln);
            importer.open(new XLWorkbook(fs));
        }
        catch (System.IO.IOException ex) { 
            MessageBox.Show(ex.Message); // ほかのプロセスが開いている、など
            _fileName = null;
            return;
        }

        //updateSheetsAndNext();
        using (var tran = MyApp.DbContext.Database.BeginTransaction()) {
            try { 
                importer.importMasterDataFile();
                tran.Commit();
            }
            catch (Exception ex) {
                tran.Rollback();
                throw;
            }
        }
    }

/*
    void updateSheetsAndNext()
    {
        var vm = (FileImportWindowViewModel) DataContext;
        vm.Sheets.Clear();

        foreach (var sheet in _workbook.Worksheets)
            vm.Sheets.Add(sheet);

        cbSheet.SelectedItem = vm.Sheets[0];
    }
*/

    void cbSheet_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
        var vm = (FileImportViewModel) DataContext;
        
        var dataTable = vm.DataTableView.Table;
        dataTable.Columns.Clear();
        dataTable.Rows.Clear(); // これも必要!

        var sheet = (IXLWorksheet) cbSheet.SelectedItem;
        if (sheet == null)
            return;

        foreach (var wk_c in sheet.Columns() ) {
            // 列番号, 行番号とも 1 始まりに注意
            // TODO: impl.
        }
    }

    // バージョン依存マスタの読み込み
    void Button_Click(object sender, RoutedEventArgs e)
    {
        // TODO: impl.
    }

    private void Button_Click_1(object sender, RoutedEventArgs e)
    {

    }
}

}
