﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfPlanning.Models;


namespace WpfPlanning.Views
{

/*
class LocationOrSupplier {
    // "<all>" はどっちも null
    readonly Location? _location;
    readonly ItemsSupplier? _supplier;

    public string Code { 
        get {
            return _supplier != null ? _supplier.SupplierCode : 
                   (_location != null ? _location.LocationCode : "<all>");
        } 
    }
}
*/

class TransferOrdersViewModel : MVVM.BindableBase
{
    public ExObservableCollection<TransferOrder> Orders { get; protected set; }

    public ExObservableCollection<Location> ShipFroms { 
        get; protected set; }

    public ExObservableCollection<Location> ShipTos { 
        get; protected set; }

    Location _ship_from_selected;
    public Location ShipFromSelected { 
        get { return _ship_from_selected ; }
        set { 
            bool changed = SetPropertyAndRaise(ref _ship_from_selected, value); 
            if (changed)
                refreshPivotTable();
        }
    }

    Location _ship_to_selected;
    public Location ShipToSelected { 
        get { return _ship_to_selected ; }
        set { 
            bool changed = SetPropertyAndRaise(ref _ship_to_selected, value); 
            if (changed)
                refreshPivotTable();
        }
    }

    public TransferOrdersViewModel() {
        Orders = new ExObservableCollection<TransferOrder>();
        ShipFroms = new ExObservableCollection<Location>();
        ShipTos = new ExObservableCollection<Location>();
    }

    void refreshPivotTable()
    {
        // TODO: impl.
    }
}

    /// purchase_orders_window.xaml の相互作用ロジック
    /// </summary>
public partial class TransferOrdersWindow : Window
{
    public TransferOrdersWindow()
    {
        InitializeComponent();
    }
}
}
