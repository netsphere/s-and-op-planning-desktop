﻿
using graph_app;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using WpfPlanning.Models;


namespace WpfPlanning.Views
{

/*
逆BoM Tree  -- roll-up するために、普通の BoM ではない
  テーブル 4
     + テーブル単品 2
     + テーブル+チャア x2 => 2
  チェア   11
     + チェア単品 3
     + テーブル+チャア x2 => 8
*/
class SupplyItemTemplate : TmplModel
{
    // BoM 展開
    public ObservableCollection<SupplyItemTemplate>? Children { get; set; }

    public override bool HasChildren()  {
        return Children != null && Children.Count > 0;
    }

    // 辿れるようにする
    // Root (All items) は null
    public readonly Product? Product;

    // Root, 第一階層は null
    public readonly ProductStructure? bomParent;


    // コンストラクタ
    public SupplyItemTemplate(Product? prod, ProductStructure? parent)
    {
        bomParent = parent;

        if (prod != null) {
            this.Product = prod;
            this.bomParent = parent;
            this.Name = prod.Description;
        }
        else 
            this.Name = "All items";
    }


    public static void add_children(SupplyItemTemplate container, 
                                    Product? self_prod)
    {
        List<Product> items;

        // Root の場合
        if (self_prod == null) {
            // こうじゃない。セット品も需給がある。=> IsSaleable なもの全部
            items = MyApp.DbContext.Products
                         .Where(p => p.IsSaleable).ToList();
            container.Children = new ObservableCollection<SupplyItemTemplate>();
            foreach (Product item in items) { 
                var tmpl = new SupplyItemTemplate(item, null);
                add_children(tmpl, item);
                container.Children.Add(tmpl);
            }

            return;
        }

        var children = self_prod.bomChildren();
        if (children.Count > 0) { 
            container.Children = new ObservableCollection<SupplyItemTemplate>();
            foreach (var ps in children) {
                var sub = new SupplyItemTemplate(ps.Child, ps);
                add_children(sub, ps.Child);
                container.Children.Add(sub);
            }
        }
    }
}


internal class SupplyPageViewModel : MVVM.BindableBase
{
    // 仮想 location = 需要が発生する単位
    public ObservableCollection<Location> Locations { get; protected set; }
    private Location _locationSelected;
    public Location LocationSelected { 
        get { return _locationSelected; }
        set { 
            bool changed = SetPropertyAndRaise(ref _locationSelected, value);
            if (changed) {    
                //refreshSupplierList();  
                refreshPivotTable(); // 時系列表の更新
            }
        }
    }

    // 左側ペイン。コレクションでないといけない
    public ObservableCollection<SupplyItemTemplate> SupplyItems { 
        get; protected set; }
    public SupplyItemTemplate ItemSelected { get; set; }

    // Point! こちらは private にする.
    private readonly DataTable _dataTable = new DataTable();
    // こちらを <DataGrid> に bind する
    // 注意! このような使い捨ての書き方にしなければならない。
    public DataView MpsTableView => new DataView(_dataTable); 


    // コンストラクタ
    public SupplyPageViewModel()
    {
        this.Locations = new ObservableCollection<Location>();
        Locations.Add(new Location() { Description = "<all loc>"});
        foreach (var loc in MyApp.DbContext.Locations)
            Locations.Add(loc);
        _locationSelected = Locations.First();

        //this.Suppliers = new ObservableCollection<LocationOrSupplier>();

        this.SupplyItems = new ObservableCollection<SupplyItemTemplate>();
    }

    // データベースから読み直して、ページを reload する
    public void refresh()
    {
        //loadSupplies();

        // 左側: ツリー
        SupplyItems.Clear();

        // Root は総合計
        var tmpl = new SupplyItemTemplate(null, null);
        SupplyItemTemplate.add_children(tmpl, null);
        SupplyItems.Add(tmpl);

        // 右側: 単にクリア 
        reloadMpsTable();
    }


    // 計画期間の分をつくってしまう. 編集はこれを更新していく.
    //List<PurchaseOrder> _week_orders;
    //List<ShipmentTran> _week_shipments;
/*
    void loadSupplies()
    {
        // TODO: ●●先にMRPを走らせる??       --> 重そう。ボタンを押して実行

        var t = MyApp.DbContext.ShipmentTran
                    .FromSql(
$@"WITH
  tmp AS (
    SELECT CAST(FORMAT(SaleDate, 'yyyy-MM-01') AS DATE) AS dt_month,
           DATEADD(DAY, -((DATEPART(weekday, SaleDate) + @@DATEFIRST - 2) % 7),SaleDate) AS dt_week,
           *
    FROM dbo.demand_tran)
SELECT MAX(tmp.Id) AS Id, PlanVersionId, ItemCategoryId, ProductId, SalesChannelId, 
    SUM(QtySold) AS QtySold, -- SUM(CalcQtySold) AS CalcQtySold, 
    SUM(SalesAmount) AS SalesAmount, -- SUM(CalcSalesAmount) AS CalcSalesAmount, 
    SUM(SalesCostAmount) AS SalesCostAmount, 
    -- SUM(CalcSalesCostAmount) AS CalcSalesCostAmount, 
    SUM(UnfilledOrders) AS UnfilledOrders, dt_week AS SaleDate, dt_month
FROM tmp -- dbo.demand_tran 
    LEFT JOIN products ON tmp.productId = products.id 
WHERE PlanVersionId = {MyApp.CurPlanVersion.Id}
GROUP BY PlanVersionId, ItemCategoryId, ProductId, SalesChannelId, dt_month, dt_week
ORDER BY SaleDate 
")
            .AsNoTracking()
            .ToList();
        _week_shipments = new List<ShipmentTran>();
    }

    ///// //////////////////////////////////////////////////////////////////
    // 右側: table

    // サプライヤ or DC リストを更新
    void refreshSupplierList() {
        this.Suppliers.Clear();
        Suppliers.Add(new LocationOrSupplier()); // "<all>"
        this.SupplierSelected = Suppliers.First();
    }
*/

/*
    private void NotifyTableUpdate()
    {
        // ここは DataView のほう. DataView を bind しているため。
        RaisePropertyChanged(nameof(MpsTableView));
    }
*/

    void reloadMpsTable() {
        DateOnly horizon_start = (DateOnly) MyApp.CurPlanVersion.StartDate;
        DateOnly horizon_end = (DateOnly) MyApp.CurPlanVersion.EndDate;

        //●●MrpResultsが null でも、実績部分は、在庫動向を表示したい

        printExpandedDemands(horizon_start, horizon_end, MyApp.MrpResults);
    }

    static readonly string[] row_names = new[] {
        "GrossReq", "Issued Orders", "Proposal Recieved", "Consumed", "Ending Stock", "Uncovered"
        };

    // 所要量計算の結果を表示
    void printExpandedDemands(DateOnly horizon_start, DateOnly horizon_end,
                              Mrp mrpResults)
    {
        if (mrpResults == null)
            throw new ArgumentNullException(nameof(mrpResults));

        Dictionary<ItemAndLocation, MpsPlan[]> demands = mrpResults.ExpandedDemands;

        _dataTable.Columns.Clear();
        _dataTable.Rows.Clear();
        _dataTable.Columns.Add("Item");
        _dataTable.Columns.Add("Location");
        _dataTable.Columns.Add("内容");

        // 先に列を全部作る。
        for (DateOnly dt = horizon_start.AddDays(-7); dt <= horizon_end; 
                                                        dt = dt.AddDays(7)) {
            // workaround: ヘッダに "/" が含まれると値が表示されない
            _dataTable.Columns.Add(new DataColumn() { 
                                    ColumnName = dt.ToString().Replace("/", "-")});
        }

            // 行を作っていく
        foreach (ItemAndLocation iloc in mrpResults.Reached) { 
            var rows = new List<DataRow>();
            foreach (var t_name in row_names) {
                var row = _dataTable.NewRow();
                row[0] = iloc.Item.Description;
                row[1] = iloc.Location.Description;
                row[2] = t_name;
                rows.Add(row);
            }

            for (int i = 0; i < demands[iloc].Count(); ++i) {
                var d = demands[iloc][i];            
                rows[0][i + 3] = d.GrossReq;
                rows[1][i + 3] = d.IssuedOrders;
                rows[2][i + 3] = d.PrpslRecieved;
                rows[3][i + 3] = d.Consumed;
                rows[4][i + 3] = d.Stock;
                rows[5][i + 3] = d.Uncovered;
            }
            foreach (var row in rows) 
                _dataTable.Rows.Add(row);
        }

        RaisePropertyChanged(nameof(MpsTableView));
    }

    internal void refreshPivotTable()
    {
        //throw new NotImplementedException();
    }
}


    /// <summary>
    /// supply_page.xaml の相互作用ロジック
    /// </summary>
public partial class SupplyPage : Page
{
    public SupplyPage()
    {
        InitializeComponent();

        var cbs = new CommandBinding[] {
            new CommandBinding(MyCommands.PurchaseOrdersWindowCommand,
                               onPurchaseOrdersWindow),
            new CommandBinding(MyCommands.TransferOrdersWindowCommand,
                               onTransferOrdersWindow),
            new CommandBinding(MyCommands.WorkOrdersWindowCommand,
                               onWorkOrdersWindow),
            };                               
        foreach (var cmd in cbs)
            CommandBindings.Add(cmd);
    }


    /// /////////////////////////////////////////////////////////////////////
    // Command Handlers

    void onWorkOrdersWindow(object sender, ExecutedRoutedEventArgs e)
    {
        var wnd = new WorkOrdersWindow();
        WorkOrdersViewModel vm = (WorkOrdersViewModel) wnd.DataContext;

        wnd.Show();
    }

    void onTransferOrdersWindow(object sender, ExecutedRoutedEventArgs e)
    {
        var wnd = new TransferOrdersWindow();
        TransferOrdersViewModel vm = (TransferOrdersViewModel) wnd.DataContext;
        wnd.Show();
    }

    void onPurchaseOrdersWindow(object sender, ExecutedRoutedEventArgs e)
    {
        var wnd = new PurchaseOrdersWindow();
        PurchaseOrdersViewModel vm = (PurchaseOrdersViewModel) wnd.DataContext;

        HashSet<string> codes = new HashSet<string>();
        foreach (var order in MyApp.MrpResults.PurchaseOrders) { 
            codes.Add(order.SupplierCode);
            vm.Orders.Add(order);
        }
        vm.SupplierCodes.AddRange(codes);

        wnd.Show();
    }


    /// /////////////////////////////////////////////////////////////////////
    // Event Handlers

    void Page_Loaded(object sender, RoutedEventArgs e)
    {

    }

    void TreeView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
    {

    }

    void DataGrid_CellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
    {

    }
}

}
