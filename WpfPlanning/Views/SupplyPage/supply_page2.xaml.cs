﻿
// 実績の需要, 出荷(供給), 在庫を表示する

using System.Windows;
using System.Windows.Controls;


namespace WpfPlanning.Views
{

    /// supply_page.xaml の相互作用ロジック
    /// </summary>
public partial class SupplyPage2 : Window
{
    public SupplyPage2()
    {
        InitializeComponent();
    }


    /// /////////////////////////////////////////////////////////////////////
    // Event Handlers

    void Page_Loaded(object sender, RoutedEventArgs e)
    {

    }

    // 商品カテゴリ tree での項目選択
    void TreeView_SelectedItemChanged(object sender, 
                                      RoutedPropertyChangedEventArgs<object> e)
    {
        var vm = (SupplyPage2ViewModel) DataContext;
        if ( e.NewValue is DemandCatTemplate dcit ) {
            vm.SelectedCatItem = dcit;
        }
    }

    void DataGrid_CellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
    {

    }

    // 品目リストの選択
    void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
        // イベントハンドラの「後」で view model が更新される. CollectionChanged を使え
        // empty.
    }

    // <All items>
    void Button_Click(object sender, RoutedEventArgs e)
    {
        lbProducts.SelectedItems.Clear();
    }
}

}
