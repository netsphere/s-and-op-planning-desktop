﻿
using Microsoft.EntityFrameworkCore;
using NReco.PivotData;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Windows;
using WpfPlanning.common;
using WpfPlanning.Models;


namespace WpfPlanning.Views
{

// 需要と供給の両方を表示。表示のみ。
internal class SupplyPage2ViewModel : CommonPlanViewModel
{
    // 左下 pane: 仮想 location = 需要が発生する単位 ////////////////////////////

    public ObservableCollection<Location> Locations { get; protected set; }

    private Location _locationSelected;
    public Location LocationSelected { 
        get { return _locationSelected; }
        set { 
            bool changed = SetPropertyAndRaise(ref _locationSelected, value);
            if (changed) {    
                //refreshSupplierList();  
                refreshPivotTable(); // 時系列表の更新, 品目リストペインはそのまま.
            }
        }
    }


    // 品目リストペイン ///////////////////////////////////////////////////////

    // 時系列ペイン ///////////////////////////////////////////////////////////


    // コンストラクタ
    public SupplyPage2ViewModel()
    {
        // 左下 pane: 仮想 locations
        this.Locations = new ObservableCollection<Location>();
        Locations.Add(new Location() { Description = "<all loc>"});
        foreach (var loc in MyApp.DbContext.Locations)
            Locations.Add(loc);
        _locationSelected = Locations.First();
    }

    // NReco.PivotData で集約する元リスト
    List<TranBase> _week_demands_shipments;

    PivotData _psiPvtData;


    // データベースから読み直して、ページを reload する
    public void initDynamicPanes()
    {
        loadDemandsAndShipments();
        buildPivot();

        // 品目リストペイン
        clearProductList();

        // 右側: 単にクリア 
        resetTable();
    }


    static List<ShipmentTran> read_shipment_table(PlanVersion planVer,
                DateOnly start, DateOnly end)
    { 
        // NReco.PivotData 側で在庫数を足し上げてしまわないように,
        // 期間ごとにジャストで 1 レコードだけを出力するようにする

        List<ShipmentTran> t = MyApp.DbContext.ShipmentTran
                     .FromSql(
$@"
SELECT MAX(tmp.Id) AS Id, PlanVersionId, CatL1, CatL2, ProductId, LocationId, 
    SUM(QtyShipped) AS QtyShipped,
    SUM(QtyConsumed) AS QtyConsumed,
    SUM(RevenueAmount) AS RevenueAmount,
    SUM(DeliveryCostAmount) AS DeliveryCostAmount,
    SUM(TranProfit) AS TranProfit,
    AVG(OnHand2) AS OnHand, dt_week AS ShipmentDate  --, dt_month
FROM ( 
    SELECT -- CAST(FORMAT(SaleDate, 'yyyy-MM-01') AS DATE) AS dt_month,
        DATEADD(DAY, -((DATEPART(weekday, ShipmentDate) + @@DATEFIRST - 2) % 7),
                ShipmentDate) AS dt_week,
        LAST_VALUE(OnHand) OVER(
                PARTITION BY PlanVersionId, ProductId, LocationId, 
                             DATEADD(DAY, -((DATEPART(weekday, ShipmentDate) + @@DATEFIRST - 2) % 7), ShipmentDate)
                ORDER BY ShipmentDate
                ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS OnHand2,
           *
    FROM dbo.shipment_tran) AS tmp
WHERE PlanVersionId = {planVer.Id} AND 
      (dt_week BETWEEN {start} AND {end})   -- ここが週前提
GROUP BY PlanVersionId, CatL1, CatL2, ProductId, LocationId, dt_week -- dt_month, 
ORDER BY ShipmentDate 
")
                     .AsNoTracking() // これがないと、キャッシュしてしまう!
                     .ToList();

        // 非正規化 -> 商品カテゴリはインポート時に展開ずみ.        

        return t;
    }


    // データベースから読み込む。`_week_demands_shipments` を作り直す
    void loadDemandsAndShipments()
    {
        // 両方を一つのリストに
        List<TranBase> t = new List<TranBase>();

        if (MyApp.CurPlanVersion.IsActual) {
            // 実績のみ。3年間 = 157週間
            DateOnly this_monday = Ext.the_monday(
                                        DateOnly.FromDateTime(DateTime.Now));
            t.AddRange(read_demand_table(MyApp.CurPlanVersion, 
                            this_monday.AddDays(-(7 * 157)), this_monday.AddDays(-1)) );
            t.AddRange(read_shipment_table(MyApp.CurPlanVersion,
                            this_monday.AddDays(-(7 * 157)), this_monday.AddDays(-1)) );
        }
        else { 
            // 計画verの場合は、実績 2年と overlay する
            PlanVersion actualVer = MyApp.DbContext.PlanVersions
                                            .Where(x => x.IsActual).First();
            DateOnly horizonStart = (DateOnly) MyApp.CurPlanVersion.StartDate;
            t.AddRange( read_demand_table(actualVer, horizonStart.AddDays(-(7 * 105)),
                                  horizonStart.AddDays(-1)) ); 
            t.AddRange(read_demand_table(MyApp.CurPlanVersion,
                                         (DateOnly) MyApp.CurPlanVersion.StartDate,
                                         (DateOnly) MyApp.CurPlanVersion.EndDate));

            t.AddRange( read_shipment_table(actualVer, horizonStart.AddDays(-(7 * 105)),
                                  horizonStart.AddDays(-1)) ); 
            t.AddRange(read_shipment_table(MyApp.CurPlanVersion,
                                         (DateOnly) MyApp.CurPlanVersion.StartDate,
                                         (DateOnly) MyApp.CurPlanVersion.EndDate));
        }

        _week_demands_shipments = t;
    }

    
    static readonly string[] _pvtDimensions = CommonPlanViewModel._pvtDimensions
                     .Concat(new[] {"location"}).ToArray();

    static readonly IAggregatorFactory[] _measure_factories = 
            CommonPlanViewModel._measure_factories.Concat(new [] {
                    new SumAggregatorFactory("QtyShipped"), 
                    new SumAggregatorFactory("QtyConsumed"), 
                    new SumAggregatorFactory("RevenueAmount"), 
                    new SumAggregatorFactory("DeliveryCostAmount"),
                    new SumAggregatorFactory("TranProfit"),
                    new SumAggregatorFactory("OnHand") }
                ).ToArray();


    // `_week_demands_shipments` から `_psiPvtData` をキャッシュ構築する
    void buildPivot()
    {
        _psiPvtData = new PivotData(
                _pvtDimensions,
                new CompositeAggregatorFactory(_measure_factories));
        _psiPvtData.ProcessData(_week_demands_shipments, getValue); 
    }


    // @param arg1 [System.Data.Common.DataRecordInternal] 値
    static object getValue(object arg1, string dimName)
    {
        if ( arg1 is DemandTran ) {
            switch (dimName)
            { 
            case "location": 
                return ((DemandTran) arg1).SalesChannel.LocationId;
            case "QtyShipped":
            case "QtyConsumed":
            case "RevenueAmount":
            case "DeliveryCostAmount":
            case "TranProfit":
            case "OnHand":
                return 0;
            default:    
                return CommonPlanViewModel.getValue(arg1, dimName);
            }
        }

        var row = (ShipmentTran) arg1 ;

        switch (dimName) 
        {
        case "category-l1": return row.CatL1;
        case "category-l2": return row.CatL2;
        case "product":     return row.ProductId;
        case "location":  return row.LocationId ;
        case "dt_month":    return row.dt_month;
        case "Date":    return row.ShipmentDate;

        // メジャ
        case "QtySold":    
        case "IntrmQtySold": 
        case "SalesAmount": 
        case "IntrmSalesAmount":
        case "SalesCostAmount": 
        case "IntrmSalesCostAmount":
        case "UnfilledOrders": 
            return 0;
        case "QtyShipped":     return row.QtyShipped;
        case "QtyConsumed":    return row.QtyConsumed;
        case "RevenueAmount": return row.RevenueAmount;
        case "DeliveryCostAmount": return row.DeliveryCostAmount ;
        case "TranProfit":  return row.TranProfit ;
        case "OnHand":      return row.OnHand ;
        default: 
            throw new ArgumentException(); //return row[dimName];
        }
    }


    ///// //////////////////////////////////////////////////////////////////
    // 右側: 時系列 table ペイン

    static readonly string[] _row_labels = 
                CommonPlanViewModel._row_labels.Concat(new [] {
                    "Qty Shipped", "Qty Consumed", 
                    "Revenue Amount", "Delivery Cost Amt", "Tran Profit",
                    "On Hand @end"}).ToArray();


    public override void refreshPivotTable()
    {
        resetTable(); // データベースから読み直さない

        // 各列を追加
        List<DataRow> dataRows = new List<DataRow>();
        foreach (var prop in _row_labels) {  // まず項目名だけ
            DataRow row = _dataTable.NewRow();
            row[0] = prop;
            row[1] = ""; // TODO: ●plan version を表示
            dataRows.Add(row);
            _dataTable.Rows.Add(row);
        }

        // mainwindow に依存している TODO: ●application に移動すること!
        var vm = (MainWindowViewModel) Application.Current.MainWindow.DataContext;

        if (_psiPvtData != null ) {
            // slice を表示
            var selectQuery = new SliceQuery(_psiPvtData)
                    .Dimension(vm.TimeStrata == TimeStrataMode.WEEK ? "Date" : "dt_month")
                    /*.Dimension("category-l1").Dimension("channel-l1")*/;
            if (_tvi != null && _tvi.ItemCategory != null) { // root 以外
                selectQuery = selectQuery.Where(
                        _tvi.ItemCategory.Level == 0 ? "category-l1" : "category-l2",
                        _tvi.ItemCategory.Id );
            }
            // TODO: ● location もグルーピングがある. (仮想倉庫)
            if (_locationSelected != null && _locationSelected.LocationCode != null) {
                selectQuery = selectQuery.Where(
                                        "location", _locationSelected.Id );
            }
            if (ItemsSelected.Count > 0) {
                // 配列を渡せるが、上手く動かない. 型が object 固定になっている
                // predicate 版を使え
                HashSet<int> selected = new HashSet<int>(ItemsSelected.Select(x => x.Id));
                selectQuery = selectQuery.Where("product",
                                (x) => selected.Contains((int) x));
            }

            PivotData slicedPvtData = selectQuery.Execute();

            var list = slicedPvtData.OrderBy(x => x.Key[0]).ToList();
            int prev_col = 0;
            for (int i = 0; i < list.Count; ++i) {
                var kv = list[i];
                int col = _dates.FindIndex(x => x == (DateOnly) kv.Key[0]);
                if (col == -1) 
                    continue;

                int j;
                for (j = prev_col; j <= col; ++j) { 
                    _dataTable.Columns.Add(new DataColumn() {
                            ColumnName = _dates[j].ToString().Replace("/", "-")});
                }
                prev_col = j;

                for (int ro = 0; ro < 8 + 6; ++ro) { 
                    dataRows[ro][col + 2] = 
                            Math.Round((Decimal) kv.Value.AsComposite()
                                                   .Aggregators[ro].Value, 2);
                }
            }
        }

        RaisePropertyChanged(nameof(DataTableView));
    }

}

}
