﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfPlanning.Models;


namespace WpfPlanning.Views
{

/*
class LocationOrSupplier {
    // "<all>" はどっちも null
    readonly Location? _location;
    readonly ItemsSupplier? _supplier;

    public string Code { 
        get {
            return _supplier != null ? _supplier.SupplierCode : 
                   (_location != null ? _location.LocationCode : "<all>");
        } 
    }
}
*/

class PurchaseOrdersViewModel : MVVM.BindableBase
{
    public ExObservableCollection<PurchaseOrder> Orders { get; protected set; }

    // dim 2: サプライヤ
    public ExObservableCollection<string> SupplierCodes { 
        get; protected set; }

    private string _supplierSelected;
    public string SupplierSelected { 
        get { return _supplierSelected; }
        set { 
            bool changed = SetPropertyAndRaise(ref _supplierSelected, value); 
            if (changed)
                refreshPivotTable();
        }
    }

    public PurchaseOrdersViewModel() {
        Orders = new ExObservableCollection<PurchaseOrder>();
        SupplierCodes = new ExObservableCollection<string>();
    }

    void refreshPivotTable()
    {
        // TODO: impl.
    }
}

    /// purchase_orders_window.xaml の相互作用ロジック
    /// </summary>
public partial class PurchaseOrdersWindow : Window
{
    public PurchaseOrdersWindow()
    {
        InitializeComponent();
    }
}
}
