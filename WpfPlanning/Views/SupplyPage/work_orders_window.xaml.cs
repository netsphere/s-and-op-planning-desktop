﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfPlanning.Models;


namespace WpfPlanning.Views
{

/*
class LocationOrSupplier {
    // "<all>" はどっちも null
    readonly Location? _location;
    readonly ItemsSupplier? _supplier;

    public string Code { 
        get {
            return _supplier != null ? _supplier.SupplierCode : 
                   (_location != null ? _location.LocationCode : "<all>");
        } 
    }
}
*/

class WorkOrdersViewModel : MVVM.BindableBase
{
    public ExObservableCollection<BuildOrder> Orders { get; protected set; }

    public ExObservableCollection<Location> Locations { 
        get; protected set; }

    private string _locationSelected;
    public string LocationSelected { 
        get { return _locationSelected; }
        set { 
            bool changed = SetPropertyAndRaise(ref _locationSelected, value); 
            if (changed)
                refreshPivotTable();
        }
    }

    public WorkOrdersViewModel() {
        Orders = new ExObservableCollection<BuildOrder>();
        Locations = new ExObservableCollection<Location>();
    }
    
    void refreshPivotTable()
    {
        // TODO: impl.
    }
}

    /// purchase_orders_window.xaml の相互作用ロジック
    /// </summary>
public partial class WorkOrdersWindow : Window
{
    public WorkOrdersWindow()
    {
        InitializeComponent();
    }
}
}
