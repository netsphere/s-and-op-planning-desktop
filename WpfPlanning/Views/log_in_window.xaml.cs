﻿
using System.Windows;

namespace WpfPlanning.Views
{
    /// <summary>
    /// log_in_window.xaml の相互作用ロジック
    /// </summary>
public partial class LogInWindow : Window
{ 
    public LogInWindow()
    {
        InitializeComponent();
    }

    // [Connect] button
    void Button_Click(object sender, RoutedEventArgs e)
    {
        DialogResult = true;
    }

    // [Cancel] button
    void cxlButton_Click(object sender, RoutedEventArgs e)
    {
        DialogResult = false;
    }
}

}
