﻿
using graph_app;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;
using WpfPlanning.Models;
using WpfPlanning.Views;


namespace WpfPlanning
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
public partial class MyApp : Application
{
    public static MyDbContext DbContext ;

    // model data
    public static PlanVersion? CurPlanVersion;

    public static Mrp? MrpResults;
    
    // Application#MainWindow を使え
    //private MainWindow mainWindow ;


    void Application_Startup(object sender, StartupEventArgs e)
    {
        var mainWindow = new MainWindow();

        var cmds = new CommandBinding[] { 
            // [File] menu
            new CommandBinding(MyCommands.LogIn, OnFileLogIn),
            new CommandBinding(MyCommands.SelectPlanVersion,
                               OnSelectPlanVersion, CanSelectPlanVersion),
            new CommandBinding(ApplicationCommands.Close, OnFileExit),

            // [Tool] menu
            new CommandBinding(MyCommands.ToolImportMasterData,
                               OnToolImportMasterData),
            new CommandBinding(MyCommands.FileImport, OnToolImportActual),

            // [Window] menu
            new CommandBinding(MyCommands.ProdCategoriesWindow, OnWindowProdCategories),
            new CommandBinding(MyCommands.ItemListWindow, OnWindowItemList),
            new CommandBinding(MyCommands.BoMWindow, OnWindowBoM),
            new CommandBinding(MyCommands.ReplenishmentRulesWindow, OnReplenishmentRulesWindow),        
            // [Help] menu
            new CommandBinding(MyCommands.HelpAbout, OnHelpAbout)
            };
        foreach (var cmd in cmds) 
            mainWindow.CommandBindings.Add(cmd);

        mainWindow.Show();

        var login = new LogInWindow() { Owner = mainWindow };
        login.database.Text = WpfPlanning.Properties.Settings.Default.ConnectionString ;
        if (login.ShowDialog() != true) {
            Application.Current.Shutdown();
            return;
        }

        DbContext = new MyDbContext();
        // 挙動が微妙すぎる:
        //  - データベースが存在して any tables がある場合、何もしない
        //  - データベースが存在して何もテーブルがない場合, スキーマを自動的に構築する
        //  - データベースが存在しない、データベースを作成する。
        //DbContext.Database.EnsureCreated();
        if ( !DbContext.Database.CanConnect()) {
            MessageBox.Show("The database is not available.");
            Application.Current.Shutdown();
        }
    }


    //////// ///////////////////////////////////////////////////////////////
    // Command handlers

    // [Window] -> 補充ルール
    void OnReplenishmentRulesWindow(object sender, ExecutedRoutedEventArgs e)
    {
        var wnd = new ReplenishmentRulesWindow();
        wnd.Show();
    }

    void OnHelpAbout(object sender, ExecutedRoutedEventArgs e)
    {
        MessageBox.Show("hoge"); // TODO: impl.
    }


    // As Another User...
    void OnFileLogIn(object sender, ExecutedRoutedEventArgs e)
    {
        var login = new LogInWindow() { Owner = this.MainWindow };
        login.database.Text = WpfPlanning.Properties.Settings.Default.ConnectionString ;
        if (login.ShowDialog() == true) {
            // TODO: impl. 
        }
    }

    // File -> 計画バージョン選択
    void OnSelectPlanVersion(object sender, ExecutedRoutedEventArgs e)
    {
        var wnd = new Views.SelectPlanVersionDlg();
        wnd.Owner = MyApp.Current.MainWindow;
        if (wnd.ShowDialog() == true && wnd.SelectedPlan != null) {
            var mainWindow = (MainWindow) Application.Current.MainWindow;
            if (CurPlanVersion == null || CurPlanVersion.Id != wnd.SelectedPlan.Id) {
                CurPlanVersion = wnd.SelectedPlan;
                mainWindow.changePlan();
                ((MainWindowViewModel) mainWindow.DataContext).CurrentPlanVersion = 
                            CurPlanVersion.Name;
            }
        }
    }


    void CanSelectPlanVersion(object sender, CanExecuteRoutedEventArgs e)
    {
        e.CanExecute = true;
    }

    // File -> 実績のインポート
    private void OnToolImportActual(object sender, ExecutedRoutedEventArgs e)
    {
        var wnd = new Views.FileImportWindow();
        if (wnd.ShowDialog() == true) {
            // TODO: impl.
        }
    }

    private void CanFileImport(object sender, CanExecuteRoutedEventArgs e)
    {
        e.CanExecute = (CurPlanVersion != null) && CurPlanVersion.IsOpen;
    }

    // File -> 終了
    private void OnFileExit(object sender, ExecutedRoutedEventArgs e)
    {
        Application.Current.Shutdown();
    }

    // Tool -> マスタのインポート
    void OnToolImportMasterData(object sender, ExecutedRoutedEventArgs e)
    {
        var wnd = new Views.ToolImportMasterDataWindow();
        if (wnd.ShowDialog() == true) {
            // TODO: impl.
        }
    }

    // Window -> 商品カテゴリ
    private void OnWindowProdCategories(object sender, ExecutedRoutedEventArgs e)
    {
        var wnd = new Views.ProdCategoriesWindow();
        wnd.Show();
    }


    // Window -> 品目表
    private void OnWindowItemList(object sender, ExecutedRoutedEventArgs e)
    {
        var wnd = new Views.ProductListWindow();
        wnd.Show();
    }

    // [Window] -> BoM
    void OnWindowBoM(object sender, ExecutedRoutedEventArgs e)
    {
        var wnd = new Views.BoMWindow();
        wnd.Show();
    }

}


static class MyCommands
{
    /// //////////////////////////////////////////////////////////////////
    // [File] menu

    public static readonly RoutedUICommand LogIn = 
            new RoutedUICommand("Log In as Another User", nameof(LogIn), typeof(MyCommands));
    
    // Plan Versions
    public static readonly RoutedUICommand SelectPlanVersion =
            new RoutedUICommand("計画バージョンの選択", nameof(SelectPlanVersion), typeof(MyCommands));

    public static readonly RoutedUICommand NewPlanVersion =
            new RoutedUICommand("新しい計画バージョン", nameof(NewPlanVersion), typeof(MyCommands));
    public static readonly RoutedUICommand PlanVersionEdit =
            new RoutedUICommand("計画バージョンの編集", nameof(PlanVersionEdit), 
                                typeof(MyCommands));
    public static readonly RoutedUICommand PlanVersionDestroy =
            new RoutedUICommand("計画バージョンの削除", nameof(PlanVersionDestroy), 
                                typeof(MyCommands));

    /// //////////////////////////////////////////////////////////////////
    // [Cycle] menu

    public static readonly RoutedUICommand GenBaselineForecast = 
            new RoutedUICommand("Generate Baseline Forecast", nameof(GenBaselineForecast), typeof(MyCommands));

    public static readonly RoutedUICommand NewProducts = 
            new RoutedUICommand("New Products", nameof(NewProducts), typeof(MyCommands));

    public static readonly RoutedUICommand PromotionsAndEvents = 
            new RoutedUICommand("Promotions and Events", nameof(PromotionsAndEvents), typeof(MyCommands));

    public static readonly RoutedUICommand DemandPlansView = 
            new RoutedUICommand("需要計画view", nameof(DemandPlansView), typeof(MyCommands));

    public static readonly RoutedUICommand RunMRPCommand =
            new RoutedUICommand("Run MRP", nameof(RunMRPCommand),
                                typeof(MyCommands));

    public static readonly RoutedUICommand ProductsView =
            new RoutedUICommand("供給計画view", nameof(ProductsView), typeof(MyCommands));

    public static readonly RoutedUICommand Supply2Show =
            new RoutedUICommand("供給計画view", nameof(Supply2Show), 
                                typeof(MyCommands));

    public static readonly RoutedUICommand FinancialReview =
            new RoutedUICommand("Financial Profit View", 
                                nameof(FinancialReview), typeof(MyCommands));

    /// //////////////////////////////////////////////////////////////////
    // View menu

    public static readonly RoutedUICommand PurchaseOrdersWindowCommand =
            new RoutedUICommand("Show purchase orders", 
                                nameof(PurchaseOrdersWindowCommand), typeof(MyCommands));
    public static readonly RoutedUICommand TransferOrdersWindowCommand =
            new RoutedUICommand("Show transfer orders", 
                                nameof(TransferOrdersWindowCommand), typeof(MyCommands));
    public static readonly RoutedUICommand WorkOrdersWindowCommand =
            new RoutedUICommand("Show work orders", 
                                nameof(WorkOrdersWindowCommand), typeof(MyCommands));

    public static readonly RoutedUICommand ComparativeVersion =
            new RoutedUICommand("Comparison Another Version", nameof(ComparativeVersion),
                                typeof(MyCommands));

    /// //////////////////////////////////////////////////////////////////
    // [Tool] menu

    public static readonly RoutedUICommand ToolImportMasterData = 
            new RoutedUICommand("マスタデータのインポート", nameof(ToolImportMasterData),
                                typeof(MyCommands));

    public static readonly RoutedUICommand FileImport = 
            new RoutedUICommand("実績データのインポート", nameof(FileImport), 
                                typeof(MyCommands));

    /// //////////////////////////////////////////////////////////////////
    // [Window] menu

    public static readonly ICommand ProdCategoriesWindow =
            new RoutedUICommand("商品カテゴリ", nameof(ProdCategoriesWindow), typeof(MyCommands));

    public static readonly ICommand ItemListWindow =
            new RoutedUICommand("品目表", nameof(ItemListWindow), typeof(MyCommands));

    public static readonly ICommand BoMWindow =
            new RoutedUICommand("Bill of Materials", nameof(BoMWindow), typeof(MyCommands));

    public static readonly ICommand ReplenishmentRulesWindow =
            new RoutedUICommand("Replenishment Rules Window", 
                                nameof(ReplenishmentRulesWindow), typeof(MyCommands));

    /// //////////////////////////////////////////////////////////////////
    // [Help] menu

    public static readonly ICommand HelpAbout =
            new RoutedUICommand("About S&OP Planning Desktop", nameof(HelpAbout),
                                typeof(MyCommands));
}


}
