﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WpfPlanning.Migrations
{
    /// <inheritdoc />
    public partial class InitialCreate : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ItemCategories",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CategoryCode = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ParentId = table.Column<int>(type: "int", nullable: true),
                    Level = table.Column<int>(type: "int", nullable: false),
                    created_at = table.Column<DateTime>(type: "datetime2", nullable: false),
                    updated_at = table.Column<DateTime>(type: "datetime2", nullable: false),
                    lock_version = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ItemCategories", x => x.id);
                    table.ForeignKey(
                        name: "FK_ItemCategories_ItemCategories_ParentId",
                        column: x => x.ParentId,
                        principalTable: "ItemCategories",
                        principalColumn: "id");
                });

            migrationBuilder.CreateTable(
                name: "Locations",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    LocationCode = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ParentId = table.Column<int>(type: "int", nullable: true),
                    Level = table.Column<int>(type: "int", nullable: false),
                    created_at = table.Column<DateTime>(type: "datetime2", nullable: false),
                    updated_at = table.Column<DateTime>(type: "datetime2", nullable: false),
                    lock_version = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Locations", x => x.id);
                    table.ForeignKey(
                        name: "FK_Locations_Locations_ParentId",
                        column: x => x.ParentId,
                        principalTable: "Locations",
                        principalColumn: "id");
                });

            migrationBuilder.CreateTable(
                name: "plan_versions",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    IsOpen = table.Column<bool>(type: "bit", nullable: false),
                    ForecastDate = table.Column<DateTime>(type: "date", nullable: true),
                    StartDate = table.Column<DateTime>(type: "date", nullable: true),
                    EndDate = table.Column<DateTime>(type: "date", nullable: true),
                    IsActual = table.Column<bool>(type: "bit", nullable: false),
                    created_at = table.Column<DateTime>(type: "datetime2", nullable: false),
                    updated_at = table.Column<DateTime>(type: "datetime2", nullable: false),
                    lock_version = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_plan_versions", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "PurchaseGroups",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    GroupCode = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    GroupMinWeight = table.Column<double>(type: "float", nullable: false),
                    GroupMinVolume = table.Column<double>(type: "float", nullable: false),
                    GroupMinQty = table.Column<int>(type: "int", nullable: false),
                    GroupMinCost = table.Column<double>(type: "float", nullable: false),
                    created_at = table.Column<DateTime>(type: "datetime2", nullable: false),
                    updated_at = table.Column<DateTime>(type: "datetime2", nullable: false),
                    lock_version = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PurchaseGroups", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Products",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ItemCode = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ItemCategoryId = table.Column<int>(type: "int", nullable: false),
                    IsSaleable = table.Column<bool>(type: "bit", nullable: false),
                    UnitWeightKg = table.Column<double>(type: "float", nullable: false),
                    UnitVolumeCm3 = table.Column<double>(type: "float", nullable: false),
                    UnitSalesPrice = table.Column<double>(type: "float", nullable: false),
                    UnitInventoryValue = table.Column<double>(type: "float", nullable: false),
                    Inactive = table.Column<DateTime>(type: "date", nullable: true),
                    created_at = table.Column<DateTime>(type: "datetime2", nullable: false),
                    updated_at = table.Column<DateTime>(type: "datetime2", nullable: false),
                    lock_version = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Products", x => x.id);
                    table.ForeignKey(
                        name: "FK_Products_ItemCategories_ItemCategoryId",
                        column: x => x.ItemCategoryId,
                        principalTable: "ItemCategories",
                        principalColumn: "id");
                });

            migrationBuilder.CreateTable(
                name: "SalesChannels",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ChannelCode = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ParentId = table.Column<int>(type: "int", nullable: true),
                    Level = table.Column<int>(type: "int", nullable: false),
                    LocationId = table.Column<int>(type: "int", nullable: true),
                    created_at = table.Column<DateTime>(type: "datetime2", nullable: false),
                    updated_at = table.Column<DateTime>(type: "datetime2", nullable: false),
                    lock_version = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SalesChannels", x => x.id);
                    table.ForeignKey(
                        name: "FK_SalesChannels_Locations_LocationId",
                        column: x => x.LocationId,
                        principalTable: "Locations",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "FK_SalesChannels_SalesChannels_ParentId",
                        column: x => x.ParentId,
                        principalTable: "SalesChannels",
                        principalColumn: "id");
                });

            migrationBuilder.CreateTable(
                name: "build_orders",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    InputItemId = table.Column<int>(type: "int", nullable: false),
                    InputQty = table.Column<int>(type: "int", nullable: false),
                    PlanVersionId = table.Column<int>(type: "int", nullable: false),
                    OrderType = table.Column<int>(type: "int", nullable: false),
                    SendoutDate = table.Column<DateTime>(type: "date", nullable: false),
                    ReceiveILoc_ProductId = table.Column<int>(type: "int", nullable: false),
                    ReceiveILoc_LocationId = table.Column<int>(type: "int", nullable: false),
                    ReceiveDate = table.Column<DateTime>(type: "date", nullable: false),
                    QtyToReceive = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_build_orders", x => x.id);
                    table.ForeignKey(
                        name: "FK_build_orders_Locations_ReceiveILoc_LocationId",
                        column: x => x.ReceiveILoc_LocationId,
                        principalTable: "Locations",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "FK_build_orders_Products_InputItemId",
                        column: x => x.InputItemId,
                        principalTable: "Products",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "FK_build_orders_Products_ReceiveILoc_ProductId",
                        column: x => x.ReceiveILoc_ProductId,
                        principalTable: "Products",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "FK_build_orders_plan_versions_PlanVersionId",
                        column: x => x.PlanVersionId,
                        principalTable: "plan_versions",
                        principalColumn: "id");
                });

            migrationBuilder.CreateTable(
                name: "items_suppliers",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProductId = table.Column<int>(type: "int", nullable: false),
                    SupplierCode = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    SuppliersItemCode = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    SuppliersCurrency = table.Column<string>(type: "nvarchar(3)", maxLength: 3, nullable: false),
                    GroupId = table.Column<int>(type: "int", nullable: true),
                    UnitPurchasePrice = table.Column<double>(type: "float", nullable: false),
                    Terminated = table.Column<DateTime>(type: "date", nullable: true),
                    created_at = table.Column<DateTime>(type: "datetime2", nullable: false),
                    updated_at = table.Column<DateTime>(type: "datetime2", nullable: false),
                    lock_version = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_items_suppliers", x => x.id);
                    table.UniqueConstraint("AK_items_suppliers_ProductId_SupplierCode", x => new { x.ProductId, x.SupplierCode });
                    table.ForeignKey(
                        name: "FK_items_suppliers_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "id");
                });

            migrationBuilder.CreateTable(
                name: "product_structures",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ParentId = table.Column<int>(type: "int", nullable: false),
                    ChildId = table.Column<int>(type: "int", nullable: false),
                    BoMType = table.Column<int>(type: "int", nullable: false),
                    Qty = table.Column<double>(type: "float", nullable: false),
                    BatchRounding = table.Column<int>(type: "int", nullable: false),
                    MinBatchSize = table.Column<int>(type: "int", nullable: false),
                    ManufacturingLeadTime = table.Column<int>(type: "int", nullable: false),
                    created_at = table.Column<DateTime>(type: "datetime2", nullable: false),
                    updated_at = table.Column<DateTime>(type: "datetime2", nullable: false),
                    lock_version = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_product_structures", x => x.id);
                    table.UniqueConstraint("AK_product_structures_ParentId_ChildId", x => new { x.ParentId, x.ChildId });
                    table.ForeignKey(
                        name: "FK_product_structures_Products_ChildId",
                        column: x => x.ChildId,
                        principalTable: "Products",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "FK_product_structures_Products_ParentId",
                        column: x => x.ParentId,
                        principalTable: "Products",
                        principalColumn: "id");
                });

            migrationBuilder.CreateTable(
                name: "purchase_orders",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    OrderNumber = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    SupplierCode = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    DeliveryDate = table.Column<DateTime>(type: "date", nullable: false),
                    LotCost = table.Column<double>(type: "float", nullable: false),
                    FreightCost = table.Column<double>(type: "float", nullable: false),
                    LotCostCurrency = table.Column<string>(type: "nvarchar(3)", maxLength: 3, nullable: false),
                    LotCostInFnCurr = table.Column<double>(type: "float", nullable: false),
                    PlanVersionId = table.Column<int>(type: "int", nullable: false),
                    OrderType = table.Column<int>(type: "int", nullable: false),
                    SendoutDate = table.Column<DateTime>(type: "date", nullable: false),
                    ReceiveILoc_ProductId = table.Column<int>(type: "int", nullable: false),
                    ReceiveILoc_LocationId = table.Column<int>(type: "int", nullable: false),
                    ReceiveDate = table.Column<DateTime>(type: "date", nullable: false),
                    QtyToReceive = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_purchase_orders", x => x.id);
                    table.ForeignKey(
                        name: "FK_purchase_orders_Locations_ReceiveILoc_LocationId",
                        column: x => x.ReceiveILoc_LocationId,
                        principalTable: "Locations",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "FK_purchase_orders_Products_ReceiveILoc_ProductId",
                        column: x => x.ReceiveILoc_ProductId,
                        principalTable: "Products",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "FK_purchase_orders_plan_versions_PlanVersionId",
                        column: x => x.PlanVersionId,
                        principalTable: "plan_versions",
                        principalColumn: "id");
                });

            migrationBuilder.CreateTable(
                name: "ReplenishmentRules",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProductId = table.Column<int>(type: "int", nullable: false),
                    LocationId = table.Column<int>(type: "int", nullable: false),
                    LastOnHand = table.Column<int>(type: "int", nullable: false),
                    ProcurementTime = table.Column<int>(type: "int", nullable: false),
                    ShippingTime = table.Column<int>(type: "int", nullable: false),
                    LeadTimeVariance = table.Column<double>(type: "float", nullable: false),
                    OrderCycle = table.Column<int>(type: "int", nullable: false),
                    OrderingDays = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Rounding = table.Column<int>(type: "int", nullable: false),
                    MinLot = table.Column<int>(type: "int", nullable: false),
                    SupplierCode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DCId = table.Column<int>(type: "int", nullable: true),
                    created_at = table.Column<DateTime>(type: "datetime2", nullable: false),
                    updated_at = table.Column<DateTime>(type: "datetime2", nullable: false),
                    lock_version = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReplenishmentRules", x => x.id);
                    table.UniqueConstraint("AK_ReplenishmentRules_ProductId_LocationId", x => new { x.ProductId, x.LocationId });
                    table.ForeignKey(
                        name: "FK_ReplenishmentRules_Locations_DCId",
                        column: x => x.DCId,
                        principalTable: "Locations",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "FK_ReplenishmentRules_Locations_LocationId",
                        column: x => x.LocationId,
                        principalTable: "Locations",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "FK_ReplenishmentRules_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "id");
                });

            migrationBuilder.CreateTable(
                name: "shipment_tran",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ShipmentDate = table.Column<DateTime>(type: "date", nullable: false),
                    ProductId = table.Column<int>(type: "int", nullable: false),
                    LocationId = table.Column<int>(type: "int", nullable: false),
                    QtyShipped = table.Column<int>(type: "int", nullable: false),
                    RevenueAmount = table.Column<double>(type: "float", nullable: false),
                    DeliveryCostAmount = table.Column<double>(type: "float", nullable: false),
                    TranProfit = table.Column<double>(type: "float", nullable: false),
                    OnHand = table.Column<int>(type: "int", nullable: false),
                    PlanVersionId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_shipment_tran", x => x.id);
                    table.ForeignKey(
                        name: "FK_shipment_tran_Locations_LocationId",
                        column: x => x.LocationId,
                        principalTable: "Locations",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "FK_shipment_tran_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "FK_shipment_tran_plan_versions_PlanVersionId",
                        column: x => x.PlanVersionId,
                        principalTable: "plan_versions",
                        principalColumn: "id");
                });

            migrationBuilder.CreateTable(
                name: "transfer_orders",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ShipFromId = table.Column<int>(type: "int", nullable: false),
                    PlanVersionId = table.Column<int>(type: "int", nullable: false),
                    OrderType = table.Column<int>(type: "int", nullable: false),
                    SendoutDate = table.Column<DateTime>(type: "date", nullable: false),
                    ReceiveILoc_ProductId = table.Column<int>(type: "int", nullable: false),
                    ReceiveILoc_LocationId = table.Column<int>(type: "int", nullable: false),
                    ReceiveDate = table.Column<DateTime>(type: "date", nullable: false),
                    QtyToReceive = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_transfer_orders", x => x.id);
                    table.ForeignKey(
                        name: "FK_transfer_orders_Locations_ReceiveILoc_LocationId",
                        column: x => x.ReceiveILoc_LocationId,
                        principalTable: "Locations",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "FK_transfer_orders_Locations_ShipFromId",
                        column: x => x.ShipFromId,
                        principalTable: "Locations",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "FK_transfer_orders_Products_ReceiveILoc_ProductId",
                        column: x => x.ReceiveILoc_ProductId,
                        principalTable: "Products",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "FK_transfer_orders_plan_versions_PlanVersionId",
                        column: x => x.PlanVersionId,
                        principalTable: "plan_versions",
                        principalColumn: "id");
                });

            migrationBuilder.CreateTable(
                name: "demand_tran",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SaleDate = table.Column<DateTime>(type: "date", nullable: false),
                    ProductId = table.Column<int>(type: "int", nullable: false),
                    SalesChannelId = table.Column<int>(type: "int", nullable: false),
                    QtySold = table.Column<double>(type: "float", nullable: false),
                    SalesAmount = table.Column<double>(type: "float", nullable: false),
                    SalesCostAmount = table.Column<double>(type: "float", nullable: false),
                    UnfilledOrders = table.Column<int>(type: "int", nullable: false),
                    PlanVersionId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_demand_tran", x => x.id);
                    table.ForeignKey(
                        name: "FK_demand_tran_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "FK_demand_tran_SalesChannels_SalesChannelId",
                        column: x => x.SalesChannelId,
                        principalTable: "SalesChannels",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "FK_demand_tran_plan_versions_PlanVersionId",
                        column: x => x.PlanVersionId,
                        principalTable: "plan_versions",
                        principalColumn: "id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_build_orders_InputItemId",
                table: "build_orders",
                column: "InputItemId");

            migrationBuilder.CreateIndex(
                name: "IX_build_orders_PlanVersionId",
                table: "build_orders",
                column: "PlanVersionId");

            migrationBuilder.CreateIndex(
                name: "IX_build_orders_ReceiveILoc_LocationId",
                table: "build_orders",
                column: "ReceiveILoc_LocationId");

            migrationBuilder.CreateIndex(
                name: "IX_build_orders_ReceiveILoc_ProductId",
                table: "build_orders",
                column: "ReceiveILoc_ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_demand_tran_PlanVersionId",
                table: "demand_tran",
                column: "PlanVersionId");

            migrationBuilder.CreateIndex(
                name: "IX_demand_tran_ProductId",
                table: "demand_tran",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_demand_tran_SalesChannelId",
                table: "demand_tran",
                column: "SalesChannelId");

            migrationBuilder.CreateIndex(
                name: "IX_ItemCategories_CategoryCode",
                table: "ItemCategories",
                column: "CategoryCode",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ItemCategories_ParentId",
                table: "ItemCategories",
                column: "ParentId");

            migrationBuilder.CreateIndex(
                name: "IX_items_suppliers_SupplierCode_SuppliersItemCode",
                table: "items_suppliers",
                columns: new[] { "SupplierCode", "SuppliersItemCode" },
                unique: true,
                filter: "[SuppliersItemCode] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Locations_Description",
                table: "Locations",
                column: "Description",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Locations_LocationCode",
                table: "Locations",
                column: "LocationCode",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Locations_ParentId",
                table: "Locations",
                column: "ParentId");

            migrationBuilder.CreateIndex(
                name: "IX_product_structures_ChildId",
                table: "product_structures",
                column: "ChildId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_ItemCategoryId",
                table: "Products",
                column: "ItemCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_ItemCode",
                table: "Products",
                column: "ItemCode",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_purchase_orders_PlanVersionId",
                table: "purchase_orders",
                column: "PlanVersionId");

            migrationBuilder.CreateIndex(
                name: "IX_purchase_orders_ReceiveILoc_LocationId",
                table: "purchase_orders",
                column: "ReceiveILoc_LocationId");

            migrationBuilder.CreateIndex(
                name: "IX_purchase_orders_ReceiveILoc_ProductId",
                table: "purchase_orders",
                column: "ReceiveILoc_ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_PurchaseGroups_GroupCode",
                table: "PurchaseGroups",
                column: "GroupCode",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_PurchaseGroups_Name",
                table: "PurchaseGroups",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ReplenishmentRules_DCId",
                table: "ReplenishmentRules",
                column: "DCId");

            migrationBuilder.CreateIndex(
                name: "IX_ReplenishmentRules_LocationId",
                table: "ReplenishmentRules",
                column: "LocationId");

            migrationBuilder.CreateIndex(
                name: "IX_SalesChannels_ChannelCode",
                table: "SalesChannels",
                column: "ChannelCode",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_SalesChannels_Description",
                table: "SalesChannels",
                column: "Description",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_SalesChannels_LocationId",
                table: "SalesChannels",
                column: "LocationId");

            migrationBuilder.CreateIndex(
                name: "IX_SalesChannels_ParentId",
                table: "SalesChannels",
                column: "ParentId");

            migrationBuilder.CreateIndex(
                name: "IX_shipment_tran_LocationId",
                table: "shipment_tran",
                column: "LocationId");

            migrationBuilder.CreateIndex(
                name: "IX_shipment_tran_PlanVersionId",
                table: "shipment_tran",
                column: "PlanVersionId");

            migrationBuilder.CreateIndex(
                name: "IX_shipment_tran_ProductId",
                table: "shipment_tran",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_transfer_orders_PlanVersionId",
                table: "transfer_orders",
                column: "PlanVersionId");

            migrationBuilder.CreateIndex(
                name: "IX_transfer_orders_ReceiveILoc_LocationId",
                table: "transfer_orders",
                column: "ReceiveILoc_LocationId");

            migrationBuilder.CreateIndex(
                name: "IX_transfer_orders_ReceiveILoc_ProductId",
                table: "transfer_orders",
                column: "ReceiveILoc_ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_transfer_orders_ShipFromId",
                table: "transfer_orders",
                column: "ShipFromId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "build_orders");

            migrationBuilder.DropTable(
                name: "demand_tran");

            migrationBuilder.DropTable(
                name: "items_suppliers");

            migrationBuilder.DropTable(
                name: "product_structures");

            migrationBuilder.DropTable(
                name: "purchase_orders");

            migrationBuilder.DropTable(
                name: "PurchaseGroups");

            migrationBuilder.DropTable(
                name: "ReplenishmentRules");

            migrationBuilder.DropTable(
                name: "shipment_tran");

            migrationBuilder.DropTable(
                name: "transfer_orders");

            migrationBuilder.DropTable(
                name: "SalesChannels");

            migrationBuilder.DropTable(
                name: "Products");

            migrationBuilder.DropTable(
                name: "plan_versions");

            migrationBuilder.DropTable(
                name: "Locations");

            migrationBuilder.DropTable(
                name: "ItemCategories");
        }
    }
}
