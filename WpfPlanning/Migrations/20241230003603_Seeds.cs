﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WpfPlanning.Migrations
{
    /// <inheritdoc />
    public partial class Seeds : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "plan_versions",
                columns: new[] { "id", "created_at", "EndDate", "ForecastDate", "IsActual", "IsOpen", "lock_version", "Name", "StartDate", "updated_at" },
                values: new object[] { 1, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, true, false, 0, "actual results", null, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "plan_versions",
                keyColumn: "id",
                keyValue: 1);
        }
    }
}
