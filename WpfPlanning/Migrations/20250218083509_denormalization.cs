﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WpfPlanning.Migrations
{
    /// <inheritdoc />
    public partial class denormalization : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CatL1",
                table: "shipment_tran",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "CatL2",
                table: "shipment_tran",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "QtyConsumed",
                table: "shipment_tran",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "CatL1",
                table: "demand_tran",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "CatL2",
                table: "demand_tran",
                type: "int",
                nullable: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CatL1",
                table: "shipment_tran");

            migrationBuilder.DropColumn(
                name: "CatL2",
                table: "shipment_tran");

            migrationBuilder.DropColumn(
                name: "QtyConsumed",
                table: "shipment_tran");

            migrationBuilder.DropColumn(
                name: "CatL1",
                table: "demand_tran");

            migrationBuilder.DropColumn(
                name: "CatL2",
                table: "demand_tran");
        }
    }
}
