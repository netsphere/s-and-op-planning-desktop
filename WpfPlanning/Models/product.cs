﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;


namespace WpfPlanning.Models
{

[Index(nameof(CategoryCode), IsUnique = true)]
//[Index(nameof(Name), IsUnique = true)]  // 名前のほうにも UNIQUE を付けておく.
public class ItemCategory : MasterBase
{
    [Key, Column("id")]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int Id { get; protected set; }

    // 大文字にする.
    [Required]
    public string CategoryCode { get; set; }

    // 重複不可はやりすぎだった. 重複可
    [Required]
    public string Name { get; set; }

    //[Required(AllowEmptyStrings = true)] // 単に `Required` では "" もエラー
    //public string Description { get; set; }

    public int? ParentId { get; set; }

    [ForeignKey(nameof(ParentId))]
    [DeleteBehavior(DeleteBehavior.NoAction)]
    public virtual ItemCategory? Parent { get; set; }

    public virtual ICollection<ItemCategory> Children { get; } = new List<ItemCategory>();

    public virtual ICollection<Product> Products { get; } = new List<Product>();

    // 階層の深さ. トップレベル = 0
    [Required]
    public int Level { get; set; }

    //[Required]
    //public bool IsLeaf { get; set; }
}


/// ///////////////////////////////////////////////////////////////
// 部品表 Bill of Materials (BoM) は、品目表と、製品構成表の二つから成る。

public class ProductStructureConfiguration : 
                                    IEntityTypeConfiguration<ProductStructure>
{
    // @override
    public void Configure(EntityTypeBuilder<ProductStructure> builder)
    {
        // 複合ユニーク制約
        builder.HasAlternateKey( (p) => new {p.ParentId, p.ChildId});

        //builder.HasOne((e) => e.ParentId).WithMany().OnDelete(DeleteBehavior.NoAction);
        //builder.HasOne((e) => e.ChildId).WithMany().Ondelete(DeleteBehavior.NoAction);
    }
}

public enum EBoMType {
    ASM = 1, KIT = 2, SUBCONTRACT = 3,

    // MRPのグラフ展開でのみ使う. BoM ではない
    TRANSFER = 11,
}

// 製品構成表: 部品構成 (PS) とも云う. 親品目と子品目の関連の情報
// 日本語では「ストラクチャ部品表」などだが、英語だと product
[Table("product_structures")]
public class ProductStructure : MasterBase
{
    [Key, Column("id")]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int Id { get; protected set; }

    [Required]
    public int ParentId { get; set; }

    [Required]
    public int ChildId { get; set; }

    [ForeignKey(nameof(ParentId))]
    [DeleteBehavior(DeleteBehavior.NoAction)]
    public virtual Product Parent { get; set; }

    [ForeignKey(nameof(ChildId))]
    [DeleteBehavior(DeleteBehavior.NoAction)]
    public virtual Product Child { get; set; }

    // ASM or KIT?
    [Required]
    public EBoMType BoMType { get; set; }

    [Required]
    public double Qty { get; set; }

    [Required]
    public int BatchRounding { get; set; }

    [Required]
    public int MinBatchSize { get; set; }

    // 生産リードタイム. Kitting のときは 0
    public int ManufacturingLeadTime { get; set; }

    // @return item の root を返す。チェック済みの場合は null.
    static Product? root_check(Product item, HashSet<int> ck)
    {
        if (ck.Contains(item.Id))
            return null;

        ck.Add(item.Id);
        var parents = item.bomParents();
        if (parents.Count == 0)
            return item;

        foreach (var p in parents) {
            var r = root_check(p, ck);
            if ( r != null )
                return r;
        }
        return null;
    }

    // @return 親子関係があるもののみを返す
    public static List<Product> roots()
    {
        var ck = new HashSet<int>();
        var ret = new List<Product>();

        var prods = MyApp.DbContext.ProductStructures
                         .Include((ps) => ps.Parent)
                         .ToList();
        foreach (var ps in prods) {
            // EFCore は, `ParentId` に値があっても, `Parent` が自動設定ではない!
            if (ps.Parent == null)
                throw new Exception("internal error");
            Product? root = root_check(ps.Parent, ck);
            if (root != null)
                ret.Add(root);
        }

        return ret;
    }
}


// 品目表. 製品/材料の区別、それぞれの品目に固有な情報をまとめる
// クラス名は単数。テーブル名は自動で複数形. カラム全部をプロパティにしないと
// いけない
[Index(nameof(ItemCode), IsUnique = true)]
//[Index(nameof(Description), IsUnique = true)]  // <s>item name のほうにも
//                                               // UNIQUE を付けておく</s>
public class Product : MasterBase, IEquatable<Product>
{
    [Key, Column("id")]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int Id { get; protected set; }

    [Required] // PK
    public string ItemCode { get; set; }

    // これを item name にする
    // 重複不可はやりすぎ。重複可
    //[Required(AllowEmptyStrings = true)]  
    [Required]  // "" もエラーにする
    public string Description { get; set; }

    // <s>Parts は販売商品カテゴリを持たない。</s> -> "material" など持たせる
    public int ItemCategoryId { get; set; }

    [ForeignKey(nameof(ItemCategoryId))]
    public virtual ItemCategory ItemCategory { get; set; }

    // 直接、販売するものは true. false は demand plan に表示しない
    [Required] 
    public bool IsSaleable { get; set; }

    // 単位は kg
    [Required]
    public double UnitWeightKg { get; set; }

    // 単位は cm3. M3 (CBM) は 6 桁違う。表示のときに工夫する。
    [Required]
    public double UnitVolumeCm3 { get; set; }

    // Fallback 用
    [Required]
    public double UnitSalesPrice { get; set; }

    // Fallback 用. 機能通貨建て
    [Required]
    public double UnitInventoryValue { get; set; }

    // 購買しない -> `items_suppliers` table に移動
    // 購買しなくても, BoM で供給できることがある
    //public DateOnly? Terminated { get; set; }

    // Not Saleable
    public DateOnly? Inactive { get; set; }

    public List<Product> bomParents() 
    { 
        var query = MyApp.DbContext.Products
                         .FromSql(
$"SELECT products.* FROM products JOIN product_structures ON product_structures.ParentId = products.Id WHERE product_structures.ChildId = {this.Id}")
                         .ToList();
        return query;
    }


    public List<ProductStructure> bomChildren()
    { 
        var query = MyApp.DbContext.ProductStructures
                         .FromSql(
$"SELECT product_structures.* FROM products JOIN product_structures ON product_structures.ChildId = products.Id WHERE product_structures.ParentId = {this.Id}")
                         .Include((ps) => ps.Child) // EFCore: 後から Eager Loading
                         .ToList();
        return query;
    }

    // @override IEquatable<Product>
    public bool Equals(Product? other)
    {
        if (ReferenceEquals(other, null))
            return false;
        return Id == other.Id;
    }

    // 非ジェネリック版も実装必須
    // ObjectEqualityComparer<T> は, object.Equals() のほうを呼び出してしまう!
    // See https://stackoverflow.com/questions/33939357/equalitycomparert-default-doesnt-return-the-derived-equalitycomparer
    public override bool Equals(object? obj)
    {
        //if (obj is Product)
        //    throw new ArgumentException("internal error");
        return Equals(obj as Product);
    }

    // これも必須
    public override int GetHashCode()
    {
        return Id.GetHashCode();
    }
}

}
