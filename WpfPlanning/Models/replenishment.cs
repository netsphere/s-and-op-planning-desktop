﻿
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;


namespace WpfPlanning.Models
{

public class ItemsSupplierConfiguration : IEntityTypeConfiguration<ItemsSupplier>
{
    // @override
    public void Configure(EntityTypeBuilder<ItemsSupplier> builder)
    {
        // 複合ユニーク制約
        builder.HasAlternateKey( (p) => new {p.ProductId, p.SupplierCode});
        
        // 追加の制約
        builder.HasIndex( (p) => new {p.SupplierCode, p.SuppliersItemCode})
               .IsUnique(); 
    }
}


// Recommendations for purchase orders and production orders
// 補充マスタのうち, location によらないもの.
// PK(ProductId, SupplierCode)  -- Secondary Sourcing 二社購買を許容する
[Table("items_suppliers")]
public class ItemsSupplier : MasterBase
{
    [Key, Column("id")]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int Id { get; protected set; }

    // PK1
    [Required] // Foreign key
    public int ProductId { get; set; }

    // Navigation property
    [ForeignKey("ProductId"), Required]
    public virtual Product Item { get; set; }

    // PK2
    // TODO: 2 レコードあったとき、優先づけどうしよう?
    [Required]
    public string SupplierCode { get; set; }

    public string? SuppliersItemCode { get; set; }

    // 品目によって通貨が異なることが、一応、あり得る
    [StringLength(3), Required]
    public string SuppliersCurrency { get; set; }

    // purchase group
    public int? GroupId { get; set; }

    // 調達リードタイム
    //[Required]
    //public int LeadTime { get; set; }

    //[Required]
    //public double LeadTimeVariance { get; set; }

    // 不定期発注 = 0. 1ヶ月=30
    //[Required]
    //public int OrderCycle { get; set; }

    // 何曜日か or 月のなかの何日か
    //[Required]
    //public string OrderingDays { get; set; }

    // MOQを超えても、いくつずつ増やすか. MOQ以上なら任意のときは 1
    //[Required]
    //public int Rounding { get; set; }

    // Minimum Order Quantity (MOQ)
    //[Required]
    //public int MinLot { get; set; }

    // 値は外貨建て
    [Required]
    public double UnitPurchasePrice { get; set; }

    [NotMapped]
    public bool IsPurchasable { get { return Terminated != null; } }

    // 有効でない
    public DateOnly? Terminated { get; set; }
}


public class ReplenishmentRuleConfiguration :
                                    IEntityTypeConfiguration<ReplenishmentRule>
{
    // @override
    public void Configure(EntityTypeBuilder<ReplenishmentRule> builder)
    {
        // 複合ユニーク制約
        builder.HasAlternateKey( (p) => new {p.ProductId, p.LocationId});
    }
}

// item - location - lead time
// PK(ProductId, LocationId) -- 補充は供給サイド, モノの観点.
public class ReplenishmentRule : MasterBase
{
    [Key, Column("id")]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int Id { get; protected set; }

    // PK1
    [Required] // Foreign key
    public int ProductId { get; set; }

    // Navigation property
    [ForeignKey("ProductId"), Required]
    public virtual Product Item { get; set; }

    // PK2, To
    [Required]
    public int LocationId { get; set; }

    // Navigation property
    [ForeignKey(nameof(LocationId)), Required]
    public virtual Location Location { get; set; }

    // Fallback用
    [Required]
    public int LastOnHand { get; set; }    

    // 調達リードタイム1. 単位 = 日
    [Required]
    public int ProcurementTime { get; set; }

    // 調達リードタイム2. 単位 = 日
    [Required]
    public int ShippingTime { get; set; }

    public int LeadTime() { return ProcurementTime + ShippingTime; }

    [Required]
    public double LeadTimeVariance { get; set; }

    // 不定期発注 = 0. 1ヶ月=30
    [Required]
    public int OrderCycle { get; set; }

    // 何曜日か or 月のなかの何日か
    [Required]
    public string OrderingDays { get; set; }

    // MOQを超えても、いくつずつ増やすか. MOQ以上なら任意のときは 1
    [Required]
    public int Rounding { get; set; }

    // Minimum Order Quantity (MOQ)
    [Required]
    public int MinLot { get; set; }

    // From (vendor)
    //[Required]
    public string? SupplierCode { get; set; }

    // From - "DC name" <s>横持ちは入荷オーダで整理</s> 横持ちにもサイクルがある
    public int? DCId { get; set; }

    [ForeignKey(nameof(DCId))]
    public virtual Location DC { get; set; }
}


// 発注時に、商品をまたいだ制約がある。
[Index(nameof(GroupCode), IsUnique = true)]
[Index(nameof(Name), IsUnique = true)]  // 名前も UNIQUE にしておく
public class PurchaseGroup : MasterBase
{
    [Key, Column("id")]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int Id { get; protected set; }

    // これが key
    [Required]
    public string GroupCode { get; set; }

    [Required]
    public string Name { get; set; }

    // `items_suppliers` table から参照される。結局、サプライヤ単位になる。
    //[Required]
    //public string SupplierCode { get; set; }

    [Required]
    public double GroupMinWeight { get; set; }

    [Required]
    public double GroupMinVolume { get; set; }

    [Required]
    public int GroupMinQty { get; set; }

    [Required]
    public double GroupMinCost { get; set; }
}

}
