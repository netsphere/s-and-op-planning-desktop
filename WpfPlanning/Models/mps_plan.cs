﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfPlanning.Models
{
   
// DBに保存不要か?
public class MpsPlan
{
    [Required]
    public DateOnly Date { get; set; }

    [Required]
    ItemAndLocation ILoc { get; set; }

    [Required]
    public int GrossReq { get; set; }

    [Required]
    public int IssuedOrders { get; set; }

    [Required]
    public int PrpslRecieved { get; set; }

    [Required]
    public int NetReq { get; set; }

    // 受注残  -> 期首期間の demand qty を使う
    //[Required]
    //public int Unfilled { get; set; }

    [Required]
    public int Consumed { get; set; }

    [Required]
    public int Stock { get; set; }

    [Required]
    public int Uncovered { get; set; }

}

}
