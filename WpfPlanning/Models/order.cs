﻿
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;


namespace WpfPlanning.Models
{

public enum OrderType {
    PURCHASE = 1,
    TRANSFER = 2,
    BUILD = 3,
}

public abstract class OrderBase : TranBase
{
    [Required]
    public OrderType OrderType { get; set; }

    // PO の発出日
    [Required]
    public DateOnly SendoutDate { get; set; }

    [Required]
    public ItemAndLocation ReceiveILoc { get; set; }

    // 入庫日
    [Required]
    public DateOnly ReceiveDate { get; set; }

    // 受け取る数量
    [Required]
    public int QtyToReceive { get; set; }
}


[Table("transfer_orders")]
public class TransferOrder : OrderBase 
{
    [Key, Column("id")]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int Id { get; protected set; }

    [Required]
    public int ShipFromId { get; set; }

    [ForeignKey(nameof(ShipFromId))]
    public virtual Location ShipFrom { get; set; }
}


// KITTING or 製造
[Table("build_orders")]
public class BuildOrder : OrderBase
{
    [Key, Column("id")]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int Id { get; protected set; }

    [Required]
    public int InputItemId { get; set; }

    [ForeignKey(nameof(InputItemId))]
    public virtual Product InputItem { get; set; }

    [Required]
    public int InputQty { get; set; }
}


// "オーダ": 入荷データ
// TODO: Work order を別に作るか?
[Table("purchase_orders")]
public class PurchaseOrder : OrderBase
{
    [Key, Column("id")]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int Id { get; protected set; }

    [Required]
    public string OrderNumber { get; set; }

    [Required]
    public string SupplierCode { get; set; }

    // 経由地
    //public int RouteId { get; set; } ●●これは止める

    //[ForeignKey(nameof(RouteId))]
    //[DeleteBehavior(DeleteBehavior.NoAction)]
    //public virtual Location Route { get; set; }

    // 納品日 (経由地の)
    [Required]
    public DateOnly DeliveryDate { get; set; }

    // 仕入代金 - 購入の対価
    [Required]
    public double LotCost { get; set; }

    // 仕入諸掛り
    // https://www.sjonescontainers.co.uk/containerpedia/the-difference-between-freight-and-shipping/
    // The Difference Between Freight And Shipping
    [Required]
    public double FreightCost { get; set; }

    [StringLength(3), Required]
    public string LotCostCurrency { get; set; }

    // 機能通貨表示
    [Required]
    public double LotCostInFnCurr { get; set; }
}


}
