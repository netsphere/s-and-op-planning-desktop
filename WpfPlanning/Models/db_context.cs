﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using System;
using System.ComponentModel.DataAnnotations; // Validator
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace WpfPlanning.Models
{

// EF Core 7 (.NET 6) では, SQL Server は .NET DateOnly 型を SQL DATE にマップできない。
// 自分でコンバータを用意する。
// See https://code-maze.com/csharp-map-dateonly-timeonly-types-to-sql/
public class DateOnlyConverter : ValueConverter<DateOnly, DateTime>
{
    public DateOnlyConverter() : base(
        (dateOnly) => dateOnly.ToDateTime(TimeOnly.MinValue),
        (dateTime) => DateOnly.FromDateTime(dateTime))
    { }
}


/*
 - SQL 標準は `ON UPDATE NO ACTION`, `ON DELETE NO ACTION` が既定値。
   `NO ACTION` は「何もしない」という意味ではなく、被参照行が削除されたときに、
   commit の時点でまだ参照行が存在していれば、エラーが発生。

 - EF Core はわざわざデフォルト挙動を変えている。CASCADE になる。
   source: src/EFCore/Metadata/Conventions/CascadeDeleteConvention.cs

 - Entity Framework 時代は、次のようにすればこの挙動を削除できた。EF Core では不可
   modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

 - `DeleteBehavior` 値で意味があるのは Cascade, Restrict, NoAction, SetNull のみ.
   ほかは無視される。ヒドい
   See https://learn.microsoft.com/en-us/ef/core/saving/cascade-delete

 - さらに, SQL Server は `Restrict` をサポートしていない。NO ACTION になる.

グローバルに NO ACTION にしてしまう.
via https://stackoverflow.com/questions/63306882/specify-on-delete-no-action-or-on-update-no-action-or-modify-other-foreign-key
See https://www.red-gate.com/simple-talk/blogs/change-delete-behavior-and-more-on-ef-core/
 */
static class ModelBuilderExt
{
    public static void RemoveCascadeDeleteConvention(ModelBuilder modelBuilder)
    { 
        //For EF Core 3.0, use this to set the TableName property(because entity.Relational() no longer exist):
        // for the other conventions, we do a metadata model loop
        foreach (IMutableEntityType entityType in modelBuilder.Model.GetEntityTypes()) {
            // equivalent of modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            //entityType.Relational().TableName = entityType.DisplayName();
            //entityType.SetTableName(entityType.DisplayName());

            // equivalent of modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            // and modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            // GetForeignKeys() の戻り値の型 = IEnumerable<IMutableForeignKey>
            //                                 IMutableForeignKey implements IMutableAnnotatable
            entityType.GetForeignKeys()
                .Where(fk => !fk.IsOwnership && fk.DeleteBehavior == DeleteBehavior.Cascade)
                .ToList()
                .ForEach(fk => fk.DeleteBehavior = DeleteBehavior.NoAction);
        }
    }
}


public class MyDbContext : DbContext
{
    protected override void OnConfiguring(DbContextOptionsBuilder builder)
    {
        if (builder.IsConfigured)
            return;

        //base.OnConfiguring(optionsBuilder);
        builder.UseSqlServer(Properties.Settings.Default.ConnectionString);
            //"Server=(localdb)\\mssqllocaldb;Database=my-new-db;Trusted_Conne" +
            //"ction=True;MultipleActiveResultSets=true");

        // `UseSeeding()` は EFCore 9 以降
        //builder.UseSeeding(...)
    }

    protected override void ConfigureConventions(ModelConfigurationBuilder builder)
    {
        base.ConfigureConventions(builder);
        
        builder.Properties<DateOnly>()
                    .HaveConversion<DateOnlyConverter>()
                    .HaveColumnType("date");
    }

    // seed はここに書く
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);

        // CASCADE をすべて削除.
        ModelBuilderExt.RemoveCascadeDeleteConvention(modelBuilder);

        // その上で、必要があれば付け直せばよい.
        new ProductStructureConfiguration().Configure(
                                modelBuilder.Entity<ProductStructure>());
        new ReplenishmentRuleConfiguration().Configure(
                                modelBuilder.Entity<ReplenishmentRule>());
        new ItemsSupplierConfiguration().Configure(
                                modelBuilder.Entity<ItemsSupplier>());

        // ここから seed data
        // `modelBuilder.Entity<TEntity>().HasData` でシードデータを生成する際、
        // `Id` は自動採番されない
        // seed を追加した後、Add-Migration が必要。何ですと!
        modelBuilder.Entity<PlanVersion>().HasData(
            new PlanVersion() { Id = 1, 
                                IsActual = true,
                                Name = "actual results" } );
    }

    // 計画バージョン
    public DbSet<PlanVersion> PlanVersions { get; set; }
    
    // 販売チャネル
    public DbSet<SalesChannel> SalesChannels { get; set; }

    // 倉庫, DC
    public DbSet<Location> Locations { get; set; }

    /// ///////////////////////////////////////////////////
    // 品目

    // 製品構成表
    public DbSet<ProductStructure> ProductStructures { get; set; }
    public DbSet<ItemCategory> ItemCategories { get; set; }

    // 品目表
    public DbSet<Product> Products { get; set; }

    /// ////////////////////////////////////////////////////////////////
    // 補充計画マスタ

    public DbSet<ItemsSupplier> ItemsSuppliers { get; set; }
    public DbSet<ReplenishmentRule> ReplenishmentRules { get; set; }
    public DbSet<PurchaseGroup> PurchaseGroups { get; set; }

    /// ///////////////////////////////////////////////////
    // トランザクション
    
    public DbSet<DemandTran> DemandTran { get; set; }
    public DbSet<ShipmentTran> ShipmentTran { get; set; }

    // orders
    public DbSet<PurchaseOrder> PurchaseOrders { get; set;}
    public DbSet<BuildOrder> BuildOrders { get; set; }
    public DbSet<TransferOrder> TransferOrders { get; set; }

    //public DbSet<SupplyTran> SupplyTran { get; set; }


    // created_at, updated_at の自動更新
    public override int SaveChanges()
    {
        add_timestamps();
        return base.SaveChanges();
    }

    public override Task<int> SaveChangesAsync(
                        CancellationToken cancellationToken = default)
    {
        add_timestamps();
        return base.SaveChangesAsync(cancellationToken);
    }

    // `created_at`, `updated_at` を更新。
    void add_timestamps()
    {
        var entities = ChangeTracker.Entries()
            .Where(x => x.Entity is MasterBase && 
                   (x.State == EntityState.Added || x.State == EntityState.Modified));

        var now = DateTime.UtcNow; // current datetime
        foreach (var entity in entities) {
            if (entity.State == EntityState.Added)
                ((MasterBase) entity.Entity).CreatedAt = now;
            ((MasterBase) entity.Entity).UpdatedAt = now;
            
            // 全部、検証を回す
            var validationContext = new ValidationContext(entity.Entity);
            Validator.ValidateObject( entity.Entity, validationContext, 
                                      validateAllProperties: true);
        }
    }

}

}
