﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;


namespace WpfPlanning.Models
{

public abstract class MasterBase
{
    [Column("created_at"), Required]
    public DateTime CreatedAt { get; set; }

    [Column("updated_at"), Required]
    public DateTime UpdatedAt { get; set; }

    [Column("lock_version"), Required]
    public int LockVersion { get; set; }
}

}
