﻿
using QuikGraph;
using QuikGraph.Algorithms.TopologicalSort;
using System;
using System.Collections.Generic;
using System.Linq;  // Distinct() 
using WpfPlanning;
using WpfPlanning.common;
using WpfPlanning.Models;


namespace graph_app
{

    /*
    AdjacencyGraph<TVertex, TEdge> where TEdge : IEdge<TVertex> (近接グラフ) クラスは, 次を実装する:
      - IEdgeListAndIncidenceGraph<TVertex, TEdge>, 
      - IMutableVertexAndEdgeListGraph<TVertex, TEdge>
      - ICloneable

    interface IMutableVertexAndEdgeListGraph<TVertex, TEdge> : 
        IMutableVertexListGraph<TVertex, TEdge>, 
        IMutableIncidenceGraph<TVertex, TEdge>, 
        IMutableGraph<TVertex, TEdge>, 
        IGraph<TVertex, TEdge>, 
        IIncidenceGraph<TVertex, TEdge>, 
        IImplicitGraph<TVertex, TEdge>, 
        IImplicitVertexSet<TVertex>, 
        IMutableVertexSet<TVertex>, 
        IVertexSet<TVertex>, 
        IMutableVertexAndEdgeSet<TVertex, TEdge>, 
        IMutableEdgeListGraph<TVertex, TEdge>, 
        IEdgeListGraph<TVertex, TEdge>, 
        IEdgeSet<TVertex, TEdge>, 
        IVertexAndEdgeListGraph<TVertex, TEdge>, 
        IVertexListGraph<TVertex, TEdge>    -- TopologicalSortAlgorithm() はこのインタフェイスを持つグラフを受け入れる
            where TEdge : IEdge<TVertex>
    */


// IEdge<> を実装するのではなく, コンクリートクラス Edge<> を派生させてもよい。
// 「向き」の問題があるので、都度つくるしかないか?
class IandLEdge : Edge<ItemAndLocation>
{
    public EBoMType EdgeType { get; set; }

    public int Weight { get; }

    public int LeadTime { get; }

    public IandLEdge(EBoMType edgeType, 
                    ItemAndLocation source, ItemAndLocation target, int weight,
                    int lead_time) 
            : base(source, target) {  
        EdgeType = edgeType;
        Weight = weight; LeadTime = lead_time;
    }
}


public class Mrp
{ 
    // time stratum 時間層 = 四半期, 月, 週
    public const int TimePeriod = 7;

    // 本来はデータベースから取ってくる
    public Mrp()
    {
    }


    // 計算結果
    public HashSet<ItemAndLocation> Reached;
    public Dictionary<ItemAndLocation, MpsPlan[]> ExpandedDemands;
    public List<PurchaseOrder> PurchaseOrders;
    public List<TransferOrder> TransferOrders;
    public List<BuildOrder> BuildOrders;
    // 計算結果ここまで


    // 頂点と辺の両方を追加する。再帰する
    void addVE(AdjacencyGraph<ItemAndLocation, IandLEdge> graph,
               ItemAndLocation demand ) 
    {
        // 1. 購買ルールがあるか. 供給元へ辿っていく
        var rules = MyApp.DbContext.ReplenishmentRules.Where((x) => 
                        x.Item == demand.Item && x.LocationId == demand.LocationId 
                        //&& MyApp.Locations.ContainsKey(x.Supplier) 
                        ).ToList();
        if (rules.Count > 0) { 
            foreach (var rule in rules ) { 
                if ( rule.SupplierCode != null )
                    continue;
                // 横持ちだけ
                var dc = new ItemAndLocation(demand.Item, rule.DC );
                bool r = Reached.Add(dc);
                if (r)  
                    graph.AddVertex(dc);
                else
                    dc = Reached.Where((x) => x.Equals(dc)).Single();
                graph.AddEdge(new IandLEdge(EBoMType.TRANSFER, 
                                        demand, dc, 1, rule.LeadTime()) );
                if (r)  
                    addVE(graph, dc);
            }
        }
        else {
            // 2. 生産 / kitting
            foreach (var child_ps in demand.Item.bomChildren()) {
                var sub = new ItemAndLocation(child_ps.Child, demand.Location);
                bool r = Reached.Add(sub);
                if (r)
                    graph.AddVertex(sub);
                else
                    sub = Reached.Where((x) => x.Equals(sub)).Single();
                // ● TODO: ASM の場合の必要日数
                graph.AddEdge(new IandLEdge(child_ps.BoMType, 
                                    demand, sub, 
                                    (int) child_ps.Qty,  // weight
                                    child_ps.BoMType == EBoMType.KIT ? 0 : 7
                                ));
                if (r)
                    addVE(graph, sub);
            }
        }
        // ループすると?
        //_graph.AddEdge(new Edge<Item>(_items["構成品5"], _items["商品B"])); 
    }

    // 所要量展開: マスタからグラフを作る
    // @param demands 起点
    AdjacencyGraph<ItemAndLocation, IandLEdge> 
        build_graph(IList<DemandTran> demands, IList<ItemAndLocation> iloc_list)
    {
        // output
        this.Reached = new HashSet<ItemAndLocation>(); //DemandCompareByILoc.instance());

        // adjacency graph 隣接グラフ
        var graph = new AdjacencyGraph<ItemAndLocation, IandLEdge>();

        // 需要がある頂点のみ。
        foreach (var iloc in iloc_list) { 
            Reached.Add(iloc);
            graph.AddVertex(iloc);
        }
        // 展開して, 頂点と辺を追加
        foreach (var iloc in iloc_list )  
            addVE(graph, iloc);

        return graph;
    }

    // 実引数
    DateOnly _start_day;
    List<DemandTran> _final_demands ;
    List<ShipmentTran> _shipments;

    // 先頭に期首残高レコードがある `_start_day` は 2番目のレコード
    List<DateOnly> _dates;


    int date_to_idx(DateOnly date) { 
        var interval = Ext.DateSubtract(date, _start_day);
        return (interval.Days / TimePeriod) + 1;
    }

    // TimePeriod 単位の日付、期間外は直前の期間に直す
    int added_date_idx(DateOnly dt, int LeadTime) {
        var span = ((int) Math.Round(((decimal) LeadTime) / TimePeriod, 
                                     MidpointRounding.AwayFromZero)) * // 切り上げる?
                   TimePeriod;
        var r = dt.AddDays(+span);
        if ( r < _start_day )
            return 0;
        
        return date_to_idx(r);
    }


    int make_build_order(DateOnly receiveDate, IandLEdge edge, int needs)
    {
        if (needs <= 0)
            throw new ArgumentOutOfRangeException(nameof(needs));
        if (edge.Source.Location != edge.Target.Location) 
            throw new ArgumentOutOfRangeException(nameof(edge.Source.Location));

        DateOnly issueDate = receiveDate.AddDays(-edge.LeadTime);
        if (issueDate < _start_day)
            return 0;

        BuildOrders.Add(new BuildOrder {
                    SendoutDate = issueDate,
                    ReceiveILoc = new ItemAndLocation(edge.Source.Item, 
                                                      edge.Source.Location),
                    ReceiveDate = receiveDate,
                    QtyToReceive = needs,
                    InputItem = edge.Target.Item,
                    InputQty = needs * edge.Weight } );
        
        // kitting
        return needs;
    }

    // @return 実際に調達可能な数量. 0のことがありえる
    int make_transfer_order(DateOnly receiveDate, IandLEdge edge, int needs)
    {
        if (needs <= 0)
            throw new ArgumentOutOfRangeException(nameof(needs));
        if (edge.Source.Location == edge.Target.Location) 
            throw new ArgumentOutOfRangeException(nameof(edge.Source.Location));

        DateOnly issueDate = receiveDate.AddDays(-edge.LeadTime);
        if (issueDate < _start_day)
            return 0;

        TransferOrders.Add(new TransferOrder { 
                    SendoutDate = issueDate,
                    ReceiveILoc = new ItemAndLocation(edge.Source.Item, 
                                                      edge.Source.Location),
                    ReceiveDate = receiveDate,
                    QtyToReceive = needs,
                    ShipFrom = edge.Target.Location, // target が出し側. 
                        } );
        return needs;
    }


    // @return 実際に調達可能な数量. 0のことがありえる
    int make_pur_order(DateOnly receiveDate, ReplenishmentRule rule, int needs) 
    {
        if (needs <= 0)
            throw new ArgumentOutOfRangeException(nameof(needs));

        DateOnly issueDate = receiveDate.AddDays(-rule.LeadTime());
        if (issueDate < _start_day)
            return 0;

        int qty = int.Max(rule.MinLot, needs);
        PurchaseOrders.Add(new PurchaseOrder {
                    SendoutDate = issueDate,
                    ReceiveILoc = new ItemAndLocation(rule.Item, rule.Location),
                    ReceiveDate = receiveDate,
                    QtyToReceive = qty,
                    SupplierCode = rule.SupplierCode });
        return qty;
    }


    // Step 1: (過去と) 子に向かって需要を展開し, 足し込んでいく
    // トポロジカルソート済み = 再帰不要
    // @param dt 需要 (受け取りたい) 日付
    int expand_requirements2(ItemAndLocation iloc, List<IandLEdge> sub_edges, 
                                DateOnly dt, int net_req) 
    {
        if (net_req <= 0)
            throw new ArgumentOutOfRangeException(nameof(net_req));

        int supply = 0;

        // KITTING or 製造. この場合だけ supply は min()
        bool first = true;
        foreach (IandLEdge edge in sub_edges) {
            if (edge.Source.Location != edge.Target.Location)
                continue;

            int target_day_idx = added_date_idx(dt, -edge.LeadTime);
            // ● TODO: サイクル日数の考慮。サイクル期間の合計分が必要
            int s = make_build_order(dt, edge, net_req );

            var ex = ExpandedDemands[new ItemAndLocation(edge.Target.Item, 
                                                         edge.Target.Location)]
                                    [target_day_idx];
            // 中間需要
            ex.GrossReq += net_req * edge.Weight;

            supply = first ? s : Math.Min(supply, s);
            first = false;
        }

        // 横持ち
        foreach (IandLEdge edge in sub_edges) {
            if ( supply >= net_req ) break;

            if (edge.Source.Location == edge.Target.Location)
                continue;
                
            int target_day_idx = added_date_idx(dt, -edge.LeadTime);
            // ● TODO: サイクル日数の考慮。サイクル期間の合計分が必要
            // 本当は依頼先の優先度もあるはず
            int s = make_transfer_order(dt, edge, net_req - supply);

            var ex = ExpandedDemands[new ItemAndLocation(edge.Target.Item, 
                                                         edge.Target.Location)]
                                    [target_day_idx];
            // 中間需要
            ex.GrossReq += net_req - supply ;
            supply += s;
        }

        // 購買
        var pur = MyApp.DbContext.ReplenishmentRules.Where((x) => 
                                    x.ProductId == iloc.ProductId && 
                                    x.LocationId == iloc.LocationId);
        foreach (ReplenishmentRule rule in pur) {
            if (supply >= net_req) break;

            if (rule.SupplierCode != null) {
                // ● TODO: サイクル日数の考慮。サイクル期間の合計分が必要
                int s = make_pur_order(dt, rule, net_req - supply);
                supply += s; 
            }
        }

        return supply;
    }

    // Step 2
    int net_supply(DateOnly receiveDate, ItemAndLocation iloc)
    {
        // KITTING or 製造。min() する
        // 要素がないとき 'Sequence contains no elements' error
        var t = BuildOrders.FindAll(x => x.ReceiveDate == receiveDate &&
                                    x.ReceiveILoc.Equals(iloc) ) ;
        int tt = t.Count > 0 ? t.Min(x => x.QtyToReceive) : 0 ;

        // 兄弟ノードの数量を削る
        foreach (BuildOrder order in t) {
            if (order.QtyToReceive > tt) {
                int new_input = tt * (order.InputQty / order.QtyToReceive);  
                this.ExpandedDemands[new ItemAndLocation(order.InputItem,
                                                         iloc.Location)]
                            [date_to_idx(order.SendoutDate)].Consumed -= order.InputQty - new_input;
                order.QtyToReceive = tt;
                order.InputQty     = new_input;
            }
            // 在庫数は step 3で合わせる
        }

        // 横持ち
        int u = TransferOrders.FindAll(x => x.ReceiveDate == receiveDate &&
                                    x.ReceiveILoc.Equals(iloc) )
                              .Sum(x => x.QtyToReceive);
        // 購買
        int v = PurchaseOrders.FindAll(x => x.ReceiveDate == receiveDate &&
                                    x.ReceiveILoc.Equals(iloc) )
                              .Sum(x => x.QtyToReceive);

        return tt + u + v;
    }

    // Step 2: 子部品から最終需要側に向かって, 未充足需要を削り込んでいく
    int reduce_uncovered_demands( DateOnly supply_date, 
                                  ItemAndLocation v, double rate)
    {
        if (!(rate >= 0.0 && rate <= 1.0) ) 
            throw new ArgumentOutOfRangeException(nameof(rate));

        // 同じ率で削る ● TODO: 本当は利益を考慮しながら削る
        int consumed = 0;

        // Final demands の提供可能数の考慮
        var f = _final_demands.Find(x => x.ProductId == v.ProductId && 
                                x.SalesChannel.LocationId == v.LocationId &&
                                x.SaleDate == supply_date);
        if (f != null) {
            int q = (int) (f.QtySold * rate);
            consumed += q;
        }

        // 横持ちの出し側
        var t = TransferOrders.FindAll(x => x.SendoutDate == supply_date && 
                                    x.ReceiveILoc.Item == v.Item && 
                                    x.ShipFrom == v.Location);
        foreach (TransferOrder order in t) {
            int q = (int) (((double) order.QtyToReceive) * rate);
            //ExpandedDemands[order.ReceiveILoc][date_to_idx(order.ReceiveDate)].PrpslRecieved -= order.Qty - q;
            //if (ExpandedDemands[order.ReceiveILoc][date_to_idx(order.ReceiveDate)].PrpslRecieved < 0)
            //    throw new Exception("internal error");
            order.QtyToReceive = q;
            consumed += order.QtyToReceive;
        }

        // 製造の子部品側
        var b = BuildOrders.FindAll(x => x.SendoutDate == supply_date && 
                                        x.InputItem == v.Item &&
                                        x.ReceiveILoc.Location == v.Location);
        foreach (BuildOrder order in b) {
            int output_q = (int) (((double) order.QtyToReceive) * rate);
            // ここで引いたら引きすぎる. min() しないといけない
            //ExpandedDemands[order.ReceiveILoc][date_to_idx(order.ReceiveDate)].PrpslRecieved -= order.Qty - output_q;
            //if (ExpandedDemands[order.ReceiveILoc][date_to_idx(order.ReceiveDate)].PrpslRecieved < 0)
            //    throw new Exception("internal error");
            order.InputQty     = output_q * (order.InputQty / order.QtyToReceive);
            order.QtyToReceive = output_q;
            consumed += order.InputQty;
        }

        return consumed;
    }


    // 横持ちであれ購買発注であれ, 在庫がない場合のみ order が発生。
    // ある日付 D について, 
    //  1. loop:
    //     最終需要側からそれぞれの品目について, 総需要にもとづき、仮の order をつくる
    //  2. loop:
    //     末端側からそれぞれの品目について, 要求合計を満たせない場合、削る
    //       -> ここに工夫があるだろうが、いったん比例で。   
    //       -> これでまた親の側で, 別の子部品への要求が減る (ここは減るのみ)
    //     つど, 日付 D まで在庫更新     
    //  3. loop:
    //     もう一度最終需要側から, order の整合性を取りながら展開する
    //     購買発注が不要になることがありえる
    //
    // @param horizon_end この日を含む週まで
    // @param demands 最終需要
    // @param shipments  手前に1列追加。Stock に在庫数を入れる
    public void run2( DateOnly start_day, DateOnly horizon_end, 
                      List<DemandTran> demands, List<ShipmentTran> shipments,
                      List<ItemAndLocation> demand_iloc_list)
    {
        // 実引数
        this._start_day = start_day;
        this._final_demands = demands;
        this._shipments = shipments;
        // 結果をクリア
        this.ExpandedDemands = new Dictionary<ItemAndLocation, MpsPlan[]>();
        this.PurchaseOrders = new List<PurchaseOrder>();
        this.TransferOrders = new List<TransferOrder>();
        this.BuildOrders = new List<BuildOrder>();

        // 展開して、必要な頂点を展開し, 辺を張る
        AdjacencyGraph<ItemAndLocation, IandLEdge> graph = 
                                    build_graph(demands, demand_iloc_list);
        var sorter = new TopologicalSortAlgorithm<ItemAndLocation, IandLEdge>(graph);
        sorter.Compute(); // ループがあると例外: QuikGraph.NonAcyclicGraphException: 'The graph contains at least one cycle.'

        // 期間を決める
        this._dates = new List<DateOnly>();
        //demands.Sort((a, b) =>  a.Date.CompareTo(b.Date)); // 破壊的メソッド
        // 1. データが空白の期間があるかも. 2. 期首残高の列を追加
        for (var d = _start_day.AddDays(-TimePeriod); d <= horizon_end; 
                                                    d = d.AddDays(TimePeriod)) {   
            this._dates.Add(d);
        }

        // 事前準備. 
        foreach (var v in sorter.SortedVertices) { 
            this.ExpandedDemands[v] = new MpsPlan[_dates.Count] ;
            for (int i = 0; i < _dates.Count; ++i)
                ExpandedDemands[v][i] = new MpsPlan();

            // 期首在庫
            ShipmentTran? st = shipments.Find((x) => 
                                    x.ProductId == v.ProductId && 
                                    x.LocationId == v.LocationId &&
                                    x.ShipmentDate == _start_day.AddDays(-TimePeriod));
            ExpandedDemands[v][0].Stock = st != null ? st.OnHand : 0;
            
            // 自分自身の最終需要を先に転記。二重カウント避け
            for (int dt_idx = 1; dt_idx < _dates.Count; ++dt_idx) { 
                DemandTran? me = demands.Find((x) => 
                                    x.ProductId == v.ProductId && 
                                    x.SalesChannel.LocationId == v.LocationId &&
                                    x.SaleDate == _dates[dt_idx]);
                ExpandedDemands[v][dt_idx].GrossReq = 
                                    me != null ? (int) me.QtySold : 0;
            }
        }

        // Step 1
        foreach (ItemAndLocation v in sorter.SortedVertices) {
            // 期首在庫数
            int stock = ExpandedDemands[v][0].Stock;

            for (int dt_idx = 1; dt_idx < _dates.Count; ++dt_idx) {
                DateOnly fnl_req_dt = _dates[dt_idx];
 
                // 自分あての (最終需要 + 中間需要)
                int gross_qty = ExpandedDemands[v][dt_idx].GrossReq;
                int net_req = -(stock + ExpandedDemands[v][dt_idx].IssuedOrders - 
                                gross_qty); // ● TODO: ここに安全在庫

                int supply = 0;
                if (net_req > 0) { 
                    // `addVE()` で張った辺 (のうち一部) だけを辿ればOK
                    var dest_edges = graph.Edges.ToList().FindAll((x) => 
                                        x.Source.Item == v.Item &&
                                        x.Source.Location == v.Location );
                    // このなかで, 仮に order をつくる
                    supply = expand_requirements2(v, dest_edges, fnl_req_dt, 
                                                  net_req);
                }

                stock = stock + ExpandedDemands[v][dt_idx].IssuedOrders - 
                        gross_qty + supply; 
                //int uncovered = 0; 
                if (stock < 0) {
                    //uncovered = -stock;
                    stock = 0;
                }

                //ExpandedDemands[v][dt_idx].GrossReq = gross_qty;
                ExpandedDemands[v][dt_idx].PrpslRecieved = supply;
                ExpandedDemands[v][dt_idx].Stock = stock ;
                //ExpandedDemands[v][dt_idx].Uncovered = uncovered;
            }
        }

        // Step 2: 子部品から最終需要へ
        foreach (ItemAndLocation v in Ext.ReverseIterator(sorter.SortedVertices)) { 
            // 期首在庫数
            int stock = ExpandedDemands[v][0].Stock;

            for (int dt_idx = 1; dt_idx < _dates.Count; ++dt_idx) { 
                DateOnly dt = _dates[dt_idx];

                // 確定分と提案分. このなかで兄弟ノードも削る
                int supply = net_supply(dt, v);
                int received = ExpandedDemands[v][dt_idx].IssuedOrders + supply;

                int consumed ; // uncovered
                if (ExpandedDemands[v][dt_idx].GrossReq <= stock + received) { 
                    consumed = ExpandedDemands[v][dt_idx].GrossReq;
                    //uncovered = 0;
                }
                else {
                    // 削り込む
                    double rate = ((double) (stock + received)) / 
                                  ExpandedDemands[v][dt_idx].GrossReq;
                    consumed = reduce_uncovered_demands(dt, v, rate);
                    //uncovered = ExpandedDemands[v][dt_idx].GrossReq - consumed;
                }

                stock = stock + received - consumed; 
                if (stock < 0) 
                    throw new Exception("internal error");
                ExpandedDemands[v][dt_idx].PrpslRecieved = supply;
                ExpandedDemands[v][dt_idx].Consumed = consumed;
                ExpandedDemands[v][dt_idx].Stock = stock;
                //ExpandedDemands[v][dt_idx].Uncovered = uncovered;
            }
        }
        
        // Step 3: 在庫数と未充足 uncovered を合わせる. 特に再計算はしない
        foreach (ItemAndLocation v in sorter.SortedVertices) {
            // 期首在庫数
            int stock = ExpandedDemands[v][0].Stock;

            for (int dt_idx = 1; dt_idx < _dates.Count; ++dt_idx) {
                DateOnly dt = _dates[dt_idx];

                int received = ExpandedDemands[v][dt_idx].IssuedOrders +
                               ExpandedDemands[v][dt_idx].PrpslRecieved;
                stock = stock + received - ExpandedDemands[v][dt_idx].Consumed; 
                if (stock < 0) 
                    throw new Exception("internal error");

                ExpandedDemands[v][dt_idx].Stock = stock;
                ExpandedDemands[v][dt_idx].Uncovered = 
                                ExpandedDemands[v][dt_idx].GrossReq -
                                ExpandedDemands[v][dt_idx].Consumed;
            }
        }
    }
}


}
