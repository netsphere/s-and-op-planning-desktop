﻿
using ClosedXML.Excel;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;


namespace WpfPlanning.Models
{

class ImportTransactionData : ImportExcelBase
{
    const int START_LINE = 3; // 0 始まり

    //XLWorkbook _workbook;
    PlanVersion _planVersion;

    public ImportTransactionData(PlanVersion planVersion)
    {
        // _workbook = workbook;
        _planVersion = planVersion;
    }


    public override void importMasterDataFile()
    {
        var sheets = new Dictionary<string, IXLWorksheet>();

        foreach (var s in _workbook.Worksheets)
            sheets.Add(s.Name.ToLower().Replace(" ", ""), s);

        IXLWorksheet sheet;

        if (sheets.TryGetValue("transactions", out sheet))
            importTransactionsSheet(sheet);
        if (sheets.TryGetValue("orderstoreceive", out sheet))
            importOrdersToReceiveSheet(sheet);
        if (sheets.TryGetValue("orderstoship", out sheet))
            importOrdersToShipSheet(sheet);

        MyApp.DbContext.SaveChanges();
    }


        // [Transactions] sheet
    void importTransactionsSheet(IXLWorksheet sheet)
    {
        var titles = make_title_indexes(sheet.Row(1));

        for (int i = START_LINE; i < sheet.Rows().Count(); ++i) {
            IXLRow line = sheet.Row(i + 1);

            Product prod = MyApp.DbContext.Products
                    .Where((x) => x.ItemCode == get_value(line, titles, "itemcode").ToUpper())
                    .Include(x => x.ItemCategory).First();

            // Location は供給側で整理
            SalesChannel channel = MyApp.DbContext.SalesChannels
                    .Where((x) => x.ChannelCode == get_value(line, titles,"channel").ToUpper())
                    .First();

            // 受注残
            var UnfilledOrders = int.Parse(get_value(line,titles,
                                                     "unfilledorders"));

            var demand = new DemandTran() {
                    PlanVersionId = _planVersion.Id ,
                    SaleDate = DateOnly.Parse(get_value(line, titles, "date")),
                    // 比例配分があるので、ここだけ double
                    QtySold = double.Parse(get_value(line, titles, "quantitysold")),
                    SalesAmount = double.Parse(get_value(line, titles, 
                                                         "transactionrevenue")),
                    SalesCostAmount = double.Parse(get_value(line, titles,
                                                             "salescost")),
                    ProductId = prod.Id,
                    SalesChannelId = channel.Id,
                    UnfilledOrders = UnfilledOrders,  };

            // 非正規化
            demand.CatL1 = prod.ItemCategory.ParentId != null ? 
                                (int) prod.ItemCategory.ParentId : prod.ItemCategory.Id;
            demand.CatL2 = prod.ItemCategory.ParentId != null ? 
                                prod.ItemCategory.Id : null;
/*
            demand.ChannelL1Id = channel.ParentId != null ? 
                                (int) channel.ParentId : channel.Id;
            demand.ChannelL2Id = channel.ParentId != null ?
                                channel.Id : null;
*/
            MyApp.DbContext.DemandTran.Add(demand);
        }
        MyApp.DbContext.SaveChanges();
    }


    void importOrdersToReceiveSheet(IXLWorksheet sheet)
    {
        var titles = make_title_indexes(sheet.Row(1));

        for (int i = START_LINE; i < sheet.Rows().Count(); ++i) {
            IXLRow line = sheet.Row(i + 1);

            int ItemId = MyApp.DbContext.Products.Single(
                                (x) => x.ItemCode == get_value(line, titles, "itemcode").ToUpper()).Id;
            int LocId = MyApp.DbContext.Locations.Single(
                                (x) => x.LocationCode == get_value(line,titles,"location").ToUpper()).Id;
            var order_type = get_value(line,titles,"ordertype").ToLower();
            if (order_type != "purchase")
                throw new ArgumentException("order type must be 'purchase'");

            // 取引先からの仕入に限定. 
            string source = get_value(line, titles, "sourcefrom").ToUpper();
            ItemsSupplier? suppl = MyApp.DbContext.ItemsSuppliers.FirstOrDefault(
                                    (x) => x.ProductId == ItemId && 
                                           x.SupplierCode == source);
            if (suppl == null)
                throw new ArgumentException("Supplier not found in the `ItemsSuppliers`");

            var Replenishment = MyApp.DbContext.ReplenishmentRules.FirstOrDefault(
                                    (x) => x.ProductId == ItemId && 
                                           x.LocationId == LocId &&
                                           x.SupplierCode == source);
            if (Replenishment == null)
                throw new ArgumentException("supplier not found in the `ReplenishmentMaster`");

            var order = new PurchaseOrder() {
                    PlanVersionId = _planVersion.Id,
                    ReceiveILoc = new ItemAndLocation() {ProductId = ItemId, 
                                                         LocationId = LocId},
                    SendoutDate = DateOnly.Parse(get_value(line,titles,"sendoutdate")),
                    DeliveryDate = DateOnly.Parse(get_value(line,titles,"deliverydate")),
                    ReceiveDate = DateOnly.Parse(get_value(line,titles,"arrivaldate")),
                    QtyToReceive = int.Parse(get_value(line,titles,"qtytoreceive")),
                    LotCost = double.Parse(get_value(line,titles,"lotcost")),
                    FreightCost = double.Parse(get_value(line,titles,"freightcost")),
                    OrderNumber = get_value(line,titles,"ordernumber"),
                    OrderType = OrderType.PURCHASE,
                    SupplierCode = source, };
            order.LotCostCurrency = get_value(line,titles,"lotcostcurr").ToUpper();
            if (order.LotCostCurrency != suppl.SuppliersCurrency)
                throw new ArgumentException("currency mismatch");

            order.LotCostInFnCurr = double.Parse(get_value(line,titles,"lotcostinfncurr"));

            MyApp.DbContext.PurchaseOrders.Add(order);
        }
        MyApp.DbContext.SaveChanges();            
    }

    // 顧客への出荷
    void importOrdersToShipSheet(IXLWorksheet sheet)
    {
        var titles = make_title_indexes(sheet.Row(1));

        for (int i = START_LINE; i < sheet.Rows().Count(); ++i) {
            IXLRow line = sheet.Row(i + 1);

            Product item = MyApp.DbContext.Products
                    .Where((x) => x.ItemCode == get_value(line, titles, "itemcode").ToUpper())
                    .Include(x => x.ItemCategory).First();
            var LocId = MyApp.DbContext.Locations.Single(
                                (x) => x.LocationCode == get_value(line,titles,"location").ToUpper()).Id;

            string onHand = get_value(line,titles,"onhand");

            try { 
                var shipment = new ShipmentTran() {
                    PlanVersionId = _planVersion.Id,
                    ShipmentDate = DateOnly.Parse(get_value(line,titles,"shipmentdate")),
                    ProductId = item.Id,
                    LocationId = LocId,
                    QtyShipped = int.Parse(get_value(line,titles,"qtytoship")),
                    OnHand = (onHand != "" ? int.Parse(onHand) : 0) };

                // 非正規化
                shipment.CatL1 = item.ItemCategory.ParentId != null ? 
                                (int) item.ItemCategory.ParentId : item.ItemCategory.Id;
                shipment.CatL2 = item.ItemCategory.ParentId != null ? 
                                item.ItemCategory.Id : null;

                MyApp.DbContext.ShipmentTran.Add(shipment);
            } catch (Exception ex) {
                MessageBox.Show("error: " + ex.Message);
            }
        }
        MyApp.DbContext.SaveChanges();
    }

}

}
