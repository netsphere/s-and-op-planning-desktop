﻿
using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Linq;


namespace WpfPlanning.Models
{

abstract class ImportExcelBase
{
    // 正規化する
    // @return インデックスは 1 始まり
    public static Dictionary<string, int> make_title_indexes(IXLRow title_line)
    {
        var ret = new Dictionary<string, int>();
        // CellCount() => 16384. これじゃない
        for (int j = 0; j < title_line.Cells().Count(); ++j) {
            var title = title_line.Cell(j + 1).GetFormattedString().ToLower()
                                  .Replace(" ", "");
            ret.Add(title, j + 1);
        }

        return ret;
    }

    public static string get_value(IXLRow data_line,
                                   Dictionary<string, int> titles, string name)
    {
        // `GetText()` は例外になる。ToString() は "A3" (セル番号).
        string value = data_line.Cell(titles[name.ToLower().Replace(" ","")])
                                .GetFormattedString().Trim();
        return value;
    }

    public static bool get_bool(IXLRow data_line,
                                  Dictionary<string, int> titles, string name)
    { 
        string value = get_value(data_line, titles, name).ToLower();
        if (value == "" || value == "false" || value == "no" || value == "off" || value == "0")
            return false;
        if (value == "true" || value == "yes" || value == "on" || value == "1" )
            return true;
        throw new ArgumentException("not bool");
    }


    protected XLWorkbook _workbook;

    public void open(XLWorkbook workbook) { 
        _workbook = workbook; 
    }

    public abstract void importMasterDataFile();
}


class ImportMasterData : ImportExcelBase
{
    const int START_LINE = 3; // 0 始まり


    public ImportMasterData() {
    }


    // 2. [Replenishment] タブ
    // Location (to) との組み合わせの設定.
    void importReplenishmentSheet(IXLWorksheet sheet)
    {
        // index は 1始まり
        var titles = make_title_indexes(sheet.Row(1));

        for (int i = START_LINE; i < sheet.Rows().Count(); ++i) {
            IXLRow line = sheet.Row(i + 1);

            Product? pk1 = MyApp.DbContext.Products.FirstOrDefault(
                            (x) => x.ItemCode == get_value(line, titles, "itemcode").ToUpper());
            if (pk1 == null)
                throw new ArgumentException("item not found");
            Models.Location pk2 = MyApp.DbContext.Locations.Single(
                            (x) => x.LocationCode == get_value(line, titles,"location").ToUpper());

            // `Single()` は見つからなかったときに例外!
            var rep = MyApp.DbContext.ReplenishmentRules.FirstOrDefault(
                            (g) => g.ProductId == pk1.Id && g.LocationId == pk2.Id);
            if ( rep == null ) {
                rep = new ReplenishmentRule() { ProductId = pk1.Id, 
                                                  LocationId = pk2.Id };
                MyApp.DbContext.ReplenishmentRules.Add(rep);
            }
            else 
                MyApp.DbContext.ReplenishmentRules.Update(rep);

            rep.LastOnHand = int.Parse(get_value(line,titles, "lastonhand"));
            rep.ProcurementTime = int.Parse(get_value(line,titles,
                                                      "procurementtime"));
            rep.ShippingTime = int.Parse(get_value(line,titles,"shippingtime"));
            rep.LeadTimeVariance = double.Parse(
                                get_value(line,titles, "leadtimevariance"));
            
            rep.OrderCycle = int.Parse(get_value(line,titles, "ordercycle"));
            rep.OrderingDays = get_value(line,titles, "orderingdays");
            rep.Rounding = int.Parse(get_value(line,titles, "rounding"));
            if (rep.Rounding <= 0)
                throw new ArgumentException("Rounding must > 0");

            rep.MinLot = int.Parse(get_value(line,titles, "minlot"));
            if (rep.MinLot <= 0)
                throw new ArgumentException("MOQ must > 0");

            // From どちらか
            var sup_code = get_value(line,titles, "suppliercode").ToUpper();
            if (sup_code != null && !sup_code.Equals("")) { 
                var sup = MyApp.DbContext.ItemsSuppliers.Single(
                                    (x) => x.ProductId == pk1.Id && 
                                           x.SupplierCode == sup_code);
                if (sup == null)    
                    throw new ArgumentException("supplier not found");
                rep.SupplierCode = sup_code;
            }
            else { 
                var dc_name = get_value(line,titles, "dcname").ToUpper();
                Location dc = MyApp.DbContext.Locations.Single(
                                            (x) => x.LocationCode == dc_name);
                if (dc == null)
                    throw new ArgumentException("dc_name not found");
                rep.DCId = dc.Id;
            }
        }

        MyApp.DbContext.SaveChanges();
    }
    

    // 3. [Purchase Group] タブ. SKUをまたぐ丸め、制約の設定
    void importPurchaseGroupSheet(IXLWorksheet sheet)
    {
        // index は 1始まり
        var title = sheet.Row(1);

        for (int i = START_LINE; i < sheet.Rows().Count(); ++i) {
            var pg = new PurchaseGroup();
            var line = sheet.Row(i + 1);

            for (int j = 0; j < sheet.Columns().Count(); ++j) {
                string value = line.Cell(j+1).GetFormattedString().Trim();
                switch (title.Cell(j + 1).GetFormattedString().ToLower().Replace(" ", "") ) 
                {
                case "groupid":    // PK
                    var x = MyApp.DbContext.PurchaseGroups.FirstOrDefault(
                                (c) => c.GroupCode == value.ToUpper());
                    if (x != null) { 
                        pg = x;
                        MyApp.DbContext.PurchaseGroups.Update(pg);
                    }
                    else {
                        pg.GroupCode = value.ToUpper();
                        MyApp.DbContext.PurchaseGroups.Add(pg);
                    }
                    break;
                case "groupname":
                    pg.Name = value;  break;
                case "groupmin.weight":
                    pg.GroupMinWeight = value != "" ? double.Parse(value) : 0.0;
                    break;
                case "groupmin.volume":
                    pg.GroupMinVolume = value != "" ? double.Parse(value) : 0.0;
                    break;
                case "groupmin.qty":
                    pg.GroupMinQty = value != "" ? int.Parse(value) : 0;
                    break;
                case "groupmin.cost":
                    pg.GroupMinCost = value != "" ? double.Parse(value) : 0.0;
                    break;
                default:
                    break;
                }
            }
            //MyApp.DbContext.PurchaseGroups.Add(pg);
        }
        MyApp.DbContext.SaveChanges();
    }


    // [Location] tab
    void importLocationSheet(IXLWorksheet sheet)
    { 
        var titles = make_title_indexes(sheet.Row(1));

        for (int i = START_LINE; i < sheet.Rows().Count(); ++i) {
            var line = sheet.Row(i+1);

            string pk = get_value(line, titles, "locationcode").ToUpper();
            var loc = MyApp.DbContext.Locations.FirstOrDefault(
                                            (x) => x.LocationCode == pk);
            if (loc == null) {
                loc = new Models.Location() { LocationCode = pk };
                MyApp.DbContext.Locations.Add(loc);
            }
            else
                MyApp.DbContext.Locations.Update(loc);

            loc.Description = get_value(line, titles, "locationdescription");

            var parent_code = get_value(line, titles, "parentcode").ToUpper();
            loc.ParentId = parent_code != "" ?
                    MyApp.DbContext.Locations.Single(
                                        (x) => x.LocationCode == parent_code).Id :
                    null;

            loc.Level = int.Parse(get_value(line, titles, "level"));

            MyApp.DbContext.SaveChanges(); // id を生成!
        }
    }


    // 6. [Sales Channel] tab
    void importSalesChannelSheet(IXLWorksheet sheet)
    {
        // index は 1始まり
        var titles = make_title_indexes(sheet.Row(1));

        for (int i = START_LINE; i < sheet.Rows().Count(); ++i) {
            IXLRow cur_row = sheet.Row(i+1);
            var line = (string x) => { return get_value(cur_row, titles, x); };
            
            string pk = line("channelcode").ToUpper();
            var sc = MyApp.DbContext.SalesChannels.FirstOrDefault(
                                                (x) => x.ChannelCode == pk);
            if (sc == null) {
                sc = new SalesChannel() { ChannelCode = pk };
                MyApp.DbContext.SalesChannels.Add(sc);
            }
            else
                MyApp.DbContext.SalesChannels.Update(sc);

            // これが名前
            sc.Description = line("channeldescription");

            var parent_code = line("parentcode").ToUpper();
            sc.ParentId = parent_code != "" ?
                    MyApp.DbContext.SalesChannels.Single(
                                    (x) => x.ChannelCode == parent_code).Id :
                    null;

            sc.Level = int.Parse(line("level"));

            // 独自。供給サイドと繋ぐ
            var loc_code = line("location").ToUpper();
            sc.LocationId = loc_code != "" ?
                    MyApp.DbContext.Locations.Single(
                                    (x) => x.LocationCode == loc_code).Id :
                    null;
                    
            MyApp.DbContext.SaveChanges() ;  // 一つずつ保存, id を生成
        }
    }


    // 事前確認なしに、読み込んでしまう。TODO: 改善
    public override void importMasterDataFile()
    {
        var sheets = new Dictionary<string, IXLWorksheet>();
        foreach (var s in _workbook.Worksheets) 
            sheets.Add(s.Name.ToLower().Replace(" ", ""), s);

        IXLWorksheet sheet;

        if (sheets.TryGetValue("purchasegroup", out sheet))
            importPurchaseGroupSheet(sheet); 
        //MyApp.DbContext.SaveChanges(); return;  // ●●DEBUG DEBUG DEBUG

        // 供給サイド
        if (sheets.TryGetValue("region", out sheet))
            importLocationSheet(sheet);
        if (sheets.TryGetValue("replenishment", out sheet))
            importReplenishmentSheet(sheet); 

        // 需要サイド
        if (sheets.TryGetValue("saleshierarchy", out sheet))
            importSalesChannelSheet(sheet);   

        MyApp.DbContext.SaveChanges();
    }

}


// 商品マスタのみ. 新商品の追加
class ImportProductMaster : ImportExcelBase
{ 
    const int START_LINE = 3; // 0 始まり

    //readonly XLWorkbook _workbook;

    // コンストラクタ
    public ImportProductMaster() {
    }


    // 1. [Item Info] タブ
    void importItemInfoSheet(IXLWorksheet sheet)
    {
        // index は 1始まり
        var titles = make_title_indexes(sheet.Row(1));

        // sheet.RowCount() => 1048576, sheet.ColumnCount() => 16384
        // 実際の行数ではない. -> sheet.Rows().Count() を使え.
        for (int i = START_LINE; i < sheet.Rows().Count(); ++i) {
            IXLRow line = sheet.Row(i + 1);

            string pk = get_value(line, titles, "itemcode").ToUpper();
            string pk2 = get_value(line, titles, "suppliercode").ToUpper();

            // Item info ///////////////////////////////////////
            var prod = MyApp.DbContext.Products.FirstOrDefault(
                                                    (x) => x.ItemCode == pk );
            if (prod == null) { 
                prod = new Product() { ItemCode = pk };
                MyApp.DbContext.Products.Add(prod);
            }
            else  
                MyApp.DbContext.Products.Update(prod);
    
            //prod.Name = get_value("itemname");
            prod.Description = get_value(line, titles, "itemdescription");

            var cat_s = get_value(line, titles, "itemcategory").ToUpper();
            if (cat_s == "") 
                throw new ArgumentException("item category missing");
            var cat = MyApp.DbContext.ItemCategories.FirstOrDefault(
                                            (c) => c.CategoryCode == cat_s);
            if (cat == null)
                throw new ArgumentException("item category not found");
            prod.ItemCategoryId = cat.Id;
            
            prod.IsSaleable = get_bool(line,titles,"issaleable");
            if (prod.IsSaleable && prod.ItemCategoryId == null) 
                throw new ArgumentException("item category is missing");
           
            var weight = get_value(line, titles, "weight/unit");
            prod.UnitWeightKg = weight != "" ? double.Parse(weight) : 0.00; 

            var volume = get_value(line, titles, "volume/unit");
            prod.UnitVolumeCm3 = volume != "" ? double.Parse(volume) : 0.00;

            if (prod.IsSaleable) {
                var salesprice = get_value(line, titles, "salesprice/unit");
                prod.UnitSalesPrice = salesprice != "" ? double.Parse(salesprice) : 0.00;
            }
            else
                prod.UnitSalesPrice = 0.0;
            // 機能通貨建て
            var inve_value = get_value(line, titles, "inventoryvalue/unit");
            prod.UnitInventoryValue = inve_value != "" ? double.Parse(inve_value) : 0.00;

            string t = get_value(line, titles,"inactive");
            prod.Inactive = t != "" ? DateOnly.Parse(t) : null;

            MyApp.DbContext.SaveChanges(); // id を生成!

            // items - suppliers record  ////////////////////////
            // Terminated だがマスタあり、がある
            if (pk2 != "") { 
                var prod_sup = MyApp.DbContext.ItemsSuppliers.FirstOrDefault(
                                    (x) => x.ProductId == prod.Id && 
                                           x.SupplierCode == pk2);
                if (prod_sup == null) {
                    // "サプライヤ" レコードはない。
                    prod_sup = new ItemsSupplier() { SupplierCode = pk2,
                                                     ProductId = prod.Id };
                    MyApp.DbContext.ItemsSuppliers.Add(prod_sup);
                }
                else
                    MyApp.DbContext.ItemsSuppliers.Update(prod_sup);

                var mpn = get_value(line, titles, "supplier'sitemcode");
                prod_sup.SuppliersItemCode = mpn != "" ? mpn : null;
                prod_sup.SuppliersCurrency = get_value(line, titles, "supplier'scurrency").ToUpper();
              
                var g = MyApp.DbContext.PurchaseGroups.FirstOrDefault(
                            (g) => g.GroupCode == get_value(line, titles, "groupid").ToUpper());
                prod_sup.GroupId = g != null ? g.Id : null;

                // 購入価格は外貨建て
                var pur_price = get_value(line, titles, "purchaseprice/unit");
                prod_sup.UnitPurchasePrice = pur_price != "" ? double.Parse(pur_price) : 0.00; 

                t = get_value(line, titles,"terminated");
                prod_sup.Terminated = t != "" ? DateOnly.Parse(t) : null;
            }
        }
        
        MyApp.DbContext.SaveChanges() ;
    }


    // 4. [Bill Of Materials] タブ
    void importBoMSheet(IXLWorksheet sheet)
    {
        var titles = make_title_indexes(sheet.Row(1));

        for (int i = START_LINE; i < sheet.Rows().Count(); ++i) {
            IXLRow cur_row = sheet.Row(i+1);
            var line = (string x) => get_value(cur_row, titles, x);

            Product pk1 = MyApp.DbContext.Products.Single(
                                (c) => c.ItemCode == line("finishedgood'scode").ToUpper());
            Product pk2 = MyApp.DbContext.Products.Single(
                                (c) => c.ItemCode == line("material'scode").ToUpper());
            var ps = MyApp.DbContext.ProductStructures.FirstOrDefault(
                        (x) => x.ParentId == pk1.Id && x.ChildId == pk2.Id);
            if (ps == null) { 
                ps = new ProductStructure() { ParentId = pk1.Id, ChildId = pk2.Id };
                MyApp.DbContext.ProductStructures.Add(ps);
            }
            else
                MyApp.DbContext.ProductStructures.Update(ps);

            var s = line("bomtype").ToUpper();
            ps.BoMType = s == "ASM" ? EBoMType.ASM :
                         (s == "KIT" ? EBoMType.KIT :
                         (s == "SUBCONTRACT" ? EBoMType.SUBCONTRACT : 
                         throw new ArgumentNullException()));

            ps.Qty = int.Parse(line("materialqty/batch")); 
            ps.BatchRounding = int.Parse(line("batchrounding"));
            ps.ManufacturingLeadTime = int.Parse(line("manufacturingleadtime"));
            ps.MinBatchSize = int.Parse(line("minbatchsize"));

            MyApp.DbContext.SaveChanges(); // 一つずつ保存が必要
        }
    }


    public override void importMasterDataFile()
    {
        var sheets = new Dictionary<string, IXLWorksheet>();
        foreach (var s in _workbook.Worksheets) 
            sheets.Add(s.Name.ToLower().Replace(" ", ""), s);

        IXLWorksheet sheet;

        if (sheets.TryGetValue("iteminfo", out sheet))
            importItemInfoSheet(sheet);  
        if (sheets.TryGetValue("billofmaterials", out sheet))
            importBoMSheet(sheet);

        MyApp.DbContext.SaveChanges();
    }
}


class ImportItemCategories : ImportExcelBase
{
    const int START_LINE = 3; // 0 始まり

    public ImportItemCategories() { }

    // 5. [Item Category] タブ
    void importItemCategorySheet(IXLWorksheet sheet)
    {
        // index は 1始まり
        var title = sheet.Row(1);

        for (int i = START_LINE; i < sheet.Rows().Count(); ++i) {
            var ic = new ItemCategory();
            var line = sheet.Row(i+1);

            for (int j = 0; j < sheet.Columns().Count(); ++j) {
                string value = line.Cell(j+1).GetFormattedString().Trim();
                switch (title.Cell(j+1).GetFormattedString().ToLower().Replace(" ", ""))
                {
                case "itemcategorycode": 
                    var x = MyApp.DbContext.ItemCategories.FirstOrDefault(
                                        (c) => c.CategoryCode == value.ToUpper());
                    if (x != null) { 
                        ic = x;
                        MyApp.DbContext.ItemCategories.Update(ic);
                    }
                    else { 
                        ic.CategoryCode = value.ToUpper(); 
                        MyApp.DbContext.ItemCategories.Add(ic);
                    }
                    break;
                case "itemcategoryname":
                    ic.Name = value; break;
                case "parentcode": // トップレベル = NULL
                    if (value != "") { 
                        var p = MyApp.DbContext.ItemCategories.Single(
                                    (x) => x.CategoryCode == value.ToUpper());
                        ic.ParentId = p.Id;
                    }
                    else
                        ic.ParentId = null;
                    break;
                case "level":
                    ic.Level = int.Parse(value);    break;
                //case "isleaf":
                //    break;
                default:
                    break;
                }
            }
            // 2階層のみ
            if ( ic.ParentId == null && ic.Level != 0 ||
                 ic.ParentId != null && ic.Level != 1 ) {
                throw new ArgumentException("item-cat level");                
            }

            MyApp.DbContext.SaveChanges() ;  // 一つずつ保存が必要。
        }
    }

    public override void importMasterDataFile()
    {
        var sheets = new Dictionary<string, IXLWorksheet>();
        foreach (var s in _workbook.Worksheets) 
            sheets.Add(s.Name.ToLower().Replace(" ", ""), s);

        IXLWorksheet sheet;

        if (sheets.TryGetValue("itemcategory", out sheet))
            importItemCategorySheet(sheet);

        MyApp.DbContext.SaveChanges();
    }
}

}
