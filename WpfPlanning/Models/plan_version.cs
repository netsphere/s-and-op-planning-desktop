﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


// See
//  - https://garywoodfine.com/data-entity-validation-efcore/
//    How to do Entity Validation with EF Core
//  - https://www.bricelam.net/2016/12/13/validation-in-efcore.html
//    Validation in EF Core

namespace WpfPlanning.Models
{

// 計画バージョン
// 実績も、計画バージョンの一つとする. `StartDate`, `EndDate` が NULL かで区別
[Table("plan_versions")]
public class PlanVersion : MasterBase, IValidatableObject
{
    // EFCore: `protected set` を取ると、自動マッピングされない
    [Key, Column("id")]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int Id { get; set; }

    [Required]
    public string Name { get; set; }

    // Open なのは<s>一つだけ</s>. -> 複数可にする。
    [Required]
    public bool IsOpen { get; set; }

    // 計画を作成した日。実績バージョンでは NULL.
    public DateOnly? ForecastDate { get; set; }

    // 計画期間の開始日。実績バージョンは NULL
    //[Required]
    public DateOnly? StartDate { get; set; }

    public DateOnly? EndDate { get; set; }

    [Required]
    public bool IsActual { get; set; }
    
    // @override
    public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
    {
        if (IsActual != true) {
            if (ForecastDate == null)
                yield return new ValidationResult("Need ForecastDate", new[]{nameof(ForecastDate)} );
            if (StartDate == null)
                yield return new ValidationResult("Need StartDate", new[]{nameof(StartDate)});
            if (EndDate == null)
                yield return new ValidationResult("Need EndDate", new[]{nameof(EndDate)});
        }
    }
}

}
