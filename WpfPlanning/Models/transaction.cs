﻿
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace WpfPlanning.Models
{

// トランザクションは、勘定科目, 日付, 値 (数値) の形で共通。
// 勘定科目を汎用にすると適用範囲が広がりそうだが、式を受け付けないといけなくなる
//   -> 科目は決め打ちにする。

public abstract class TranBase
{
    // 実績は "実績バージョン" を使う
    [Required]
    public int PlanVersionId { get; set; }  // 外部キー

    [ForeignKey(nameof(PlanVersionId))]
    public virtual PlanVersion PlanVersion { get; set; } // reference navigation

    // 記帳日
    //[Required]
    //public DateOnly Date { get; set; }
}


// EFCoreで複数の列に展開できるか?
// -> https://ichiroku11.hatenablog.jp/entry/2017/11/18/225345
//    EFCore owned types 
[Owned]
public class ItemAndLocation : IEquatable<ItemAndLocation>
{
    [Required]
    public int ProductId { get; set; }

    // Navigation property
    [ForeignKey(nameof(ProductId))]
    public virtual Product Item { get; set; }

    // 最終の納入先
    [Required]
    public int LocationId { get; set; }

    [ForeignKey(nameof(LocationId))]
    public virtual Location Location { get; set; }


    public ItemAndLocation() { }

    // コピーする場合
    public ItemAndLocation(Product item, Location location)
    {
        if (item == null || location == null)
            throw new ArgumentNullException(nameof(item));
        ProductId = item.Id;
        Item = item;
        LocationId = location.Id;
        Location = location;
    }


    // @override IEquatable<ItemAndLocation>
    public bool Equals(ItemAndLocation? other)
    {
        if (ReferenceEquals(other, null)) 
            return false;
        return ProductId == other.ProductId && LocationId == other.LocationId;
    }

    // 非ジェネリック版も実装必須
    public override bool Equals(object? obj)
    {
        if (obj is ItemAndLocation) 
            throw new ArgumentException("internal error");

        return Equals(obj as ItemAndLocation);
    }

    // これも実装必須
    public override int GetHashCode()
    {
        return HashCode.Combine(ProductId, LocationId);
    }
}


// For `Distinct()`
class DemandCompareByILoc : IEqualityComparer<DemandTran> { 
    static DemandCompareByILoc? _instance = null;
    public static DemandCompareByILoc instance() {
        if (_instance == null) 
            _instance = new DemandCompareByILoc();
        return _instance;
    }

    // @override
    public bool Equals(DemandTran? x, DemandTran? y) {
        if (ReferenceEquals(x, null))
            return ReferenceEquals(y, null) ? true : false;

        return x.ProductId == y.ProductId && 
               x.SalesChannel.LocationId == y.SalesChannel.LocationId;
    }

    // @override
    public int GetHashCode([DisallowNull] DemandTran obj) {
        return HashCode.Combine(obj.ProductId, obj.SalesChannel.LocationId);
    }
}


// 需要データ
[Table("demand_tran")]
//[Keyless]
public class DemandTran : TranBase
{
    [Key, Column("id")]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int Id { get; protected set; }

    // 販売日 (週の月曜日)
    [Required]
    public DateOnly SaleDate { get; set; }

    [NotMapped]
    public DateOnly dt_month { get; set; }

    // 商品カテゴリは非正規化で保存
    //[NotMapped]
    public int CatL1 { get; set; }

    //[NotMapped]
    public int? CatL2 { get; set; }

    [Required] 
    public int ProductId { get; set; }

    [ForeignKey(nameof(ProductId))]
    public virtual Product Product { get; set; }

    [NotMapped]
    public int ChannelL1 { get; set; }

    [NotMapped]
    public int? ChannelL2 { get; set; }

    [Required] 
    public int SalesChannelId { get; set; }

    // こちらは正規化
    [ForeignKey(nameof(SalesChannelId))]
    //[DeleteBehavior(DeleteBehavior.Cascade)]  これが逆に無効化されてしまう
    public virtual SalesChannel SalesChannel { get; set; }

    // 直接需要がある item のみ, 値を発生.
    // 編集で比例配分があるため、ここだけ `double`.
    [Required]
    public double QtySold { get; set; }

    // Kitting (ファントム組立品) の場合だけ, 上のレイヤーから配分する. 上から下へ一方向
    // 自分自身 (の下へ配分する) の値は含めない
    [Required]
    public double IntrmQtySold { get; set; }

    [Required]
    public double SalesAmount { get; set; }

    // Kitting の場合だけ, 上のレイヤーから配分する。
    [Required]
    public double IntrmSalesAmount { get; set; }

    // クレジットカード手数料とか
    [Required]
    public double SalesCostAmount { get; set; }

    [Required]
    public double IntrmSalesCostAmount { get; set; }

    // 受注残
    [Required]
    public int UnfilledOrders { get; set; }
    
    public DemandTran() { 
        //Account = "DEMAND";
    }
}


// 出荷実績と手元在庫. 需要があっても在庫がなければ 0 になる.
[Table("shipment_tran")]
public class ShipmentTran : TranBase
{
    [Key, Column("id")]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int Id { get; protected set; }

    // 自社→顧客への出荷日
    [Required]
    public DateOnly ShipmentDate { get; set; }

    [NotMapped]
    public DateOnly dt_month { get; set; }

    // 商品カテゴリは非正規化で保存
    //[NotMapped]
    public int CatL1 { get; set; }

    //[NotMapped]
    public int? CatL2 { get; set; }

    // Kitting がある。出荷する最小単位
    [Required] 
    public int ProductId { get; set; }

    [ForeignKey(nameof(ProductId))]
    public virtual Product Product { get; set; }

    // <s>Kitting: 上のレイヤーでは nil になる</s> 組み立て地がある。
    [Required]
    public int LocationId { get; set; }
    
    [ForeignKey(nameof(LocationId))]
    public virtual Location Location { get; set; }

    // 末端のレイヤーだけ, ではない.
    // PN がそれぞれ 1 出荷で、kitted SKU として 1の出荷になることがある。
    //   -> 下からの足し上げではない.
    [Required]
    public int QtyShipped { get; set; }
    
    // 内部消費
    [Required]
    public int QtyConsumed { get; set; }

    [Required]
    public double RevenueAmount { get; set; }

    // 上から下への一方通行.
    //[Required]
    //public double CalcRevenueAmount { get; set; }

    // 配送費など.
    [Required]
    public double DeliveryCostAmount { get; set; }

    // 上のレイヤーから降りてくる値
    //[Required]
    //public double CalcDeliveryCostAmount { get; set; }

    [Required]
    public double TranProfit { get; set; }

    //[Required]
    //public double CalcTranProfit { get; set; }

    // 在庫: 末端レイヤーのみ。アッシーは在庫あり
    // <s>kit 組の場合は、下から min() を取る.</s> 0 でよい。MRP で kit-orderを発生させる
    [Required]
    public int OnHand { get; set; }
}


/*
// 1日分の供給 (入荷) データ: IsPurchasable = true の商品, アッシー.
[Table("supply_tran")]
public class SupplyTran : TranBase
{
    [Key, Column("id")]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int Id { get; protected set; }

    [Required] 
    public int ProductId { get; set; }

    [ForeignKey(nameof(ProductId))]
    public virtual Product Item { get; set; }

    [Required]
    public int LocationId { get; set; }
    
    [ForeignKey(nameof(LocationId))]
    public virtual Location Location { get; set; }

    [Required]
    public double QtyReceived { get; set; }

    // 仕入代金 - 購入の対価
    [Required]
    public double GoodsCost { get; set; }

    // 仕入諸掛り
    [Required]
    public double FreightCost { get; set; }
}
*/


}
