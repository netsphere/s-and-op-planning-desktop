﻿
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;


namespace WpfPlanning.Models
{

// 販売組織
[Index(nameof(ChannelCode), IsUnique = true)]
[Index(nameof(Description), IsUnique = true)]  // 名前のほうにも UNIQUE を付けておく.
public class SalesChannel : MasterBase
{
    [Key, Column("id")]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int Id { get; protected set; }
    
    // 大文字にする.
    [Required]
    public string ChannelCode { get; set; }

    //[Required]
    //public string Name { get; set; }

    // これを名前にする
    //[Required(AllowEmptyStrings = true)] // 単に `Required` では "" もエラー
    [Required]
    public string Description { get; set; }
    
    public int? ParentId { get; set; }

    [ForeignKey(nameof(ParentId))]
    [DeleteBehavior(DeleteBehavior.NoAction)]
    public virtual SalesChannel? Parent { get; set; }

    // 階層の深さ. トップレベル = 0
    [Required]
    public int Level { get; set; }

    //[Required]
    //public bool IsLeaf { get; set; }

    // 需要予測と供給を繋ぐ
    // NULL は、グルーピング用。
    public int? LocationId { get; set; }

    [ForeignKey(nameof(LocationId))]
    [DeleteBehavior(DeleteBehavior.NoAction)]
    public virtual Location? Location { get; set; }
}


// 供給サイド
[Index(nameof(LocationCode), IsUnique = true)]
[Index(nameof(Description), IsUnique = true)]  // 名前のほうにも UNIQUE を付けておく.
public class Location : MasterBase
{
    [Key, Column("id")]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int Id { get; protected set; }

    [Required]
    public string LocationCode { get; set; }

    // これを名前にする
    [Required]
    public string Description { get; set; }

    //[Required(AllowEmptyStrings = true)] // 単に `Required` では "" もエラー
    //public string Description { get; set; }

    public int? ParentId { get; set; }

    [ForeignKey(nameof(ParentId))]
    [DeleteBehavior(DeleteBehavior.NoAction)]
    public virtual Location? Parent { get; set; }

    // 階層の深さ. トップレベル = 0
    [Required]
    public int Level { get; set; }
}

}
