
# インポートできる形式で, ダミーデータを3年分作る

require "date"

items = [
  ["50046",       123],
  ["1866-MB-CW",   12.67],
  ["56213",       400],
  ["1866-MB-OB",  659.22],
  ["1866-MB-RO",   65.876],
  ["1866-MB-TWG",  54.9865],
  ["1866-MB-WB",  432.98],
  ["MB-50046",   3211.1],
  ["56213-P",    3800],
  ["kids-bmx-01-blue", 12345],
  ]
  
channels = [
  "amazon-jp", 
  "store-hoge", 
  "OROSHI-1", 
  ]
  

print "Date,Quantity sold,Transaction revenue,Sales price/unit,Sales Cost,Transaction profit,Item code,Channel,Unfilled orders,Outlier\n"
print "\n"
print "\n"

items.each do |item|
  item_code = item[0]; base_price = item[1]
  channels.each do |channel|
    y = 1000.0
    d = Date.new(2019, 12, 30) # 月曜日 
    while d < Date.new(2024, 1, 1)
      y = [y + y * (rand() - 0.48), 0].max    # / 2.0, 0].max
      amt = (base_price + base_price * (rand() - 0.5) / 2.5) * y
      cost = amt * 0.02
      print "#{d},#{y},#{amt},,#{cost},,#{item_code},#{channel},0,\n"
      d = d + 7
      y = 100.0 if y <= 0      
    end
  end
end


