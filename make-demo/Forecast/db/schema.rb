# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.1].define(version: 2023_12_31_024816) do
  create_table "order_items", force: :cascade do |t|
    t.string "order_id", null: false
    t.integer "order_item_id", null: false
    t.string "product_id", null: false
    t.string "seller_id", null: false
    t.datetime "shipping_limit_date", null: false
    t.decimal "price", precision: 12, scale: 2, null: false
    t.decimal "freight_value", precision: 12, scale: 2, null: false
    t.index ["order_id", "order_item_id"], name: "index_order_items_on_order_id_and_order_item_id", unique: true
  end

  create_table "orders", id: false, force: :cascade do |t|
    t.string "order_id", null: false
    t.string "customer_id", null: false
    t.string "order_status", null: false
    t.datetime "order_purchase_timestamp", null: false
    t.datetime "order_approved_at", null: false
    t.datetime "order_delivered_carrier_date"
    t.datetime "order_delivered_customer_date"
    t.date "order_estimated_delivery_date", null: false
    t.index ["order_id"], name: "index_orders_on_order_id", unique: true
  end

  create_table "product_categories", force: :cascade do |t|
    t.string "pt", null: false
    t.string "en", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["pt"], name: "index_product_categories_on_pt", unique: true
  end

  create_table "products", id: false, force: :cascade do |t|
    t.string "product_id", null: false
    t.string "product_category_name", null: false
    t.integer "product_name_lenght", null: false
    t.integer "product_description_lenght", null: false
    t.integer "product_photos_qty", null: false
    t.integer "product_weight_g", null: false
    t.integer "product_length_cm", null: false
    t.integer "product_height_cm", null: false
    t.integer "product_width_cm", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["product_id"], name: "index_products_on_product_id", unique: true
  end

  create_table "sellers", id: false, force: :cascade do |t|
    t.string "seller_id", null: false
    t.string "seller_zip_code_prefix", null: false
    t.string "seller_city", null: false
    t.string "seller_state", limit: 2, null: false
    t.index ["seller_id"], name: "index_sellers_on_seller_id", unique: true
  end

  add_foreign_key "order_items", "orders", primary_key: "order_id"
  add_foreign_key "order_items", "products", primary_key: "product_id"
  add_foreign_key "order_items", "sellers", primary_key: "seller_id"
  add_foreign_key "products", "product_categories", column: "product_category_name", primary_key: "pt"
end
