
class Initial < ActiveRecord::Migration[7.1]
  def change
    # マスタ ######################################################3
    
    create_table :product_categories do |t|
      t.string :pt, null:false, index:{unique:true} # Portuguese
      t.string :en, null:false
      
      t.timestamps
    end
    
    create_table :products, id:false do |t|
      # 主キー
      t.string "product_id",            null:false, index:{unique:true}
      
      t.string "product_category_name", null:false
      t.integer "product_name_lenght",  null:false    # 実際の商品名は秘密
      t.integer "product_description_lenght", null:false
      t.integer "product_photos_qty",   null:false
      t.integer "product_weight_g",     null:false
      t.integer "product_length_cm",    null:false
      t.integer "product_height_cm",    null:false
      t.integer "product_width_cm",     null:false

      t.timestamps

      # 制約
      # add_reference() は列を追加する。制約を付けるだけでよい
      t.foreign_key :product_categories ,
                    column:"product_category_name", primary_key:"pt"
    end # create_table

    create_table :sellers, id:false do |t|
      # 主キー
      t.string "seller_id",    null:false, index:{unique:true}
      
      t.string "seller_zip_code_prefix", null:false
      t.string "seller_city",  null:false
      t.string "seller_state", null:false, limit:2  # 州
    end
    
    # トランザクション ################################################
    
    create_table :orders, id:false do |t|
      # 主キー
      t.string "order_id",     null:false, index:{unique:true}
      
      t.string "customer_id",  null:false
      # "delivered", "canceled" はキャンセル済み (別行にはならない)
      t.string "order_status", null:false    
      t.datetime "order_purchase_timestamp", null:false
      t.datetime "order_approved_at",        null:false
      t.datetime "order_delivered_carrier_date"  #null:false "canceled" だと空
      t.datetime "order_delivered_customer_date" #null:false "canceled" だと空
      t.date "order_estimated_delivery_date", null:false
    end

    create_table :order_items do |t|
      t.string  "order_id",       null:false
      t.integer "order_item_id",  null:false
      # 複数個の場合, 同じ product で行が分かれる. price も繰り返し.
      t.string  "product_id",     null:false
      t.string  "seller_id",      null:false
      t.datetime "shipping_limit_date", null:false
      # precision は全体の桁数
      t.decimal  "price",         null:false, precision: 12, scale: 2
      t.decimal  "freight_value", null:false, precision: 12, scale: 2

      # 制約
      t.foreign_key :orders, column:"order_id", primary_key:"order_id"
      t.index ["order_id", "order_item_id"], unique:true
      t.foreign_key :products, column:"product_id", primary_key:"product_id"
      t.foreign_key :sellers,  column:"seller_id", primary_key:"seller_id"
    end

  end
end
