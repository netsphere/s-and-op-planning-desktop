
class ProductCategory < ApplicationRecord
  has_many :products, primary_key: "pt", foreign_key:"product_category_name" 
end
