
class Seller < ApplicationRecord
  # 主キー
  self.primary_key = :seller_id

  has_many :order_items
end
