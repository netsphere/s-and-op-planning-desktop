
class OrderItem < ApplicationRecord
  # 相手方の主キーが "id" ではない. マスタ側で primary_key を設定.
  belongs_to :order #, primary_key:"order_id"
  belongs_to :product #, primary_key:"product_id"
  belongs_to :seller #, primary_key:"seller_id"
end
