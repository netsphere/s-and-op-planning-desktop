
class Order < ApplicationRecord
  # 主キー
  self.primary_key = "order_id"
  
  has_many :order_items
end
