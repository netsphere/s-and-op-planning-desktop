
class Product < ApplicationRecord
  # 主キー
  self.primary_key = :product_id
  
  belongs_to :product_category, foreign_key:"product_category_name",
             primary_key:"pt"
  has_many :order_items
end
