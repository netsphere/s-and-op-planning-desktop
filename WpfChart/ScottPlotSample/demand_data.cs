﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScottPlotSample
{

// CSV データ
class TrainingData
{
    // CsvHelper: `[Index(0)]` のように attribute (アノテーション) で指定してもよい。
    //public int Id { get; set; }

    public DateOnly Date { get; set; }

    public string Product_Code { get; set; }

    public string Channel { get; set; }
    //public string Warehouse	{ get; set; }
    //public string Product_Category { get; set; }

    public double Order_Demand { get; set; }
    //public string month_year { get; set; }
    //public int year { get; set; }
}


class PredictedResult
{
    public string Warehouse { get; set; }
    public string Product_Category { get; set; }
    public string week { get; set; }
    public double Order_Demand { get; set; }
}


}
