
using Microsoft.Win32;
using NReco.PivotData;
using ScottPlot;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Windows;
using System.Windows.Input;


/*
参考 https://forecastegy.com/posts/hierarchical-time-series-forecasting-python/
    Hierarchical Time Series Forecasting with Python

次のように列名で木を作る
spec = [['Website'],
        ['Website', 'Device Category'],
        ['Website', 'Device Category', 'Browser']]
 */

namespace ScottPlotSample
{

internal class MainWindowViewModel
{
    public static readonly DateTime startDate = new DateTime(2015, 12, 28); // 月曜
    public static readonly DateTime endDate = new DateTime(2022, 1, 31); // 含まない

    // category => 値の配列
    //public Dictionary<string, double[]> Category_OrderDemand;

    public static readonly DateTime predictStartDate = new DateTime(2020, 1,13);
    public static readonly DateTime predictEndDate = predictStartDate.AddDays(7 * 53);

    public Dictionary<string, double[]> PredictedDemands;

    PivotData _salesPvtData;
    public PivotData SalesPvtData {
        get { return _salesPvtData; }
        set { _salesPvtData = value; }
    }

    ObservableCollection<string> _products;
    ObservableCollection<string> _channels;


    // コンストラクタ
    public MainWindowViewModel()
    {
        // Category_OrderDemand = new Dictionary<string, double[]>();
        _products = new ObservableCollection<string>();
        _channels = new ObservableCollection<string>();
    }


    // 訓練データを読み込み、表示する
    void read_file(string filename)
    {
        List<TrainingData> trainingData = new List<TrainingData>();

        // 見つからない場合: FileNotFoundException 例外
        using (var sr = new StreamReader(filename)) {
            //Category_OrderDemand = new Dictionary<string, double[]>();

            // `CsvReader(StreamReader)` コンストラクタは廃止. カルチャが必要
            using (var csv = new CsvHelper.CsvReader(sr, CultureInfo.InvariantCulture)) {
                // 1行ずつ読む. Read() とヘッダまたは本体
                csv.Read(); csv.ReadHeader();
                while (csv.Read() ) {
                    TrainingData row = new TrainingData() {
                            Date = csv.GetField<DateOnly>("ds"),
                            Product_Code = csv.GetField("product"),
                            Channel = csv.GetField("channel"),
                            Order_Demand = csv.GetField<double>("y") };
                    trainingData.Add(row);

                    if (!_products.Contains(row.Product_Code))
                        _products.Add(row.Product_Code);
                    if (!_channels.Contains(row.Channel))
                        _channels.Add(row.Channel);
                }
            }
        }

        // build pivot
        _salesPvtData = new PivotData(
                new [] { "product", "channel", "ds"},
                new CompositeAggregatorFactory(
                        new SumAggregatorFactory("y"))
                );
        _salesPvtData.ProcessData(trainingData, getValue);
    }

    static object getValue(object arg1, string dimName) {
        var row = (TrainingData) arg1;
        switch (dimName) {
        case "product": return row.Product_Code;
        case "channel": return row.Channel;
        case "ds":      return row.Date;
        case "y":       return row.Order_Demand;
        default:
            throw new ArgumentException();
        }
    }
/*
    public void read_file2(string filename)
    {
        using (var sr = new StreamReader(filename)) {
            PredictedDemands = new Dictionary<string, double[]>();

            using (var csv = new CsvHelper.CsvReader(sr, CultureInfo.InvariantCulture)) {
                csv.Context.RegisterClassMap<PredictedMapper>();

                csv.Read(); csv.ReadHeader();
                while (csv.Read()) {
                    PredictedRow row = csv.GetRecord<PredictedRow>();

                    string cat = row.Product_Category.Trim();
                    double[] demands;
                    if (PredictedDemands.ContainsKey(cat))
                        demands = PredictedDemands[cat];
                    else {
                        demands = new double[(predictEndDate - predictStartDate).Days / 7 + 1];
                        PredictedDemands.Add(cat, demands);
                    }

                    int idx = (DateTime.Parse(row.week.Split("/")[0]) - predictStartDate).Days / 7;
                    demands[idx] += row.Order_Demand;
                }
            }
        }  
    }
*/
   

    /// //////////////////////////////////////////////////////////////////
    // Command Handlers

    // Excel なら 1M 行の CSV ファイルも楽勝。
    public void OnFileOpen(object sender, ExecutedRoutedEventArgs e)
    {
        var ofd = new OpenFileDialog() {Filter = "CSVファイル(*.csv)|*.csv"};
        if (ofd.ShowDialog() == true ) {
            read_file(ofd.FileName);
        }
    }

    // ファイル2
    public void OnOpenPredictedFile(object sender, RoutedEventArgs e)
    {
        var ofd = new OpenFileDialog() {Filter = "CSVファイル(*.csv)|*.csv"};
        if (ofd.ShowDialog() == true ) {
            //read_file2(ofd.FileName);
        }
    }

}


// See https://scottplot.net/cookbook/5.0/Signal/SignalRenderIndexes/
//     Partial Signal Rendering

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
public partial class MainWindow : Window
{
    public MainWindow()
    {
        InitializeComponent();

        var vm = (MainWindowViewModel) DataContext;
        CommandBindings.Add(new CommandBinding(ApplicationCommands.Open,
                                               vm.OnFileOpen));
        CommandBindings.Add(new CommandBinding(MyCommands.OpenPredictedFile,
                                               vm.OnOpenPredictedFile));
        CommandBindings.Add(new CommandBinding(MyCommands.DoPlot,
                                               OnDoPlot));

        //plot();
        //plot2();
    }


    /// //////////////////////////////////////////////////////////////////
    // Command Handlers

    // Plot!
    void OnDoPlot(object sender, RoutedEventArgs e)
    {
        ScottPlot.Plot myPlot = _wpfPlot1.Plot;
        var vm = (MainWindowViewModel) DataContext;

        // まずスライスする
        var selectQuery = new SliceQuery(vm.SalesPvtData)
                                    .Dimension("ds");
        PivotData slicedPvtData = selectQuery.Execute();
        var list = slicedPvtData.OrderBy(x => x.Key[0]).ToList();

        // X軸をデータの個数と同じにする
        DateTime start = new DateTime(2020, 1, 1, 0, 0, 0);
        double[] dates = Enumerable.Range(0, 48)
                             .Select((x) => start.AddMonths(x).ToOADate())
                             .ToArray();

        // 日付に合わせて埋めていく
        double[] ys = new double[48];
        for (int i = 0; i < list.Count; ++i) {
            var kv = list[i];
            var date = (DateOnly) kv.Key[0];
            ys[(date.Year - start.Year) * 12 + (date.Month - start.Month)] =
                    Decimal.ToDouble((decimal) kv.Value.AsComposite().Aggregators[0].Value);
        }
/*
        var sig = myPlot.Add.Signal(ys);
        sig.Data.XOffset = start.ToOADate();
        // これだとズレていく  => そんなに目立たないのでこれでも差し支えなさそう
        sig.Data.Period = 365.25 / 12.0;
*/
        myPlot.Add.Scatter(dates, ys);
        myPlot.Axes.DateTimeTicksBottom();
        myPlot.ShowLegend(Alignment.UpperRight);
/*
            var sig = myPlot.Add.Signal(pair.Value);
            sig.Label = pair.Key;
            sig.Data.Period = 7.0; // one week between each point
            sig.Data.XOffset = MainWindowViewModel.startDate.ToOADate();

            var predicted = vm.PredictedDemands[pair.Key];
            var sigp = myPlot.Add.Signal(predicted);
            sigp.Label = pair.Key;
            sigp.Data.Period = 7.0;
            sigp.Data.XOffset = MainWindowViewModel.predictStartDate.ToOADate();

            //break;
        }
        myPlot.Axes.DateTimeTicksBottom();
        AxisLimits limits = myPlot.Axes.GetLimits();
        myPlot.Axes.SetLimits(limits.Left, limits.Right, 0, 30_000_000);
        myPlot.ShowLegend(Alignment.UpperRight);
        */
    }


    /// ////////////////////////////////////////////////////////////////// 

    void plot()
    {
        // データの個数とX軸の数を合わせる
        int pointCount = 1_000_000; // Scatter() で 1M は重い
        double[] data = Generate.RandomWalk(pointCount);
        double[] data2 = Generate.RandomWalk(pointCount);

        // X軸. OLE オートメーション日付 (double) にする
        DateTime firstTime = new DateTime(2016, 6, 27, 15, 0, 0);
        // Range() の第2引数は個数.
        double[] dates = Enumerable.Range(0, pointCount)
                              .Select((x) => firstTime.AddMinutes(x).ToOADate())
                              .ToArray();

        // これは遅い!!
        //var scatter = _wpfPlot1.Plot.Add.Scatter(dates, data);
        
        // これは十分速い
        _wpfPlot1.Plot.Add.Signal(data2);

        //_wpfPlot1.Plot.Add.Y
        _wpfPlot1.Plot.Axes.DateTimeTicksBottom();
        _wpfPlot1.Plot.Title("Signal plot with one million points");
    }

    void plot2()
    {
ScottPlot.Plot myPlot = _wpfPlot1.Plot;

DateTime start = new(2024, 1, 1);
double[] values = Generate.RandomWalk(1000);

var sigAll = myPlot.Add.Signal(values);
sigAll.Label = "Full";
sigAll.Data.YOffset = 80;
            //sigAll.Data.XOffset = start.ToOADate();
sigAll.Data.Period = 1.0; // one day between each point

var sigLeft = myPlot.Add.Signal(values);
sigLeft.Label = "Left";
sigLeft.Data.YOffset = 60;
sigLeft.Data.MaximumIndex = 700;

var sigRight = myPlot.Add.Signal(values);
sigRight.Label = "Right";
sigRight.Data.YOffset = 40;
sigRight.Data.MinimumIndex = 300;

var sigMid = myPlot.Add.Signal(values);
sigMid.Label = "Mid";
sigMid.Data.YOffset = 20;
sigMid.Data.MinimumIndex = 300;
sigMid.Data.MaximumIndex = 700;

myPlot.ShowLegend(Alignment.UpperRight);
//myPlot.Axes.Margins(top: .5);
myPlot.Axes.DateTimeTicksBottom();
    }

}

}
