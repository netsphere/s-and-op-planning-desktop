﻿using System.Configuration;
using System.Data;
using System.Windows;
using System.Windows.Input;

namespace ScottPlotSample
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
public partial class MyApp : Application
{
    void Application_Startup(object sender, StartupEventArgs e)
    {
        var wnd = new MainWindow();
        wnd.Show();
    }
}

static class MyCommands
{
    public static readonly RoutedUICommand OpenPredictedFile =
            new RoutedUICommand("Read the predicted file", nameof(OpenPredictedFile),
                                typeof(MyCommands));

    public static readonly RoutedUICommand DoPlot = 
            new RoutedUICommand("Let's plot!", nameof(DoPlot), typeof(MyCommands));
}

}
