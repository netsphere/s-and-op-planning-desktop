﻿using LiveChartsCore.SkiaSharpView;
using LiveChartsCore;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfChart
{
internal class MainWindowViewModel
{

    public ISeries[] Series { get; protected set; }
    
    public MainWindowViewModel()
    {
        this.Series = new ISeries[] {
                new LineSeries<double> {
                    Values = new double[] { 2, 1, 3, 5, 3, 4, 6 },
                    Fill = null
                }
        };
    }
}


    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
public partial class MainWindow : Window
{
    public MainWindow()
    {
        InitializeComponent();
    }
}

}