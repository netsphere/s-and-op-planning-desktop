﻿using System.Configuration;
using System.Data;
using System.Windows;

namespace WpfChart
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
public partial class MyApp : Application
{
    private void Application_Startup(object sender, StartupEventArgs e)
    {
        var wnd = new MainWindow();
        wnd.Show();
    }
}

}
