﻿# -*- coding:utf-8-with-signature -*-

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import sktime

# index_col: 読み込み時に index 列を指定.
multindex = pd.read_csv('../repos/retail-demand-forecaster/data/sales.csv',
                        index_col=0)
print(type(multindex))  #=> <class 'pandas.core.frame.DataFrame'>
print(multindex.head())
"""
   Product_Code Warehouse Product_Category        Date  Order_Demand month_year  year
0  Product_0993    Whse_J     Category_028  2017-07-27         100.0    2017-07  2017
1  Product_0979    Whse_J     Category_028  2017-01-19         500.0    2017-01  2017
2  Product_0979    Whse_J     Category_028  2017-02-03         500.0    2017-02  2017
3  Product_0979    Whse_J     Category_028  2017-02-09         500.0    2017-02  2017
4  Product_0979    Whse_J     Category_028  2017-03-02         500.0    2017-03  2017
"""
print(multindex.shape)   #=> (1031437, 7)

# 軸を減らすことができる。
hier_data = multindex.groupby(['Warehouse','Product_Category','Date'],
                              as_index=False)['Order_Demand'].sum()
print(hier_data.head())
"""
  Warehouse Product_Category        Date  Order_Demand
0    Whse_A     Category_001  2018-06-06          50.0
1    Whse_A     Category_001  2018-06-14           1.0
2    Whse_A     Category_001  2018-07-02           2.0
3    Whse_A     Category_001  2018-07-05           5.0
4    Whse_A     Category_001  2018-07-15          30.0
"""
print(hier_data.shape)  #=> (69930, 4)

# これで日付型に整える (必須)
# format='%Y-%m' を付けてもよい
hier_data['Date'] = pd.to_datetime(hier_data['Date'])
# こちらは期間 (period), いったん日付に. => 月曜日になる
hier_data["week"] = hier_data["Date"].dt.to_period("W").dt.to_timestamp()

# fig: 描画領域全体
# axes: 一つのグラフの領域
# fig.add_subplot() で領域を一つ追加
"""
fig, axes = plt.subplots(figsize=(20,10))
axes.plot(hier_data.groupby(['month_year'])['Order_Demand'].sum())
locator = mdates.AutoDateLocator()
formatter = mdates.ConciseDateFormatter(locator)
axes.xaxis.set_major_locator(locator)
axes.xaxis.set_major_formatter(formatter)
"""

# カテゴリごとにグラフ化
fig, ax2 = plt.subplots(figsize=(20,10))
#ax2 = fig.add_subplot() #figsize=(20,10))
# sum() は Series 型を返す. unstack() が必要.
bycat = hier_data.groupby(['week','Product_Category'])['Order_Demand'].sum().unstack()
labels = bycat.columns
ax2.plot(bycat)
locator = mdates.AutoDateLocator()
formatter = mdates.ConciseDateFormatter(locator)
ax2.xaxis.set_major_locator(locator)
ax2.xaxis.set_major_formatter(formatter)
ax2.legend(labels)

"""
fig, ax3 = plt.subplots(figsize=(20,10))
by_wh = hier_data.groupby(['month_year','Warehouse'])['Order_Demand'].sum().unstack()
labels = by_wh.columns
ax3.plot(by_wh)
locator = mdates.AutoDateLocator()
formatter = mdates.ConciseDateFormatter(locator)
ax3.xaxis.set_major_locator(locator)
ax3.xaxis.set_major_formatter(formatter)
ax3.legend(labels)
"""

#pd.DataFrame(multindex).T.plot()
#time_series = hier_data['Warehouse'].plot()

#plt.plot(hier_data.month_year, hier_data['Warehouse'])

#########################################################################

# Creating warehouse, and category pairs.
category_combination = multindex[['Warehouse', 'Product_Category']].drop_duplicates()
# 時間軸 = 年月
time = pd.DataFrame(hier_data.week.drop_duplicates())

# Creating categorization and time pairs
# 軸をつくる
basis = category_combination.merge(time, how='cross')

new_df = basis.merge( hier_data, on=['Warehouse', 'Product_Category', 'week'], how='left')
print(new_df.head())
"""
  Warehouse Product_Category       week       Date  Order_Demand
0    Whse_J     Category_028 2018-06-04 2018-06-04        3354.0
1    Whse_J     Category_028 2018-06-04 2018-06-05        8310.0
2    Whse_J     Category_028 2018-06-04 2018-06-06        3750.0
3    Whse_J     Category_028 2018-06-04 2018-06-07        4105.0
4    Whse_J     Category_028 2018-06-04 2018-06-10        3031.0
"""
print(new_df.shape)  #=> (76141, 5)  行が増えてる。ナル値の組み合わせか?

# Replacing null values with one to make applicable to several forecaster models
# like `MultipexForecaster`
new_df = new_df.fillna(1.0)

#new_df["month_year"] = pd.to_datetime(new_df["month_year"], format = '%Y-%m')
# 予測させるときは期間にする
new_df["week"] = new_df["week"].dt.to_period('W')

new_df = new_df[new_df['week'] >= "2017-01-02"]  # 月曜日
new_df = new_df.groupby(['Warehouse', 'Product_Category', 'week'])["Order_Demand"].sum()
print(type(new_df))  #=> <class 'pandas.core.series.Series'>
print(new_df.head())
"""
Warehouse  Product_Category  month_year
Whse_A     Category_001      2017-01       1.0
                             2017-02       1.0
                             2017-03       1.0
                             2017-04       1.0
                             2017-05       1.0
Name: Order_Demand, dtype: float64   # Series 型のときはここに値の名前が表示
"""

# `to_frame()` は Series 型のメソッド.
new_df = new_df.to_frame()
print(type(new_df))  #=> <class 'pandas.core.frame.DataFrame'>
print(new_df.head())
"""   表の形が変わる. 値 (メジャー) 列を複数持てる.
                                       Order_Demand
Warehouse Product_Category week
Whse_A    Category_001     2017-01-02           1.0
                           2017-01-09           1.0
                           2017-01-16           1.0
                           2017-01-23           1.0
                           2017-01-30           1.0
"""

###### ここから学習させる ###################################################

# Training dataset
train_hier = new_df[new_df.index.get_level_values('week') <= "2020-01-06"]
print(train_hier.tail())
#print(train.index.get_level_values('month_year').dtype)
"""
                                       Order_Demand
Warehouse Product_Category week
Whse_S    Category_032     2019-12-09       14050.0
                           2019-12-16      152664.0
                           2019-12-23        8108.0
                           2019-12-30       11146.0
                           2020-01-06       19832.0
"""

# Validation dataset
validation_hier = new_df[new_df.index.get_level_values('week') > "2020-01-06"]

# 予測者を得る
from sktime.forecasting.base import ForecastingHorizon
from sktime.forecasting.arima import ARIMA
from sktime.forecasting.arima import AutoARIMA
from sktime.forecasting.exp_smoothing import ExponentialSmoothing # 指数平滑法 (へいかつほう) モデル

#forecaster = ARIMA()
#forecaster = AutoARIMA(suppress_warnings=True)
# seasonal_periods must be integer_like (int or np.integer, but not bool or timedelta64) or None
forecaster = ExponentialSmoothing(trend="add", seasonal="add", sp=52) 
  # 「季節成分=あり」は Holt-Winter's Seasonal Smoothing model
  # 季節成分がある対象に、よくフィットする
  
range = pd.date_range("2020-01-13", periods= 53, freq="W-SUN")
print(range.day_of_week)  #=> Index([6,6,6,6,...])  これが違う
fh = ForecastingHorizon(
    pd.PeriodIndex(range), is_relative=False
)

# fh: the forecasting horizon
y_pred = forecaster.fit(y = train_hier, fh = fh).predict()
print(y_pred)

""" AutoARIMA
                                       Order_Demand
Warehouse Product_Category month_year
Whse_A    Category_001     2020-02         1.000000
                           2020-03         1.000000
                           2020-04         1.000000
                           2020-05         1.000000
                           2020-06         1.000000
...                                             ...
Whse_S    Category_032     2020-09     79358.352558
                           2020-10     79374.774699
                           2020-11     79368.569596
                           2020-12     79370.914193
                           2021-01     79370.028288
[1128 rows x 1 columns]
"""

""" ARIMA
                                       Order_Demand
Warehouse Product_Category month_year
Whse_A    Category_001     2020-02        10.675234
                           2020-03        17.490448
                           2020-04        22.291070
                           2020-05        25.672617
                           2020-06        28.054571
...                                             ...
Whse_S    Category_032     2020-09     38057.379226
                           2020-10     38057.379226
                           2020-11     38057.379226
                           2020-12     38057.379226
                           2021-01     38057.379226
[1128 rows x 1 columns]
"""

print(forecaster.forecasters_)
y_pred.to_csv("output.csv")

y_pred.plot()

plt.show()
