
# グラフを表示する



<a href="https://hazm.at/mox/math/analysis/holt-winters-forecast.html">Holt-Winters 法 - MOXBOX</a>
> ホルト-ウィンターズ法は季節変動が必要である。株価のように明確な季節変動 (一定の周期変動) がない時系列を扱うことはできない。

レベル、トレンド、季節成分

```python
from statsmodels.tsa.api import ExponentialSmoothing
```

`trend`, `seasonal`, `seasonal_periods` 引数が必要。`"add"` or `"mul"`

季節性はあるが、傾向が一貫していない場合は, "add", "add" か. https://www.bounteous.com/insights/2020/09/15/forecasting-time-series-model-using-python-part-two





## Chart ライブラリ

### LiveCharts2  <a href="https://livecharts.dev/">LiveCharts2</a>
   SkiaSharp ベース。マルチプラットフォーム
  
   nuget パッケージ: LiveChartsCore.SkiaSharpView.WPF   やたら似た名前が多い
                    net462, netcoreapp3.1
                    
 - Cross platform
   + MAUI, Uno Platform, Avalonia
   + Xamarin
   + (EtoForms)

 - Web only
   + Blazor Wasm

 - Windows only
   + WPF, WinForms, and WinUI
   + <s>UWP</s> replaced with Uno Platform



### ScottPlot.NET   <a href="https://scottplot.net/faq/version-5.0/">What&#39;s New in ScottPlot 5.0</a>
   これも SkiaSharp ベース。高速なのが売り。
   
   nuget パッケージ: ScottPlot.WPF
                    net462, net6.0-windows7.0
                    
  In addition to supporting Windows Forms (WinForms), WPF, Avalonia and Eto, <i>ScottPlot</i> 5 now supports WinUI, Android, and Blazor!

  MVVM は対応していない。


SciChart | Fastest WPF, iOS, Android, JavaScript Charts
   2,000 $から.





## CSV ライブラリ

単に CSV ファイルを読むだけでも、多くのライブラリがあり、スピードもまちまち。
https://www.joelverhagen.com/blog/2020/12/fastest-net-csv-parsers

☆ CsvHelper   https://joshclose.github.io/CsvHelper/  一番有名
   RFC 4180 Compliant
   .NET 6.0; .NET Standard 2.0
   Apache License, Ver.2.0
  ●●使い方の記事は多い。例えば
      https://tomosoft.jp/design/?p=45333
      https://qiita.com/tnishiki/items/bfe0978592e023099588
      https://blog.okazuki.jp/entry/2014/12/26/121954
      
      破壊的変更がしばしば??

▲ ServiceStack.Text   JSON・JSV (JSON+CSV)・CSVに対応
       .NET 6.0; .NET Standard 2.0
   商用. $299 /developer
   
CsvTextFieldParser  -- Microsoft.VisualBasic アセンブリの置き換え.
   netstandard2.0

▲ LumenWorksCsvReader   最新が2018年リリース。開発終了
   netstandard2.0

TinyCsvParser
   .NET 6.0; netstandard2.0
   
▲ Csv    https://github.com/stevehansen/csv/   ずいぶん遅い
   .NET Framework 4.0; netstandard2.1
   Read, Write

