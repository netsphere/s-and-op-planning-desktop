
# S&OP Planning Desktop

実運用に使うのはお勧めしない。これはただのサンプル。

Licence: GPLv3+

Copyright (c) 2023-2024 Netsphere Laboratories, Hisashi Horikawa.
All rights reserved.



## What is this?

需要予測, 販売計画の合意, MRP, MPS, 財務レビュー
 - .NET Desktop 8.0 
 - SQL Server
 
<img src="screenshot1.png" width="480" />

<img src="mrp/step3.png" width="480" />



### 機能

 - マスタ管理
   - Excel取り込み
     + ✅️ product items, product categories
     + ✅️ channels
     + ✅️ Bill of Materials (BoM)
     + 為替レート表
     + RCCP
   - 閲覧
     + Products/Items  まだ途中. 単価の更新と保持。
     + ✅️ BoM

 - ✅️ Baseline Forecast  -- 先に forecast をつくって、人が修正していく運用イメージ
   + ✅️ 実績データからCSV出力
   + ✅️ <s>sktime</s> Nixtla を使う。いったん Holt-Winters 法決め打ち.
   + ✅️ Forecast Reconciliation. Empirical Risk Minimization (ERM) reconciliation strategy
   + 結果の取り込みと新しいヴァージョン計画作成. 取り込みはできた。❌商品単価の反映未了。実績があれば実績から、なければマスタから持ってくる

 - Demand Planning
   + ✅️ 品目カテゴリ、販売チャネルのキューブ
   + 編集機能  未了
   + 実績と計画をつなげて、3ヶ年のサイクルの折れ線グラフ
   + 別ヴァージョンとの比較
   + 

 - New Products
   + ファイルの取り込み

 - MRP  -- 先にオーダ提案を生成させ、供給計画のレビューと修正を行う運用イメージ。
   + ✅️ BoM展開, 所要量計算
   + ✅️ オーダ提案の作成
   + キャパシティ、発注サイクルの考慮
   + 統合未了 <s>❌需要側の最終需要とMPSの需要の合計が合っていない? 要デバグ.</s> fixed.

 - Supply Planning
   + オーダ提案の表示, レビューと修正
   + 更新後の MPS
   + 基準在庫 (=安全在庫 + サイクル在庫) 提案
   
 - Financial Review
   + 財務見通し
   + 保管費用 (在庫金利) のチャージ



## How to run

管理者権限で SQL Server Management Studio を実行。
データベースを作成。

<pre>
PM> <kbd>Update-Database</kbd>
Build started...
Build succeeded.
Applying migration <code>'20231208131637_InitialCreate'</code>.
Done.
</pre>


需要予測は Python でおこなう。次のパッケージを使う;
 - hierarchicalforecast
 - statsforecast
 
挙動確認
 1. Tool > Import Master Data... (1) 品目カテゴリの取り込み, (2) 品目一覧の取り込み, (3) その他マスタデータ
 2. Tool > Import Actual...  受注実績の取り込み    ❌ TODO: 出荷実績の取り込みテスト
 3. 実績表を確認   Current Plan: で実績を選択し, [Demand Plan]ボタン
           品目カテゴリを降りて、内訳。同じく販売チャネル。品目カテゴリは、個別商品も選択できる。
 4. PSI表を確認  <s>❌ 単に表示されない</s> fixed.
 5. 新しい計画期間をつくる
 6. 需要予測させる
 7. 予測期間を手で修正。(1) 下のカテゴリも比例配分される (2) 個別品目を発生させる
 8. MRPを実行
 9. 実行結果を確認



## 先行するプロダクト

<a href="https://www.goodfirms.co/production-scheduling-software/blog/best-free-production-scheduling-software">Top 8 Free and Open Source Production Scheduling Software</a>

  1. FrePPLe  Webベース。Odoo 統合あり。
              https://github.com/frePPLe/frepple/   C++, Python
  2. <s>OpenPro</s>
  3. Axelor Open Suite  https://github.com/axelor/axelor-open-suite/
                         Java
  4. ERPNext   https://github.com/frappe/erpnext/  Python
  5. Odoo
  6. <s>Dynamic 3i</s>
  7. <s>MRPeasy</s>
  8. <s>Katana</s>



## Get Involved

 1. Sign the CLA.
    I require all contributors to sign a contributor license agreement (CLA) before they can commit content to any of my projects.
    https://www.nslabs.jp/contributor-license-agreement.rhtml
 
 2. Submit your first pull request.



## About S&OP

<a href="https://blog.hubspot.com/sales/sales-operations-planning">S&OP: A Comprehensive Overview of Sales and Operations Planning</a>

 - Demand and Supply S&OP Metrics
   + Demand forecast versus actual
   + Production forecast versus actual
   + Inventory turnover   在庫回転率
   + Capacity utilization  設備稼働率
   + On-time delivery
   + Accuracy in order delivery
   + Cycle times
 - Financial S&OP Metrics
   + Total sales in a period (e.g., month, quarter, year)
   + Total sales versus forecast
   + Gross margin
   + Working capital versus plan

<a href="https://www.exa-corp.co.jp/column/scm-7.html">第7回 SCM効率を評価するKPIの新提案</a>

https://aws.amazon.com/jp/blogs/news/how-cpgs-can-improve-on-time-in-full-otif-deliveries/



## Development memo

### Migration

<pre>
PM> <kbd>Add-Migration InitialCreate</kbd>
Build started...
Build succeeded.
To undo this action, use <kbd>Remove-Migration</kbd>.
</pre>

