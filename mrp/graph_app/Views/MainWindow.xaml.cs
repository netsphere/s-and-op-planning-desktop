﻿
using CsvHelper;
using CsvHelper.Configuration;
using Microsoft.Win32;
using NReco.PivotData;
using System.Data;
using System.Globalization;
using System.IO;
using System.Windows;
using System.Windows.Input;


namespace graph_app.Views
{

class MainWindowViewModel : MVVM.BindableBase
{
    readonly DataTable _dataTable = new DataTable();
    public DataView MpsTableView => new DataView(_dataTable);

    readonly DataTable _grossReqDataTable = new DataTable();
    public DataView GrossReqTableView => new DataView(_grossReqDataTable);

    public DateTime HorizonStart;

    Mrp _mrp = new Mrp();


    public MainWindowViewModel()
    {
    }

    public DateOnly ActualEndDate { get; set; }


    List<FinalDemand>? readDemands()
    {
        var ofd = new OpenFileDialog() { Filter = "CSV files|*.csv" };
        if (ofd.ShowDialog() != true) 
            return null;

        // Web上の解説は誤りが多い。
        // See https://qiita.com/mkuwan/items/6b4fff2f28d4e5d8a9ee
        //    -> これも誤り。やっぱり `ReadHeader()` が必要

        using var reader = new StreamReader(ofd.FileName);
        var config = new CsvConfiguration(CultureInfo.InvariantCulture) {
                        HasHeaderRecord = true }; // 最近はデフォルトで true.
        using var csv = new CsvReader(reader, config);
        csv.Read();
        csv.ReadHeader();

        var demands = new List<FinalDemand>();
        while (csv.Read()) {
            var r = new FinalDemand {
                            // `[]` は見つからないとき例外
                            Date = csv.GetField<DateOnly>("date"),
                            Item = MyApp.Items[csv.GetField("item")],
                            Location = MyApp.Locations[csv.GetField("location")],
                            GrossReq = float.Parse(csv.GetField("qty"))
                };
            demands.Add(r);
        }

        return demands;
    }
    
    static readonly string[] row_names = new[] {
        "GrossReq", "Issued Orders", "Proposal Recieved", "Consumed", "Ending Stock", "Uncovered"
        };
        
    // 所要量計算の結果を表示
    void printExpandedDemands(DateOnly horizon_start, DateOnly horizon_end,
                              Mrp mrpResults)
    {
        Dictionary<ItemAndLocation, MpsPlan[]> demands = mrpResults.ExpandedDemands;

        _dataTable.Columns.Clear();
        _dataTable.Rows.Clear();
        _dataTable.Columns.Add("Item");
        _dataTable.Columns.Add("Location");
        _dataTable.Columns.Add("内容");

        // 先に列を全部作る。
        for (DateOnly dt = horizon_start.AddDays(-7); dt <= horizon_end; 
                                                        dt = dt.AddDays(7)) {
            // workaround: ヘッダに "/" が含まれると値が表示されない
            _dataTable.Columns.Add(new DataColumn() { 
                                    ColumnName = dt.ToString().Replace("/", "-")});
        }

        // 行を作っていく
        foreach (ItemAndLocation iloc in mrpResults.Reached) { 
            var rows = new List<DataRow>();
            foreach (var t_name in row_names) {
                var row = _dataTable.NewRow();
                row[0] = iloc.Item.Name;
                row[1] = iloc.Location.Name;
                row[2] = t_name;
                rows.Add(row);
            }

            for (int i = 0; i < demands[iloc].Count(); ++i) {
                var d = demands[iloc][i];            
                rows[0][i + 3] = d.GrossReq;
                rows[1][i + 3] = d.IssuedOrders;
                rows[2][i + 3] = d.PrpslRecieved;
                rows[3][i + 3] = d.Consumed;
                rows[4][i + 3] = d.Stock;
                rows[5][i + 3] = d.Uncovered;
            }
            foreach (var row in rows) 
                _dataTable.Rows.Add(row);
        }

        RaisePropertyChanged(nameof(MpsTableView));
    }


    static object getValue(object arg1, string dimName) {
        var row = (FinalDemand) arg1;
        switch (dimName) {
        case "item":     return row.Item.Name;
        case "location": return row.Location.Name;
        case "week":     return row.Date;
        case "QtySold":  return row.GrossReq;
        default:
            throw new ArgumentException();
        }
    }

    // 最終需要を表示
    // @param demands 最終需要表
    void printGrossRequirements(IReadOnlyList<FinalDemand> demands)
    {
        PivotData finalDemands = new PivotData(
                new[] {"item", "location", "week"},
                new CompositeAggregatorFactory(
                        new SumAggregatorFactory("QtySold") ));
        finalDemands.ProcessData(demands, getValue);

        // ここから表示
        _grossReqDataTable.Columns.Clear();
        _grossReqDataTable.Rows.Clear();
        _grossReqDataTable.Columns.Add("Item");
        _grossReqDataTable.Columns.Add("Location");
        _grossReqDataTable.Columns.Add("内容");

        var pvtTbl = new PivotTable( new[]{"item", "location"}, // dimension(s) for rows 
                                     new[]{"week"},    // Columns
                                     finalDemands);
        foreach (ValueKey dt in pvtTbl.ColumnKeys) {
            _grossReqDataTable.Columns.Add(new DataColumn() {
                                ColumnName = dt.DimKeys[0].ToString().Replace("/", "-")});
        }

        //var rows = new List<DataRow>();
        for (int r = 0; r < pvtTbl.RowKeys.Length; ++r) { 
            ValueKey rowKeys = pvtTbl.RowKeys[r];
            DataRow row = _grossReqDataTable.NewRow();
            row[0] = rowKeys.DimKeys[0].ToString();
            row[1] = rowKeys.DimKeys[1].ToString();
            row[2] = "QtySold";
            //rows.Add(row);

            for (var c = 0; c < pvtTbl.ColumnKeys.Length; ++c) { 
                decimal? v = (decimal?) pvtTbl[r, c].AsComposite().Aggregators[0].Value;
                if (v != null)  
                    row[3 + c] = ((decimal) v).ToString("F2");
                else
                    row[3 + c] = (decimal) 0.0;                
            }

            _grossReqDataTable.Rows.Add(row);
        }
        
        RaisePropertyChanged(nameof(GrossReqTableView));
    }


    // Command Handlers /////////////////////////////////////////////////

    public void handleExpandDemands(object sender, ExecutedRoutedEventArgs ea)
    {
        List<FinalDemand>? demands = readDemands() ;
        if (demands == null)
            return; 
        
        printGrossRequirements(demands);

        demands.Sort((a, b) => a.Date.CompareTo(b.Date)); // 破壊的メソッド
        var horizon_start = demands[0].Date;
        var horizon_end = demands.Last().Date ; // TODO: パラメータで指定

        // ●テストデータ
        List<Shipment> stocks = new List<Shipment>();
        stocks.Insert(0, new Shipment() { 
                    Date = horizon_start.AddDays(-7),
                    Item = MyApp.Items["構成品3"],
                    Location = MyApp.Locations["FACTORY1"],
                    OnHand = 30, });
        stocks.Insert(0, new Shipment() { 
                    Date = horizon_start.AddDays(-7),
                    Item = MyApp.Items["構成品2"],
                    Location = MyApp.Locations["FACTORY1"],
                    OnHand = 15, });

        _mrp.run2(horizon_start, horizon_end, demands, stocks);
        printExpandedDemands(horizon_start, horizon_end, _mrp);
    }

    public void handleOrderRecommendations(object sender, ExecutedRoutedEventArgs ea)
    {
        var wnd1 = new TransferOrdersWindow(_mrp.TransferOrders);
        wnd1.Show();

        var wnd2 = new PurchaseOrdersWindow(_mrp.PurchaseOrders);
        wnd2.Show();
    }
}


    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
public partial class MainWindow : Window
{
    public MainWindow()
    {
        InitializeComponent();

        var vm = (MainWindowViewModel) DataContext;
        var cmds = new List<CommandBinding>() {
                new CommandBinding(MyCommands.ExpandDemands, handleExpandDemands),
                new CommandBinding(MyCommands.OrderRecommendations, vm.handleOrderRecommendations)
            };
        foreach (var cmd in cmds)
            CommandBindings.Add(cmd);
    }


    // Event Handlers  //////////////////////////////////////////////
    
    void handleExpandDemands(object sender, ExecutedRoutedEventArgs e)
    {
        var vm = (MainWindowViewModel) DataContext;
        vm.handleExpandDemands(sender, e);

        // 列を緑色にする
        // @note this.Resources[] は DynamicResource
        var cnv = (ActualGreenConverter) this.FindResource("ActualGreenConverter");
        // TODO: もうちょっとまともな方法がない?
        cnv.HorizonStart = new DateOnly(2024, 8, 19);
    }

}


}