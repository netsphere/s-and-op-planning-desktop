﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace graph_app.Views
{

class TransferOrdersWindowVM : MVVM.BindableBase
{
    ObservableCollection<TransferOrder> _transfers;
    public ObservableCollection<TransferOrder> OrderList { 
        get => _transfers;
        set { SetPropertyAndRaise(ref _transfers, value); }
    }

    public TransferOrdersWindowVM() {
        //OrderList = new ObservableCollection<PurchaseOrder>();
    }
}


    /// <summary>
    /// order_window.xaml の相互作用ロジック
    /// </summary>
public partial class TransferOrdersWindow : Window
{
    public TransferOrdersWindow(List<TransferOrder> orders)
    {
        InitializeComponent();

        TransferOrdersWindowVM viewModel = (TransferOrdersWindowVM) DataContext;
        viewModel.OrderList = new ObservableCollection<TransferOrder>(orders);
    }
}

}
