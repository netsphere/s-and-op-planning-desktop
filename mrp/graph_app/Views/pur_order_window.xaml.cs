﻿
using System.Collections.ObjectModel;
using System.Windows;


namespace graph_app.Views
{

class PurchaseOrdersWindowVM : MVVM.BindableBase
{
    ObservableCollection<PurchaseOrder> _purchaseOrders;
    public ObservableCollection<PurchaseOrder> OrderList { 
        get => _purchaseOrders;
        set { SetPropertyAndRaise(ref _purchaseOrders, value); }
    }

    public PurchaseOrdersWindowVM() {
        //OrderList = new ObservableCollection<PurchaseOrder>();
    }
}


    /// <summary>
    /// order_window.xaml の相互作用ロジック
    /// </summary>
public partial class PurchaseOrdersWindow : Window
{
    public PurchaseOrdersWindow(List<PurchaseOrder> orders)
    {
        InitializeComponent();

        PurchaseOrdersWindowVM viewModel = (PurchaseOrdersWindowVM) DataContext;
        viewModel.OrderList = new ObservableCollection<PurchaseOrder>(orders);
    }
}

}
