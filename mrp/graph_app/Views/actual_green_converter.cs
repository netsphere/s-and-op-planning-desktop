﻿
using System.Data;
using System.Globalization;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows;


namespace graph_app.Views
{

// 実績は緑色の文字
// See https://qiita.com/7of9/items/e7c0a2b013a8aafc1424
//     Visual Studio / WPF > DataGrid > 2列だけ色を変える > IValueConverterの使用 > 一部失敗
[ ValueConversion(typeof(DataGridCell), typeof(Brush)) ]
class ActualGreenConverter : IValueConverter
{
    public DateOnly HorizonStart { get; set; }

    // デフォルトコンストラクタのみでないといけない

    // @override
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        DataGridCell dgc = (DataGridCell) value;
        DataGridColumn dg_col = dgc.Column; // .DisplayIndex プロパティ 0始まり
        // DataRowView, DataView は間を繋ぐ.
        DataRowView dc = (DataRowView) dgc.DataContext; 
        // 行を得るのは .Row, しかし列を得るのは DataRowView#[n] ではない。
        DataRow row = dc.Row;
        //object col = dc[dg_col.DisplayIndex]; // string "商品A" 値が取れる
        // .Table => DataTable
        DataColumn col = dc.DataView.Table.Columns[dg_col.DisplayIndex];
        // col.ColumnName が求める日付

        DateOnly col_date;
        if (DateOnly.TryParse(col.ColumnName, out col_date)) { 
            if (col_date < HorizonStart)
                return Brushes.Green;
        }

        return DependencyProperty.UnsetValue;
    }

    // @override
    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        throw new NotImplementedException();
    }
}



}
