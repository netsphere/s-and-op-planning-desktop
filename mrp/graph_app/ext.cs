﻿

namespace graph_app
{

/*
List<T>.Reverse() は破壊的メソッド!
Enumerable.Reverse(this IEnumerable<TSource>) は新しいインスタンスを生成
   -> 実装を見ると, IEnumerable<> だと個数が取れないので、一度全部 copy している
      これでは遅い
Enumerable.OrderBy() は並べ替え順序を指定
*/

static class Ext
{
    // 型を個数が取れるものに限定
    // Single-dimensional arrays also implement IList<T> and IEnumerable<T>.
    public static IEnumerable<T> ReverseIterator<T>(IList<T> items)
    {
        for (int i = items.Count - 1; i >= 0; --i)
            yield return items[i];
    }

    // @return x - y
    public static TimeSpan DateSubtract(DateOnly x, DateOnly y) {
        return x.ToDateTime(TimeOnly.MinValue) - y.ToDateTime(TimeOnly.MinValue);
    }
}

}
