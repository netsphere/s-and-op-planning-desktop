﻿
using System.ComponentModel.DataAnnotations;


namespace graph_app
{
   
public class OrderBase
{
    // Order の発出日
    [Required]
    public DateOnly IssueDate { get; set; }

    // 受取る品目+場所
    [Required]
    public ItemAndLocation ReceiveILoc { get; set; }

    [Required]
    public DateOnly ReceiveDate { get; set; }

    // 受取る数量
    [Required]
    public int Qty { get; set; }
}

public class PurchaseOrder : OrderBase
{
    [Required]
    public string Vendor { get; set; }
}

public class TransferOrder : OrderBase
{
    [Required]
    public Location ShipFrom { get; set; }
}

// KITTING or 製造
public class BuildOrder : OrderBase
{
    [Required]
    public Item InputItem { get; set; }

    [Required]
    public int InputQty { get; set; }
}

}
