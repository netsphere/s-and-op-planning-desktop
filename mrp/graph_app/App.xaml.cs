﻿
using System.Windows;
using System.Windows.Input;

namespace graph_app
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
public partial class MyApp : Application
{
    // 品目マスタ
    public static Dictionary<string, Item> Items = new Dictionary<string, Item>();

    // 場所マスタ
    public static Dictionary<string, Location> Locations = new Dictionary<string, Location>();

    // ベンダマスタ
    public static Dictionary<string, Vendor> Vendors = new Dictionary<string, Vendor>();

    public static List<PurchaseRule> PurchaseRules ;


    void make_ps(PSType type, Item parent, Item child, int qty) {
        var s = new Structure(type, parent, child, qty);
        parent.Children.Add(s); 
        child.Parents.Add(s);
    }

    void setup_master()
    {
        // 品目マスタ
        var item = new Item { Name = "商品A"}; Items.Add(item.Name, item);
        item = new Item { Name = "商品B"};     Items.Add(item.Name, item);

        for (int i = 0; i < 5; ++i) { 
            item = new Item { Name = $"構成品{i + 1}"};
            Items.Add(item.Name, item); 
        }

        // ASSEMBLE だと需要展開の時点で, 材料に時間概念が必要。いったん全部 KIT にする
        make_ps(PSType.KIT, Items["商品A"], Items["構成品1"], 1);
        make_ps(PSType.KIT, Items["商品A"], Items["構成品2"], 2);  // 共通構成品
        make_ps(PSType.KIT, Items["商品B"], Items["構成品2"], 3); 
        make_ps(PSType.KIT, Items["商品B"], Items["構成品3"], 5); 
        make_ps(PSType.KIT, Items["構成品2"], Items["構成品4"], 7); 
        make_ps(PSType.KIT, Items["構成品2"], Items["構成品5"], 11); 

        // 場所マスタ
        Locations.Add("FACTORY1", new Location(LocType.FACTORY, "FACTORY1"));
        Locations.Add("STORE1", new Location(LocType.SALES, "STORE1"));

        // ベンダマスタ
        Vendors.Add("V1", new Vendor() {Name = "V1"});
        Vendors.Add("V2", new Vendor() {Name = "V2"});
        Vendors.Add("V3", new Vendor() {Name = "V3"});

        // 購入ルール
        MyApp.PurchaseRules = new List<PurchaseRule> {
                new PurchaseRule() { 
                        Item = Items["商品A"], 
                        Supplier = "FACTORY1", Destination = Locations["STORE1"],
                        LeadTime = 7, MOQ = 1, OrderCycle = 7 },
                new PurchaseRule() { 
                        Item = Items["構成品1"], 
                        Supplier = "V1", Destination = Locations["FACTORY1"],
                        LeadTime = 14, MOQ = 50, OrderCycle = 21 },
                new PurchaseRule() { 
                        Item = Items["構成品3"], 
                        Supplier = "V1", Destination = Locations["FACTORY1"],
                        LeadTime = 21, MOQ = 50, OrderCycle = 14 },
                new PurchaseRule() { 
                        Item = Items["構成品4"], 
                        Supplier = "V2", Destination = Locations["FACTORY1"],
                        LeadTime = 14, MOQ = 50, OrderCycle = 14 },
                new PurchaseRule() { 
                        Item = Items["構成品5"], 
                        Supplier = "V3", Destination = Locations["FACTORY1"],
                        LeadTime = 21, MOQ = 50, OrderCycle = 21 },
            };
    }


    // Event Handlers /////////////////////////////////////////////////////

    void Application_Startup(object sender, StartupEventArgs e)
    {
        setup_master();

        var wnd = new Views.MainWindow();
        wnd.Show();
    }
}


static class MyCommands
{
    public static readonly RoutedUICommand ExpandDemands =
            new RoutedUICommand("Do MRP", nameof(ExpandDemands), typeof(MyCommands));

    public static readonly RoutedUICommand OrderRecommendations =
            new RoutedUICommand("Show results", nameof(OrderRecommendations), 
                                typeof(MyCommands));
}

}
