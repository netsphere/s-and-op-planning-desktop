using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace MVVM
{

// 似たようなクラスは、至るところに定義されている。
// 例えば,
// https://github.com/jamesmontemagno/mvvm-helpers/blob/master/MvvmHelpers/ObservableObject.cs

// ViewModelBase より相応しい名前
public abstract class BindableBase : INotifyPropertyChanged
{
    // プロパティの変更を通知するためのイベント.
    public event PropertyChangedEventHandler PropertyChanged;

    // 値の設定と notify を兼ねる
    // 値が異なる場合のみ、プロパティを設定し、リスナに通知する
    protected virtual bool SetPropertyAndRaise<T>(ref T storage, T value,
                                [CallerMemberName] string propertyName = "",
                                Action onChanged = null,
                                Func<T, T, bool> validateValue = null )
            //where T : class
    {
        if (EqualityComparer<T>.Default.Equals(storage, value))
            return false;

        if (validateValue != null && !validateValue(storage, value))
            return false;

        storage = value;

        onChanged ?.Invoke();
        RaisePropertyChanged(propertyName);

        return true;
    }


    // プロパティ値が変更されたことをリスナに通知する
    protected virtual void RaisePropertyChanged([CallerMemberName] string prop = "")
    {
        PropertyChanged ?.Invoke(this, new PropertyChangedEventArgs(prop));
    }

} // class ViewModelBase

}
