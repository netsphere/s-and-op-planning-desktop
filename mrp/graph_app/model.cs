﻿
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace graph_app
{

// 品目
public class Item
{
    //public int Id { get; set; }
    [Required]
    public string Name { get; set; }

    public List<Structure> Parents { get; }
    public List<Structure> Children { get; }

    public Item() { 
        Parents = new List<Structure>();
        Children = new List<Structure>();
    }

    public override bool Equals(object? obj) { 
        if (obj == null)
            return false;
        return this.Name == ((Item) obj).Name ;
    }

    public override int GetHashCode() {
        return this.Name.GetHashCode();
    }
}

public enum PSType {
    MANUFACTURE = 1,
    KIT = 2,
}

// これが edge (辺) の元の一つ
public class Structure 
{ 
    [Required]
    public PSType Type { get; }

    [Required]
    public Item Parent { get; }

    [Required]
    public Item Child { get; }

    [Required]
    public int Qty { get; }

    public Structure(PSType type, Item parent, Item child, int qty) {  
        Type = type;
        Parent = parent; Child = child; Qty = qty; 
    }
}

public enum LocType {
    FACTORY = 1,
    SALES = 2,
}

// 場所
public class Location
{ 
    [Required]
    public LocType Type { get; }

    //public int Id { get; set; } 

    [Required]
    public string Name { get; }

    public Location(LocType type, string name) {
        Type = type; Name = name;
    }

    public override bool Equals(object? obj) { 
        if (obj == null)
            return false;
        return this.Name == ((Location) obj).Name ;
    }

    public override int GetHashCode()
    {
        return this.Name.GetHashCode();
    }
}


public class Vendor {
    [Required]
    public string Name { get; set; }
}

// 横持ちもサイクルがある. supplier = location or vendor.
// これが edge の元
public class PurchaseRule 
{ 
    [Required]
    public Item Item { get; set; }

    // Location or vendor
    [Required]
    public string Supplier { get; set; }

    // 受取場所
    [Required]
    public Location Destination { get; set; }

    [Required]
    public int LeadTime { get; set; }

    [Required]
    public int MOQ { get; set; }

    [Required]
    public int OrderCycle { get; set; }
}

// 生産ルール
class WorkRule {
    [Required]
    public Location Location { get; set; }

    // Structure が別にあるので、ここはアウトプットのみ
    [Required]
    public Item Product { get; set; }

    [Required]
    public int LeadTime { get; set; }

    [Required]
    public int Lot { get; set; }

}


public class ItemAndLocation : IEquatable<ItemAndLocation> 
{
    [Required]
    public Item Item { get; set; }

    [Required]
    public Location Location { get; set; }


    // Dictionary の key にする場合, これも必要
    // @override IEquatable<>
    public bool Equals(ItemAndLocation? other)
    {
        if (ReferenceEquals(other, null))
            return false;
        return Item.Name == other.Item.Name && 
               Location.Name == other.Location.Name ;
    }

    // 非ジェネリック版も実装しないといけない
    public override bool Equals(object? obj) 
    {
        if (obj is ItemAndLocation) 
            throw new ArgumentException("internal error");
    
        return Equals(obj as ItemAndLocation);
    }

    // これも実装必須
    public override int GetHashCode()
    {
        return HashCode.Combine(Item.Name, Location.Name);
    }
}

// For `HashSet`
class ItemAndLocCompare : IEqualityComparer<ItemAndLocation> { 
    static ItemAndLocCompare? _instance = null;
    public static ItemAndLocCompare instance() {
        if (_instance == null) 
            _instance = new ItemAndLocCompare();
        return _instance;
    }

    // @override
    public bool Equals(ItemAndLocation? x, ItemAndLocation? y) {
        if (ReferenceEquals(x, null))
            return ReferenceEquals(y, null) ? true : false;

        return x.Equals(y);
    }

    // @override
    public int GetHashCode([DisallowNull] ItemAndLocation obj) {
        return obj.GetHashCode();
    }
}


// 最終需要
class FinalDemand : ItemAndLocation
{
    [Required]
    public DateOnly Date { get; set; }

    [Required]
    public double GrossReq { get; set; }
}

// 出荷と手元在庫
class Shipment : ItemAndLocation
{
    [Required]
    public DateOnly Date { get; set; }

    [Required]
    public int OnHand { get; set; }
}


class MpsPlan : ItemAndLocation
{
    [Required]
    public DateOnly Date { get; set; }

    //[Required]
    //public Item Item { get; set; }

    //[Required]
    //public Location Location { get; set; }

    [Required]
    public int GrossReq { get; set; }

    [Required]
    public int IssuedOrders { get; set; }

    [Required]
    public int PrpslRecieved { get; set; }

    [Required]
    public int NetReq { get; set; }

    // 受注残  -> 期首期間の demand qty を使う
    //[Required]
    //public int Unfilled { get; set; }

    [Required]
    public int Consumed { get; set; }

    [Required]
    public int Stock { get; set; }

    [Required]
    public int Uncovered { get; set; }
}


}
