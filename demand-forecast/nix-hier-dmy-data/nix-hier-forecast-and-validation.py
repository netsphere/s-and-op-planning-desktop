
# 日付列は月末日でなければなさそう
# spec は合計列が必要
# top-down を使う場合は、木の形でなければならない


# https://forecastegy.com/posts/hierarchical-time-series-forecasting-python/

import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
import os
import sys

data = pd.read_csv("dmy-data.csv", parse_dates=['ds']).loc[:, ['ds', 'product', 'channel', 'y']]
data['totalcol'] = 'totalcol'
#data['Date'] = pd.to_datetime(data['Date'])
#data = data.rename(columns={'Date': 'ds', 'Order_Demand': 'y'})
#data.set_index(['ds'], inplace=True)
#data = data.resample('M').sum()
print(data.columns)
print(data.index)
#print(data)

train = data.loc[ data['ds'] < '2023-01-01' ]
#print(train)
valid = data.loc[ (data['ds'] >= '2023-01-01') & (data['ds'] < '2024-01-01') ]
#print(valid)
h = valid['ds'].nunique()
print(h)   #=> 12

spec = [['totalcol'],
        ['totalcol', 'product'],
        ['totalcol', 'product', 'channel'],
        #['totalcol', 'channel']   ValueError: Top-down reconciliation requires strictly hierarchical structures.
        ]

from hierarchicalforecast.utils import aggregate

train_agg, S_train, tags = aggregate(train, spec)
valid_agg, _, _ = aggregate(valid, spec)
print(train_agg)

from statsforecast import StatsForecast
from statsforecast.models import HoltWinters, AutoARIMA, SeasonalExponentialSmoothing
model = StatsForecast(#df=train_agg,
                 models=[HoltWinters(season_length=12, error_type='A')],
                 # models=[AutoARIMA(season_length=12)],
                 #models=[SeasonalExponentialSmoothing(season_length=12, alpha=0.1)],
                              freq='M' , n_jobs=-1)
model.fit(train_agg)

p = model.forecast(h=h, fitted=True)
p_fitted = model.forecast_fitted_values()

from hierarchicalforecast.methods import BottomUp, TopDown, MinTrace, ERM, OptimalCombination
from hierarchicalforecast.core import HierarchicalReconciliation

reconcilers = [BottomUp(), 
               TopDown(method='forecast_proportions'),
               TopDown(method='average_proportions'),
               TopDown(method='proportion_averages'),
               MinTrace(method='ols', nonnegative=True),
               MinTrace(method='wls_struct', nonnegative=True),
               MinTrace(method='wls_var', nonnegative=True),
               MinTrace(method='mint_shrink', nonnegative=True), 
               MinTrace(method='mint_cov', nonnegative=True),
               OptimalCombination(method='ols', nonnegative=True), 
               OptimalCombination(method='wls_struct', nonnegative=True),
               ERM(method='closed'),
               ERM(method='reg'),
               ERM(method='reg_bu'),
              ]

rec_model = HierarchicalReconciliation(reconcilers=reconcilers)

p_rec = rec_model.reconcile(Y_hat_df=p, Y_df=p_fitted, S=S_train, tags=tags)
p_rec.to_csv("reconcile-output.csv")

p_rec_ = p_rec.merge(valid_agg, on=['ds', 'unique_id'], how='left')
p_rec_['y'] = p_rec_['y'].fillna(0)
p_rec_ = p_rec_.reset_index(drop=False)

from sklearn.metrics import mean_squared_error

rmse = dict()
for model_ in p_rec.columns[1:]:
    rmse_ = mean_squared_error(p_rec_['y'].values, p_rec_[model_].values, squared=False)/1e3
    # get only the model name
    model__ = model_.split('/')[-1]
    rmse[model__] = rmse_

print( pd.DataFrame(rmse, index=['RMSE']).T.sort_values('RMSE') )
"""
                                                         RMSE
TopDown_method-average_proportions                  43.296613
TopDown_method-proportion_averages                  45.931197
TopDown_method-forecast_proportions                 47.451296
MinTrace_method-ols_nonnegative-True                51.393378
OptimalCombination_method-ols_nonnegative-True      51.393378
HoltWinters                                         51.525430
MinTrace_method-wls_var_nonnegative-True            51.636645
MinTrace_method-mint_shrink_nonnegative-True        52.040063
MinTrace_method-wls_struct_nonnegative-True         52.369882
OptimalCombination_method-wls_struct_nonnegativ...  52.369882
MinTrace_method-mint_cov_nonnegative-True           52.464643
BottomUp                                            52.651804
ERM_method-reg_lambda_reg-0.01                      58.319068
ERM_method-closed_lambda_reg-0.01                   59.010100
ERM_method-reg_bu_lambda_reg-0.01                   59.054801
"""

