
# 日付列は月末日でないといけない
# top-down を使う場合は、木の形でなければならない


# https://forecastegy.com/posts/hierarchical-time-series-forecasting-python/
#  -> MinTrace, Optimal Combination and Empirical Risk Minimization のどれかがいいのでは?
#     実データで試して決めるのがよい

# https://www.kaggle.com/code/houssemaminetouihri/ts-8-hierarchical-time-series
#   -> MinTrace の模式図がある


import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
import os
import sys

data = pd.read_csv("sales-4700lines.csv", parse_dates=['ds'],
                dtype={'product':'string', 'category':'string', 'channel':'string', 'y':'float'}
                   ).loc[:, ['ds', 'product', 'category', 'channel', 'y']]
# spec は合計列が必要
data['totalcol'] = 'totalcol'

#data['Date'] = pd.to_datetime(data['Date'])
#data = data.rename(columns={'Date': 'ds', 'Order_Demand': 'y'})
#data.set_index(['ds'], inplace=True)
#data = data.resample('M').sum()
print(data.columns)
print(data.index)
#print(data)

train = data.loc[ data['ds'] < '2026-01-01' ]
print(train)
#valid = data.loc[ (data['ds'] >= '2023-01-01') & (data['ds'] < '2024-01-01') ]
#print(valid)
#h = valid['ds'].nunique()
#print(h)   #=> 12
h = 53

spec = [['totalcol'],
        ['totalcol', 'category'],
        ['totalcol', 'category', 'product'],
        ['totalcol', 'channel', 'category', 'product'],  # 'product' がリスト内に必要
        #['totalcol', 'product', 'channel'],
        #['totalcol', 'channel']   ValueError: Top-down reconciliation requires strictly hierarchical structures.
        ]

from hierarchicalforecast.utils import aggregate

train_agg, S_train, tags = aggregate(train, spec)
#valid_agg, _, _ = aggregate(valid, spec)
print(train_agg)

from statsforecast import StatsForecast
from statsforecast.models import HoltWinters, AutoARIMA, SeasonalExponentialSmoothing
model = StatsForecast(models=[HoltWinters(season_length=52, error_type='A')],
                 # models=[AutoARIMA(season_length=12)],
                 #models=[SeasonalExponentialSmoothing(season_length=12, alpha=0.1)],
                              freq='W' , n_jobs=-1)
model.fit(train_agg)

p = model.forecast(df=train_agg, h=h, fitted=True)
p_fitted = model.forecast_fitted_values()

from hierarchicalforecast.methods import BottomUp, TopDown, MinTrace, ERM, OptimalCombination
from hierarchicalforecast.core import HierarchicalReconciliation

# TODO: eval
# eg. https://github.com/Nixtla/hierarchicalforecast/blob/main/nbs/examples/TourismLarge-Evaluation.ipynb
reconcilers = [
    # BottomUp(),    NG, <0
    # TopDown(method='forecast_proportions'),  NG, <0     top-downメソッド3択
#    TopDown(method='average_proportions'),
#    TopDown(method='proportion_averages'),
#    MinTrace(method='ols', nonnegative=True),              # min-traceメソッド5択
#    MinTrace(method='wls_struct', nonnegative=True),
#    MinTrace(method='wls_var', nonnegative=True),
#    MinTrace(method='mint_shrink', nonnegative=True), 
    #MinTrace(method='mint_cov', nonnegative=True),
#    OptimalCombination(method='ols', nonnegative=True),        # メソッド2択
#    OptimalCombination(method='wls_struct', nonnegative=True),
    ERM(method='closed'),     # Emp. Risk Minimization   # メソッド3択
#    ERM(method='reg'),
#    ERM(method='reg_bu'),
              ]

rec_model = HierarchicalReconciliation(reconcilers=reconcilers)

p_rec = rec_model.reconcile(Y_hat_df=p, Y_df=p_fitted, S=S_train, tags=tags)
p_rec.to_csv("reconcile-output.csv")

