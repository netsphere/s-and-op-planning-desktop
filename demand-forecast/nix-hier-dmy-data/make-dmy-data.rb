
require 'csv'

prods = ["Prod#1", "Prod#2"]
channels = ["Shop#1", "Shop#2"]

print "ds,product,channel,y\n"

prods.each do |prod|
  channels.each do |channel|
    y = 1000.0
    d = Date.new(2020,1,1)  
    while d < Date.new(2024, 1, 1)
      y = y + y * (rand() - 0.4) # / 2.0, 0].max
      print "#{d.next_month - 1},#{prod},#{channel},#{y}\n"    # must be end day
      d = d.next_month
    end
  end
end

        
