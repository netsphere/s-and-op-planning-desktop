
# darts 環境構築

darts は依存ライブラリがあまりに多すぎて、いったん見送り。


時系列のライブラリには, darts, sktime などがある
the Nixtla suite, SKTime, and DARTS are the big 3.


 - darts -- 内部で tbats, torch などのライブラリを利用
            pandas なども
        ARIMAモデル, など様々なモデルを統一的に扱う
 - Prophet -- Meta 社 (旧 Facebook 社)
 - sktime -- ARIMAモデルも含む
 - tslearn --


 - ▲ scikit-hts
   階層時系列 hierarchical time series modeling
     内部で statsmodels を利用。しかし statsmodels の SARIMAX は非効率.

 - nixtla
    <a href="https://nixtlaverse.nixtla.io/hierarchicalforecast/">Hierarchical Forecast 👑</a>





## darts のインストール

コマンド一発:
<pre>
$ <kbd>pip install --break-system-packages darts</kbd>
</pre>


<blockquote>
Successfully installed 
Cython-3.0.5 MarkupSafe-2.1.3 PyYAML-6.0.1 adagio-0.2.4 aiohttp-3.8.6 aiosignal-1.3.1 antlr4-python3-runtime-4.11.1 appdirs-1.4.4 async-timeout-4.0.3 attrs-23.1.0 certifi-2023.7.22 charset-normalizer-3.3.2 cloudpickle-3.0.0 contourpy-1.2.0 cycler-0.12.1 darts-0.26.0 filelock-3.13.1 fonttools-4.44.0 frozenlist-1.4.0 fs-2.4.16 fsspec-2023.10.0 fugue-0.8.6 fugue-sql-antlr-0.1.8 holidays-0.35 idna-3.4 jinja2-3.1.2 joblib-1.3.2 kiwisolver-1.4.5 lightning-utilities-0.9.0 llvmlite-0.41.1 matplotlib-3.8.1 mpmath-1.3.0 multidict-6.0.4 networkx-3.2.1 nfoursid-1.0.1 numba-0.58.1 numpy-1.26.1 nvidia-cublas-cu12-12.1.3.1 nvidia-cuda-cupti-cu12-12.1.105 nvidia-cuda-nvrtc-cu12-12.1.105 nvidia-cuda-runtime-cu12-12.1.105 nvidia-cudnn-cu12-8.9.2.26 nvidia-cufft-cu12-11.0.2.54 nvidia-curand-cu12-10.3.2.106 nvidia-cusolver-cu12-11.4.5.107 nvidia-cusparse-cu12-12.1.0.106 nvidia-nccl-cu12-2.18.1 nvidia-nvjitlink-cu12-12.3.52 nvidia-nvtx-cu12-12.1.105 packaging-23.2 pandas-2.1.2 patsy-0.5.3 pillow-10.1.0 pmdarima-2.0.4 polars-0.19.12 protobuf-4.25.0 pyarrow-14.0.0 pyod-1.1.1 pyparsing-3.1.1 python-dateutil-2.8.2 pytorch-lightning-2.1.0 pytz-2023.3.post1 qpd-0.4.4 requests-2.31.0 scikit-learn-1.3.2 scipy-1.11.3 shap-0.43.0 six-1.16.0 slicer-0.0.7 sqlglot-19.0.3 statsforecast-1.6.0 statsmodels-0.14.0 sympy-1.12 tbats-1.1.3 tensorboardX-2.6.2.2 threadpoolctl-3.2.0 torch-2.1.0 torchmetrics-1.2.0 tqdm-4.66.1 triad-0.9.1 triton-2.1.0 typing-extensions-4.8.0 tzdata-2023.3 urllib3-2.0.7 xarray-2023.10.1 xgboost-2.0.1 yarl-1.9.2
</blockquote>
