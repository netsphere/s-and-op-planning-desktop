
import pandas as pd
import polars
from sktime.datasets import load_hierarchical_sales_toydata
from sktime.forecasting.arima import ARIMA
from sktime.forecasting.base import ForecastingHorizon

# 2000-01..2004-12
y = load_hierarchical_sales_toydata()
print(y)
"""
注意: column 数が 1 になっている
                                         Sales
Product line      Product group Date          
Food preparation  Hobs          2000-01  245.0
                                2000-02  144.0
                                2000-03  184.0
                                2000-04  265.0
                                2000-05  236.0
...                                        ...
Food preservation Fridges       2004-08  176.0
                                2004-09  205.0
                                2004-10   59.0
                                2004-11  151.0
                                2004-12  173.0
[240 rows x 1 columns]
"""

#polars.exceptions.ComputeError: cannot create series from Extension("pandas.period", Int64, Some("{\"freq\": \"M\"}"))
#yp = polars.from_pandas(y, include_index=True)
#print(yp)

forecaster = ARIMA()

# 予測期間
fh = ForecastingHorizon(
       pd.PeriodIndex(pd.date_range("2005-01", periods=12, freq="ME")),
       is_relative=False)

y_pred = forecaster.fit(y, fh=fh).predict()
print(y_pred)

print(forecaster.forecasters_)

y.to_csv("hier-output.csv")

