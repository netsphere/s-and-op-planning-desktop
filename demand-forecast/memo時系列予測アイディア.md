
# 時系列予測アイディア


## 階層時系列 Hierarchical

https://josephine-amponsah.medium.com/forecasting-demand-of-retail-inventory-hierarchical-approach-3ec9c42a65e8
HIERARCHICAL FORECASTING: RETAIL INVENTORY PLANNING


https://www.salesanalytics.co.jp/software/sktime/sktime005/
第5回：sktimeのデータ構造
   -> 階層と観測日を pandas.MultiIndex として構成. 観測日が最下層
      観測値の列名を columns として指定.
      
データの種類 scitype   データの持ち方 mtype
-------------------------------------------
  Series 時系列データ    pd.Series, pd.DataFrame
  Hierarchical 階層      `pd_multiindex_hier`


    https://www.sktime.net/en/stable/examples/AA_datatypes_and_datasets.html#Section-1.3:-Hierarchical-time-series---the-%22Hierarchical%22-scitype
    In-memory data representations and data loading

    TSF format ファイルから読み込む関数
    https://www.sktime.net/en/stable/api_reference/auto_generated/sktime.datasets.load_tsf_to_dataframe.html



https://www.salesanalytics.co.jp/software/sktime/sktime006/
第6回：データの読み込み方・作り方（CSV編）

   この読み込み方でいいかどうか? sub-total を表示するには?



## sktime サンプルデータ

https://sktime-backup.readthedocs.io/en/stable/api_reference/auto_generated/sktime.datasets.load_hierarchical_sales_toydata.html
load_hierarchical_sales_toydata
   Product hierarchy with row MultiIndex “Product line”, “Product group”, “Date”. Column “Sales” contains total sales for each product group in monthly period.
    -> 5年分の月データ




## ライブラリ




<a href="https://nykergoto.hatenablog.jp/entry/2019/07/09/FFT_%E3%82%92%E4%BD%BF%E3%81%A3%E3%81%9F%E6%99%82%E7%B3%BB%E5%88%97%E3%83%87%E3%83%BC%E3%82%BF%E8%A7%A3%E6%9E%90">FFT を使った時系列データ解析</a>

  季節性が明らかな場合は、先に、季節性を除去するのはどうか?
      -> 注意。「予測」には使えなさそう




## 間欠需要 Intermittent Demand


- <a href="https://otexts.com/fppjp/counts.html">13.2 カウント数の時系列 | 予測: 原理と実践 (第3版)</a>

クロストン法 Croston's Method
実装例  https://www.pmorgan.com.au/tutorials/crostons-method/


<i>sktime</i> には class `Croston` がある
https://www.sktime.net/en/stable/api_reference/auto_generated/sktime.forecasting.croston.Croston.html

https://towardsdatascience.com/croston-forecast-model-for-intermittent-demand-360287a17f5f
  需要が 0 のときにも直前の数値が入るので、過剰になる?
  手書きコードの実装例
     改良版
        Croston TSB Model
           - Simple Smoothing に近い。ほぼ同じ




## Exponential Smoothing 指数平滑法

シンプルに指数平滑法を使うのが、意外性も生じず、結局いいのでは?

Holt-Winter's Seasonal Smoothing model
  トレンド成分=あり, 季節成分=あり
    -> https://www.salesanalytics.co.jp/datascience/datascience086/
    
sktime  class ExponentialSmoothing
    trend= , seasonal= を指定できる   ☆



## Polars

sktime の changelog を見ると、サポートが進んでいるようだ。



