
# https://www.sktime.net/en/latest/examples/01_forecasting.html
# User Guide > Forecasting with sktime

import warnings
import numpy as np
import pandas as pd
# hide warnings
warnings.filterwarnings("ignore")
import matplotlib.pyplot as plt

# step 1: Preparation of the data
# Use demo data
from sktime.datasets import load_airline
from sktime.utils.plotting import plot_series

# Using a ForecastingHorizon based forecasting horizon
from sktime.forecasting.base import ForecastingHorizon
# Engine
# `AutoARIMA` requires package 'pmdarima'
from sktime.forecasting.arima import AutoARIMA

# MAPE
from sktime.performance_metrics.forecasting import MeanAbsolutePercentageError
from sktime.split import temporal_train_test_split

# demo data
y = load_airline()
y_train, y_test = temporal_train_test_split(y, test_size=36)

print(y)

# step 2: specifying forecasting horizon
#   Using a numpy forecasting horizon
#   fh = np.arange(1, 37)
fh = ForecastingHorizon(y_test.index, is_relative=False)

# step 3: specifying the forecasting algorithm
forecaster = AutoARIMA(sp=12, suppress_warnings=True)

# step 4: fitting the forecaster
forecaster.fit(y_train)

# step 5: querying predietions
y_pred = forecaster.predict(fh)

# optional: plotting predictions and past data
plot_series(y_train, y_test, y_pred, labels=["y_train", "y_test", "y_pred"])

mape = MeanAbsolutePercentageError(symmetric=False)
print(mape(y_test, y_pred))

plt.show()
