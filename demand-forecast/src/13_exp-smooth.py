
# チュートリアルどおりにやってみる
# https://www.sktime.net/en/latest/examples/01_forecasting.html

#https://www.salesanalytics.co.jp/datascience/datascience116/
#SARIMAモデルは、季節成分が1つの場合に対応し、複数には対応していません。
#複数の季節性をもつ時系列モデルがあります。その1つがTBATSモデルです。


import warnings
import numpy as np
import pandas as pd
# hide warnings
warnings.filterwarnings("ignore")
import matplotlib.pyplot as plt

# デモデータを使う
from sktime.datasets import load_airline
from sktime.utils.plotting import plot_series

from sktime.forecasting.base import ForecastingHorizon
# 推測するエンジン
#from sktime.forecasting.naive import NaiveForecaster
#from sktime.forecasting.arima import AutoARIMA

#from sktime.forecasting.tbats import TBATS
from sktime.forecasting.exp_smoothing import ExponentialSmoothing
#from sktime.forecasting.statsforecast import (StatsForecastAutoARIMA, StatsForecastAutoTBATS )

# 精度検証
from sktime.performance_metrics.forecasting import MeanAbsolutePercentageError
from sktime.split import temporal_train_test_split

y = load_airline()
y_train, y_test = temporal_train_test_split(y, test_size=36)

print(y)
# Period
# 1949-01    112.0
# 1949-02    118.0
# 1949-03    132.0

# step 2: specifying forecasting horizon
#fh = np.arange(1, 37)    # 予測期間 = 3年
fh = ForecastingHorizon(y_test.index, is_relative=False)

# step 3: specifying the forecasting algorithm
#forecaster = NaiveForecaster(strategy="last", sp=12)
#forecaster = AutoARIMA(sp=12, suppress_warnings=True)
#forecaster = TBATS(sp=12, use_trend=True, use_box_cox=False)
forecaster = ExponentialSmoothing(trend="add", seasonal="add")
#forecaster = StatsForecastAutoTBATS(seasonal_periods=1)

# step 4: fitting the forecaster
#forecaster.fit(y)
forecaster.fit(y_train)

# step 5: querying predictions
#y_pred = forecaster.predict(fh)
y_pred = forecaster.predict(fh)

# optional: plotting predictions and past data
#plot_series(y, y_pred, labels=["y", "y_pred"])
plot_series(y_train, y_test, y_pred, labels=["y_train", "y_test", "y_pred"])

mape = MeanAbsolutePercentageError(symmetric=False)
print(mape(y_test, y_pred))

# これでOK. 内部で共用される.
plt.show()

#from sktime.forecasting.compose import make_reduction
#from sklearn.ensemble import RandomForestRegressor
#from sktime.forecasting.model_selection import temporal_train_test_split
#from sktime.performance_metrics.forecasting import MeanAbsolutePercentageError



