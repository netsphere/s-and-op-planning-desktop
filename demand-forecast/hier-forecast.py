
# https://codecut.ai/hierarchical-forecasting-in-python/

import numpy as np
import pandas as pd

Y_df = pd.read_csv('https://raw.githubusercontent.com/Nixtla/transfer-learning-time-series/main/datasets/tourism.csv')
Y_df = Y_df.rename({'Trips': 'y', 'Quarter': 'ds'}, axis=1)

# わざわざ追加しているのは、合計列が必要?
Y_df.insert(0, 'Country', 'Australia')

Y_df = Y_df[['Country', 'State', 'Region', 'Purpose', 'ds', 'y']]
Y_df['ds'] = Y_df['ds'].str.replace(r'(\d+) (Q\d)', r'\1-\2', regex=True)
Y_df['ds'] = pd.to_datetime(Y_df['ds'])

print(Y_df.head())
"""
     Country            State    Region   Purpose         ds           y
0  Australia  South Australia  Adelaide  Business 1998-01-01  135.077690
1  Australia  South Australia  Adelaide  Business 1998-04-01  109.987316
2  Australia  South Australia  Adelaide  Business 1998-07-01  166.034687
3  Australia  South Australia  Adelaide  Business 1998-10-01  127.160464
4  Australia  South Australia  Adelaide  Business 1999-01-01  137.448533
"""

spec = [
    ['Country'],   # ここが合計列の列名
    ['Country', 'State'], 
    ['Country', 'Purpose'], 
    ['Country', 'State', 'Region'], 
    ['Country', 'State', 'Purpose'], 
    ['Country', 'State', 'Region', 'Purpose']
]

from hierarchicalforecast.utils import aggregate

Y_df, S_df, tags = aggregate(Y_df, spec)
Y_df = Y_df.reset_index()

print( Y_df.sample(10) )
"""
                                               unique_id         ds           y
9827                      Australia/ACT/Canberra/Holiday 2014-10-01  135.255625
23378     Australia/South Australia/Murraylands/Visiting 2002-07-01   15.554179
29903         Australia/Victoria/Melbourne East/Business 2013-10-01    8.603122
21260    Australia/South Australia/Clare Valley/Business 2013-01-01   10.923392
11708  Australia/New South Wales/New England North We... 2005-01-01  151.224015
24765  Australia/Tasmania/Launceston, Tamar and the N... 2009-04-01   30.898817
11175        Australia/New South Wales/Central NSW/Other 2011-10-01   25.063269
17190    Australia/Queensland/Central Queensland/Holiday 2015-07-01  131.575216
23529        Australia/South Australia/Riverland/Holiday 2000-04-01   45.205097
17810          Australia/Queensland/Fraser Coast/Holiday 2010-07-01   83.430484
"""

Y_test_df = Y_df.groupby('unique_id').tail(8)
Y_train_df = Y_df.drop(Y_test_df.index)

Y_test_df = Y_test_df.set_index('unique_id')
Y_train_df = Y_train_df.set_index('unique_id')

print( Y_train_df.groupby('unique_id').size() )
"""
unique_id
Australia                                                72
Australia/ACT                                            72
Australia/ACT/Business                                   72
Australia/ACT/Canberra                                   72
Australia/ACT/Canberra/Business                          72
                                                         ..
Australia/Western Australia/Experience Perth/Other       72
Australia/Western Australia/Experience Perth/Visiting    72
Australia/Western Australia/Holiday                      72
Australia/Western Australia/Other                        72
Australia/Western Australia/Visiting                     72
Length: 425, dtype: int64
"""

# FutureWarning: `ETS` will be deprecated in future versions of `StatsForecast`. Please use `AutoETS` instead.
from statsforecast.models import ETS
from statsforecast.core import StatsForecast

# The `df` argument of the StatsForecast constructor as well as reusing stored dfs from other methods is deprecated and will raise an error in a future version. Please provide the `df` argument to the corresponding method instead, e.g. fit/forecast.
fcst = StatsForecast(df=Y_train_df,
models=[ETS(season_length=4, model='ZZA')],
freq='QS', n_jobs=-1)
Y_hat_df = fcst.forecast(h=8, fitted=True)
Y_fitted_df = fcst.forecast_fitted_values()

from hierarchicalforecast.methods import BottomUp
from hierarchicalforecast.core import HierarchicalReconciliation

reconcilers = [BottomUp()]
hrec = HierarchicalReconciliation(reconcilers=reconcilers)
Y_rec_df = hrec.reconcile(Y_hat_df=Y_hat_df, Y_df=Y_fitted_df, S=S_df, tags=tags)

print( Y_rec_df.head() )
"""
                  ds           ETS  ETS/BottomUp
unique_id                                       
Australia 2016-01-01  25990.068004  24381.908203
Australia 2016-04-01  24458.490282  22903.884766
Australia 2016-07-01  23974.055984  22412.265625
Australia 2016-10-01  24563.454495  23127.351562
Australia 2017-01-01  25990.068004  24518.113281
"""








