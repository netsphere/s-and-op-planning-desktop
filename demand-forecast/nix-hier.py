
# Nixtla を使う

# 実行手順:
# https://github.com/Nixtla/hierarchicalforecast/blob/main/nbs/examples/TourismSmall.ipynb

# 何で評価するか: 好ましい結果を高く評価しなければならない
# https://deep-and-shallow.com/2020/10/07/forecast-error-measures-intermittent-demand/
#   間欠需要には MSE ベースが好ましい可能性
#   not to rely on metrics like sMAPE, RelMAE, MAE, MAPE, MASE

import numpy as np
import pandas as pd

# obtain hierarchical dataset  デモデータ
from datasetsforecast.hierarchical import HierarchicalData

# compute base forecast no coherent
from statsforecast.core import StatsForecast
from statsforecast.models import AutoARIMA, Naive

#obtain hierarchical reconciliation methods and evaluation
from hierarchicalforecast.core import HierarchicalReconciliation

# 評価: https://nixtlaverse.nixtla.io/hierarchicalforecast/evaluation.html
# 組込み関数:
#   'rel_mse' Relative Mean Squared Error   useful for comparing the accuracy of estimates across different scales.
#               R package 'smooth' では RelMSE は RelRMSE で置き換えられた。最後に平方根を取ると RelRMSE
#               様々な一覧 https://www.ncbi.nlm.nih.gov/pmc/articles/PMC9718476/
#   'msse'    Mean Squared Scaled Error   sktimeにもある
#               さらにこれを平方根した RMSSE は https://otexts.com/fppjp/accuracy.html などに見える
#   'scaled_crps', 'energy_score', 'log_score', 'HierarchicalEvaluation']
from hierarchicalforecast.evaluation import HierarchicalEvaluation
from hierarchicalforecast.methods import BottomUp, TopDown, MiddleOut


# Load TourismSmall dataset
# https://github.com/Nixtla/datasetsforecast/blob/main/datasetsforecast/hierarchical.py
# Y_df: pd.DataFrame
#     Target time series with columns ['unique_id', 'ds', 'y'].
#     Containes the base time series.
# S_df: pd.DataFrame
#     Summing matrix of size (hierarchies, bottom). 加算先を表す行列
Y_df, S_df, tags = HierarchicalData.load('./data', 'TourismSmall')
Y_df['ds'] = pd.to_datetime(Y_df['ds'])
print(Y_df)
"""
           unique_id         ds      y
0              total 1998-03-31  84503
1              total 1998-06-30  65312
2              total 1998-09-30  72753
3              total 1998-12-31  70880
4              total 1999-03-31  86893
...              ...        ...    ...
3199  nt-oth-noncity 2005-12-31     59
3200  nt-oth-noncity 2006-03-31     25
3201  nt-oth-noncity 2006-06-30     52
3202  nt-oth-noncity 2006-09-30     72
3203  nt-oth-noncity 2006-12-31    138

[3204 rows x 3 columns]
"""

print(S_df)  # これも作ってやらないといけない HierarchicalReconciliationに必要
             # `hol` などはキーワードではなく, `Purpose` カテゴリの値
             # `wa` などは `State` カテゴリの値
"""
                 nsw-hol-city  nsw-hol-noncity  vic-hol-city  ...  tas-oth-noncity  nt-oth-city  nt-oth-noncity
total                     1.0              1.0           1.0  ...              1.0          1.0             1.0
hol                       1.0              1.0           1.0  ...              0.0          0.0             0.0
vfr                       0.0              0.0           0.0  ...              0.0          0.0             0.0
bus                       0.0              0.0           0.0  ...              0.0          0.0             0.0
oth                       0.0              0.0           0.0  ...              1.0          1.0             1.0
...                       ...              ...           ...  ...              ...          ...             ...
wa-oth-noncity            0.0              0.0           0.0  ...              0.0          0.0             0.0
tas-oth-city              0.0              0.0           0.0  ...              0.0          0.0             0.0
tas-oth-noncity           0.0              0.0           0.0  ...              1.0          0.0             0.0
nt-oth-city               0.0              0.0           0.0  ...              0.0          1.0             0.0
nt-oth-noncity            0.0              0.0           0.0  ...              0.0          0.0             1.0

[89 rows x 56 columns]
"""

#split train/test sets
Y_test_df  = Y_df.groupby('unique_id').tail(4)
Y_train_df = Y_df.drop(Y_test_df.index)

# Compute base auto-ARIMA predictions
fcst = StatsForecast(df=Y_train_df,
            models=[AutoARIMA(season_length=4), Naive()], # 3ヶ月単位, season = 4
                                                          # Naive() は比較用
            freq='Q', n_jobs=-1)

# 合計も、個別もそれぞれ予測する -> 上と下とでモデル変えられない?
#   -> データを分割すれば可能か
Y_hat_df = fcst.forecast(h=4)  # h= horizon 予測期間
print(Y_hat_df)
"""
                       ds     AutoARIMA    Naive
unique_id                                       
bus            2006-03-31   9412.287109  11547.0
bus            2006-06-30   9829.840820  11547.0
bus            2006-09-30  11566.556641  11547.0
bus            2006-12-31  11207.451172  11547.0
hol            2006-03-31  44043.000000  26418.0
...                   ...           ...      ...
wa-vfr-city    2006-12-31   1271.184448   1236.0
wa-vfr-noncity 2006-03-31    859.472229    745.0
wa-vfr-noncity 2006-06-30    859.472229    745.0
wa-vfr-noncity 2006-09-30    859.472229    745.0
wa-vfr-noncity 2006-12-31    859.472229    745.0

[356 rows x 3 columns]
"""

# Reconcile the base predictions
reconcilers = [
    BottomUp(),  # a simple addition to the upper levels.
    TopDown(method='forecast_proportions'),
    MiddleOut(middle_level='Country/Purpose/State',
              top_down_method='forecast_proportions')
]
# reconcilers= List[Callable] the reconciliation methods module
hrec = HierarchicalReconciliation(reconcilers=reconcilers)
Y_rec_df = hrec.reconcile(Y_hat_df=Y_hat_df, Y_df=Y_train_df,
                          S=S_df, tags=tags)
print(Y_rec_df)
"""
                       ds  ...  Naive/MiddleOut_middle_level-Country/Purpose/State_top_down_method-forecast_proportions
unique_id                  ...                                                                                         
total          2006-03-31  ...                                            63392.0                                      
total          2006-06-30  ...                                            63392.0                                      
total          2006-09-30  ...                                            63392.0                                      
total          2006-12-31  ...                                            63392.0                                      
hol            2006-03-31  ...                                            26418.0                                      
...                   ...  ...                                                ...                                      
nt-oth-city    2006-12-31  ...                                               49.0                                      
nt-oth-noncity 2006-03-31  ...                                               59.0                                      
nt-oth-noncity 2006-06-30  ...                                               59.0                                      
nt-oth-noncity 2006-09-30  ...                                               59.0                                      
nt-oth-noncity 2006-12-31  ...                                               59.0                                      

[356 rows x 9 columns]
"""

# 0 に近いほどよい
# RMSE = root(mse). 最後に平方根 (指標を元のスケールに合わせる)
def mse(y, y_hat):
    return np.mean((y-y_hat)**2)
#          ~~~~~~~         n
#           この部分が 1/nΣ
#                         i=1
evaluator = HierarchicalEvaluation(evaluators=[mse])
ev = evaluator.evaluate(
                Y_hat_df=Y_rec_df, Y_test_df=Y_test_df.set_index('unique_id'),
                tags=tags, benchmark='Naive')   # ここで比較対象=Naive
print(ev.filter(like='ARIMA', axis=1).T)
"""
level                                                 Overall    Country  ... Country/Purpose/State Country/Purpose/State/CityNonCity
metric                                             mse-scaled mse-scaled  ...            mse-scaled                        mse-scaled
AutoARIMA                                            0.188574   0.164931  ...              0.181859                          0.221785
AutoARIMA/BottomUp                                    0.09941   0.069593  ...               0.16835                          0.221785
AutoARIMA/TopDown_method-forecast_proportions        0.169527   0.164931  ...              0.215082                          0.245711
AutoARIMA/MiddleOut_middle_level-Country/Purpos...   0.120221   0.103676  ...              0.181859                          0.231327

[4 rows x 5 columns]
"""
