﻿ 
# Python 環境構築

Debian 12
Python 3.11

Web上の解説、特に新し目のページは、どれもこれも断片的過ぎて、役に立たない。


## matplotlib

<i>matplotlib</i> は依存するライブラリが多いので, <kbd>apt</kbd> で入れる
  python3-matplotlib/stable,now 3.6.3-1+b1 amd64

Fedora にはパッケージがない。<kbd>pip</kbd> で入れる

設定ファイルの場所と、バックエンド (描画するライブラリ) のデフォルト値を確認する。

<pre>
import matplotlib as mpl

print(mpl.matplotlib_fname())
print(mpl.get_backend())
</pre>

Debian 12 だと次のようになる。これは OK. <code>"Agg"</code> は non-GUI で, ファイルへの書き出ししかできない。

<pre>
/etc/matplotlibrc
TkAgg
</pre>






## venv

ここがドツボに嵌った。コマンドオプションを指定しないと、グローバルのライブラリを探しにいかない。ので、matplotlib が正常に使えない。


### グローバルな <kbd>pip</kbd>

root で動かすか一般ユーザで動かすかで、インストール先が変わる。一般ユーザの場合は, <code>~/.local/lib/python3.11/site-packages/</code> 以下


### venv 環境をつくる

取り違えを防ぐために, <code>~/.bashrc</code> ファイルに次を追加すること。 

<pre>
export PIP_REQUIRE_VIRTUALENV=true
</pre>

次のようなエラーを発生させるようになる。

<pre>
$ <kbd>pip install sktime</kbd>
ERROR: Could not find an activated virtualenv (required).
</pre>


プロジェクトのディレクトリを掘って、そこで次のようにする. <kbd>--system-site-packages</kbd> オプションを付ける。

ディレクトリ名は <code>venv</code> がよく使われる。

<pre>
$ <kbd>python3 -m venv venv --system-site-packages</kbd>
$ <kbd>. venv/bin/activate</kbd>
</pre>

<pre>
$ <kbd>pip install polars</kbd>
</pre>

<pre>
$ <kbd>pip install sktime</kbd>   # 依存関係で pandas も入る
(中略)
Installing collected packages: tzdata, threadpoolctl, scikit-base, joblib, scikit-learn, pandas, sktime
Successfully installed joblib-1.3.2 pandas-2.1.3 scikit-base-0.6.1 scikit-learn-1.3.2 sktime-0.24.1 threadpoolctl-3.2.0 tzdata-2023.3
</pre>



### 動くことをテスト


<pre>
import matplotlib.pyplot as plt
import numpy as np

import matplotlib as mpl

fig, ax = plt.subplots()  # Create a figure containing a single axes.
ax.plot([1, 2, 3, 4], [1, 4, 2, 3])  # Plot some data on the axes.

plt.show()  # これでウィンドウを開く
</pre>


以上!

